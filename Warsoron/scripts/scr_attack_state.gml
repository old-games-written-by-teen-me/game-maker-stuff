///scr_attack_state
{
    image_speed = 1;
    movement = ATTACK; 
    
    if (image_index >= 3 and attacked == false) {
        var xx = 0;
        var yy = 0;
        switch (face) {
            case DOWN:
                xx = x;
                yy = y+24;
                break;
                
            case UP:
                xx = x;
                yy = y-13;
                break;
                
            case RIGHT:
                xx = x+16;
                yy = y;
                break;
                
            case LEFT:
                xx = x-16;
                yy = y;
                break;
        }
        var damage = instance_create (xx, yy, obj_damage);
        damage.sprite_index = spr_damage;
        damage.creator = id;
        damage.damage = obj_player_stats.attack;
        attacked = true;
    }
}