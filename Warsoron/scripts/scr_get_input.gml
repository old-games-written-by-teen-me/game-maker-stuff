///scr_get_input
right_key = keyboard_check(ord('D'));
left_key = keyboard_check(ord('A'));
up_key = keyboard_check(ord('Q'));
down_key = keyboard_check(ord('S'));
dash_key = keyboard_check_pressed(vk_control);
attack_key = mouse_check_button(mb_left);
pause_key = keyboard_check_pressed(vk_escape);
spell_key = keyboard_check(ord('F'));
menu_select_key = keyboard_check_pressed(vk_space);
item_cycle_key = keyboard_check_pressed(vk_tab);
item_index_1_key = keyboard_check(ord('1'));
item_index_2_key = keyboard_check(ord('2'));
npc_interact_key = keyboard_check(vk_space);
inventory_key = keyboard_check_pressed(ord('E'));

//Get the axis
xaxis = (right_key - left_key);
yaxis = (down_key - up_key);