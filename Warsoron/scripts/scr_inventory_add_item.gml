///scr_inventory_add_item(x, y, item)
var xx = argument[0];
var yy = argument[1];
var add_item = argument[2];

//Check for out of range
if (xx != median(0, xx, obj_inventory.width-1) || yy != median(0, yy, obj_inventory.height-1)) {
    show_debug_message("ERROR CODE : SCR19L7");//The x and y are outside of the range
    exit;
}


//Get the item count
var item_count = obj_inventory.box[# xx, yy];

if (item_count == 0) {
    //Add the item
    inventory.box[# xx, yy] = add_item;
    
    //Add one to the count
    obj_inventory.count[#xx, yy]++;
    
    //Return to back out
    return true;
    
} else if (item_count > 0) {
    //There is already an item here
    // Is the item we are adding the same?
    if (obj_inventory.box[# xx, yy] == add_item) {
        //Add 1 to count
        obj_inventory.count[#xx, yy]++;
        return true;
    } else {
        show_debug_message("Error code : SCR19L33")//There is already a debug there
        return false;
    }
} else {
    // Out item count is less than 0
    //Error
    show_error("Error code : SCR19L39", false);
}