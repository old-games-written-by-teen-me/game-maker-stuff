///scr_jump_attack(x, y, jump_height, spd);

//setting up the arguments

argument[0] = xx;//xx is the obj's x we are going to
argument[1] = yy;//yy is the obj's y we are going to
argument[2] = jump_height;
argument[3] = spd;

//Do the jump
if (x != xx and y != yy) {
    move_towards_point(xx, yy, spd);
}