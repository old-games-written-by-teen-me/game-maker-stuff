///scr_text("text", speed, x, y);

txt = instance_create(argument2, argument3, obj_text)
with (txt) {
    padding = 5;
    maxlength = 180;
    text = argument0;
    spd = argument4;
    fontsize = argument1;
    font = fnt;
    
    textlength = string_length(text);
    
    draw_set_font(font);
    
    textwidth = string_width_ext(text, fontsize+(fontsize/2), maxlength);
    textheight = string_height_ext(text, fontsize+(fontsize/2), maxlength);
    
    boxwidth = textwidth + (padding*2);
    boxheight = textheight + (padding*2);
}
