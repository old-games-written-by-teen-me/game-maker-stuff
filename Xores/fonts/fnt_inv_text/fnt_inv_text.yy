{
    "id": "ab3c65d5-3c8d-4818-8250-0a350c360df6",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_inv_text",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4ad3ab9a-1a2c-43c7-915a-73df4aec2174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a8965209-6dfd-426a-8591-5a153758e434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 81,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6822b726-ca3e-478b-93cc-981fe0137474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 7,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "673d45cf-c3d4-4c5c-9738-958e9024cc2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e989cb05-bb05-4840-a6f7-ee8314ca14ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b8fafa57-8549-4eed-9731-15c5e4067f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "29ceec53-c388-4387-85ce-8dc035da53fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 34,
                "y": 13
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0efb1146-6065-4a58-ac21-ad47f2fa3623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 93,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fe594909-fdf4-4ff3-ba76-2623fbd9f81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 57,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b5ab2acf-9a25-4202-9dd6-122a1a1126d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 62,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a6c81a0c-9bb8-4dc1-8979-3ff6f41be9d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "78098b1a-25c2-467e-bbf8-37d98d2e7557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "af9e5010-eb5b-4e1a-96f1-7a3db119bcd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "336bc31e-bc54-40de-b026-b4cdf309d77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 47,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "088a1a64-8c5d-4b24-a16f-a1d265e25d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 105,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d730aa86-e913-4f4d-b0c6-2634139b684b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 37,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d75a3028-182b-4d76-931f-b482b88a2090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "3ec0fd5d-fa57-4b8b-add0-a06915e6d4b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 32,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "791751cf-bbfc-4c39-b0e1-253f9e00526d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "31f44870-c1f0-47f0-89ea-b8a2aa9967b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0f7cf4d0-ef98-4691-a494-a51f8ae7b50e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "99613bb6-c9c4-469f-b8f9-7af6b3a82a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c6dae7cb-68dc-44a6-bf61-aabf3a2a0736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "88907ed3-6f5e-4152-a147-97f77852170b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "120bf518-5041-40e5-a42c-2622e61f17bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "97d0c53f-d503-4c12-8a68-8484f652ff4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 107,
                "y": 24
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eb9c2617-8319-461f-817f-d61947345487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 113,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9076979d-03f9-418d-b5ff-0b500a305c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 117,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3384b099-0de4-416c-9e96-4f909981a676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 35
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ab5219d2-cd05-4349-905c-7cdccbccd359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4148bc5b-a1c8-47b9-9c44-7ba6385d9dbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 24
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5d8bb0d5-a8b2-4ac4-809f-831d46d14fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 24
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c022c85f-f5f3-4823-b11d-05bb48c7baa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "acf33197-35df-4c39-8413-d49dddd522c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 7,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "6379dd34-eabd-4747-9e59-fe946e9c6847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6f881b51-2336-4fe5-8cde-8b9e3032b785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "21d30d0a-48cb-4430-b038-de1b17c7d2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "aa124932-7b33-4fa8-9566-1f1f6ba1c076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "09086270-233d-4093-bebf-475bfd03dfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 13
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9a493917-6054-421b-b032-e532b8cead23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4d19e270-2879-45ac-bf50-85c9870e5c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5e9ae54f-b952-4cbb-a032-47a59a344fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 2,
                "x": 109,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ea818b01-583c-48f0-b4b2-8d3d2677565d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 109,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "97b8590d-1ff9-4e03-b780-158d96b73146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "bfe76fda-3517-4575-bd22-676f00d657eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 24
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "40f68181-6219-4612-8d66-5b47681457a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d7eaa3c4-ca26-4ab6-a247-3d7ca6c444a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "86d03448-71a4-4f1a-8cef-2b491ace0ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 13
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "33f45cf1-fb0d-4dd5-b014-34c804b90547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 66,
                "y": 13
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b2a71d48-66e6-4fd3-a444-6b29c1964b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "846545a3-778c-4aa0-b89e-da0746a8fcba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c238cc9f-ccef-49c8-bfb0-a34e2b396ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 101,
                "y": 13
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "72339c68-6745-4e87-9747-6e2f70228b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6a1922e3-35b4-4b25-8212-9c4fc5406736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 13
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1f7c26f2-1f3d-47be-9a22-5877f49fff1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e9f90944-2a48-47e1-a40e-bca61292ce66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b4e4fd15-21ef-470f-a9fb-27445b502977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 42,
                "y": 13
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ef1f201d-daf3-487b-9062-734f4176fa2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 18,
                "y": 13
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "34bd7930-1cae-48f3-8903-5053fe86b2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 13
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ef14bfed-facd-4dc8-b7c6-e71d0dc116c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 1,
                "shift": 2,
                "w": 3,
                "x": 72,
                "y": 46
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b2602057-c6dc-43f8-9d7e-8add1d042654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 67,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "38d435fe-245f-4422-a9df-36742f4697d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 97,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "459de15e-bea3-4d4c-814f-a83c1ba47ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 91,
                "y": 35
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fb73abc6-20a3-41b5-962d-506e90002405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e68bcf0b-f891-445f-aa3d-440061df5f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 85,
                "y": 46
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bf1a84e1-ea67-4298-8745-498c31634e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 73,
                "y": 13
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a6e98dfc-604c-42d0-9641-69b951be2353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 80,
                "y": 13
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fd79100b-c195-478e-a1bc-8ce57909cf18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 85,
                "y": 35
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "80bbaf3a-bb2e-48b1-9b5f-3d71b52a7d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 79,
                "y": 35
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7d5b80be-6fff-4d73-ae32-cb9cb6e1912b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 13
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5d97ca56-945a-4dd6-b5da-961fa9fbdf5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 42,
                "y": 46
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bb153154-3530-4cba-9b5e-aeec457e8a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 67,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "db8486ab-dee1-4b2a-8521-90ebf9f87aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 61,
                "y": 35
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "898703fc-5cde-43a0-8dcc-531e9bc2188f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 89,
                "y": 46
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "4d82a283-9860-4e82-aec6-0b8f12c68d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 27,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f293f63e-b021-4c84-93ab-cffb09bafb08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 35
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "82688a75-0308-4094-9194-e517a24e0ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e1c8f17b-8be3-4e98-8590-b930b50bff8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a5986e92-4336-4894-8307-e73617d6c0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 43,
                "y": 35
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6d66bec8-72c4-4cb3-8fa0-8bbf80046725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 108,
                "y": 13
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e58d4148-fb7b-4b97-b222-b2bfb7e18f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "55d1a0b9-898a-4a2c-b651-9b11f1f45bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 37,
                "y": 35
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "967da50e-87ea-4516-8776-b444c8e2d9bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8876b4fe-49f3-4ae7-bca1-8fa2726f52e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 97,
                "y": 35
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4cc8662c-badb-4131-a274-e834b70d297d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 17,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9217be5f-6ab8-4a90-81bd-4a2bae28e4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 49,
                "y": 35
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cd322523-3fab-4ac3-beff-df381d449e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 73,
                "y": 35
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "220e27dd-0cec-482b-9446-bc108fcdd584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a4c373f3-976f-4a39-84c4-91aded77dcb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 115,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6a6b733f-1d68-4fe9-bb09-7958519c1012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e3ef3872-1859-4f90-9216-a0539f6f28f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "896b5a00-8734-4a11-a27f-91d818ece6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7f308692-b873-479b-9ce0-9294d8c90cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 77,
                "y": 46
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b7d07683-d12f-4783-86b2-1dc31f80af09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 22,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9af55761-9f7f-4edb-a6c1-964a5181126c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 24
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}