{
    "id": "ade5a85a-0174-46bb-815e-26e7bac2dadb",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial Black",
    "glyphs": [
        {
            "Key": 113,
            "Value": {
                "id": "0753da13-17b7-4086-8fa8-d6df45876a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 154,
                "y": 24
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f5c38123-f779-485b-913a-55c10c4e4caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "132ee9a4-de6b-4133-82cd-cfd5b5a0bbb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 47,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8a232535-b4fb-43f9-b1ad-ef703c4a8e94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 249,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "50da3fd8-6f82-43ba-9221-0798550a0ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7e8c7528-4b95-4e5e-82c9-270478f7a287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9267f069-0f14-4507-aa46-1a73b962539b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 34,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "215f06d4-ce80-437a-9238-1d1a579b4a5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 25,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "93f43185-8061-4337-ab6b-3e2bf189bb54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 80,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e17d73f6-5f6e-4bcb-afec-4bedd67a6570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 212,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "78c08361-04c6-40ea-b5ca-8a156c6f689b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 244,
                "y": 71
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1dbf8f79-33a3-4f26-a825-250c609480a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 189,
                "y": 71
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7a9abd8b-89bb-400d-b223-0e6970d9dd1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 224,
                "y": 24
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e7390c1b-63dc-4440-afab-8f2a39765c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8ab2b678-fce5-4306-b04d-46347d8c4ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d68e1892-db93-48ae-8596-2a6fb4b8bac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 36,
                "y": 48
            }
        },
        {
            "Key": 32,
            "Value": {
                "id": "0f1d9756-0faa-4919-86c1-b643ff66066a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 23,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 131,
                "y": 71
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "d4a123c9-6b32-4322-9677-afa9b1cf051b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 168,
                "y": 71
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "59e46332-ed9b-4541-b028-8d6cc5e0f59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "28aec274-f791-435b-9f19-ad1dac02a7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 190,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ef451400-34f8-4288-8b21-81d802024cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f1d6d4c8-08f9-4cdb-a701-789fc63066c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 179,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "77903647-3702-41ff-b397-38545fb21a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 236,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "08cf3af9-2d1c-405c-9415-51de9a519ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 157,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "58a6db3f-6779-4d60-8861-d047e8dc3bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 12,
                "y": 96
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ded3a2a5-8282-46f7-a576-5f0c81f6432e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "38a9f43f-bd0a-4d96-8560-d291ceb0a8b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4ba27c4e-5036-420c-9607-0d379208066f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 234,
                "y": 71
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a9de5818-fac5-4680-8c22-dd37328e82c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "e8ab0fac-cb9c-4c7f-94b3-4e5696db6017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2ef72e93-d131-4fe2-90eb-2b9e5436720c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 196,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "727a25bf-f1c0-4891-9985-d4f296eac588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 14,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6366cfae-af53-456d-82f3-96327356a698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 131,
                "y": 24
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1bb87a67-dc87-4523-879a-db298ecfc62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "edea4424-4f9e-4da6-b7ab-faaf61dbf860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e1e489ae-ff0f-4581-8119-54db77d28e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5658af30-a1ea-4fbd-9b49-deda4e93ef50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 124,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "06acff84-a9a4-4cae-8a76-4a8bf0dfbb35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 176,
                "y": 24
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "efb555b7-37ae-426a-8f0f-d2b27a80177d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ef108aa7-a305-466a-86b9-97f2e000e8f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a267c115-ed73-439b-918a-3313c11e3d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 161,
                "y": 71
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "663ae27d-88f6-4cd4-9492-cd04dd100aa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 165,
                "y": 24
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "47a2a0c1-05c3-4b40-9708-585215ca2744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "50025bd7-6c93-47de-9c0d-82fc8f353640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 21,
                "y": 96
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9c502799-89d3-4ed6-9cf8-ef9fadbdd88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 44,
                "y": 71
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5de26702-bab7-4e6f-b04a-567b2477c1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 212,
                "y": 48
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "28412c6c-ae64-4c25-b331-93896cade20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 118,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ac329879-92de-4ffa-ba3d-3d0b696e6c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 64,
                "y": 71
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1ec78ea8-bd53-4362-aec0-44ce36b99de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 7,
                "y": 96
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "01b5d987-6f54-4a90-a6f6-0e4202b65adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 14,
                "y": 48
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "8b6c2518-dc44-4333-bf41-99f8671f96cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 228,
                "y": 71
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "714004a2-38f2-4502-8734-af739d53276a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 5,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "84b72eea-255e-4a26-92b4-2464ecd5131c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4e32f128-f60e-4e77-b812-bd42136cf020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8b46850c-0a02-4772-bae1-04345724f806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "53b14941-9151-4c8c-b507-8a8da11dbfe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 200,
                "y": 24
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cdfd7a5f-b584-46ee-b2d7-7226318a87f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 105,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a9b2df53-91d3-440a-b814-0e13c02ab185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 91,
                "y": 48
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "992ff42c-fae2-49dc-8994-b86cdc895169",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a008388b-c916-410a-8e1d-a88ebce9cf5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a8c36ea4-a63c-42b2-b59e-d28c38c3819a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 53,
                "y": 24
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3cc560c7-fbca-4d6d-9326-0d236886462f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 27,
                "y": 96
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a3e15260-d421-4671-aba1-55a7867ddf96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 109,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b14a915c-5cc8-4260-9b52-ead7532bdbc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 239,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e0fe54b9-4cf2-44a6-873a-e130a196b59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 84,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a46633cf-4b22-4a14-af30-4589c30ea30f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 69,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ae30e803-74b4-455e-81e3-60d4c53fa849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 58,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2d8fa156-b58b-4707-930b-ad9e9ba2d72b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 40,
                "y": 24
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a9efa0cd-7b34-44bc-80bf-286792440d62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 244,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "97d3c202-d2da-44e4-8c7a-ce13a63580d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ab4a428b-e728-4ccc-84c1-62d3d41fe6ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0ea509a6-25fa-4224-965b-ecf5a85cadca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 138,
                "y": 71
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "29307997-c19e-4a86-a159-c9524862b8a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e42b262e-cd27-4e73-b452-94909fd96a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 175,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "091f73db-e316-40a7-940e-bc17fd543bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4c26cfd4-b68f-4cfd-bf73-4b687a049f31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 24,
                "y": 71
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "61f56e60-4552-41ce-8334-3fc61b86bb79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 93,
                "y": 71
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "f27b8488-09ad-4d0f-91b6-94b9210a7862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 120,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6ce262db-bec2-4fb5-99ab-9a5babe75402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 201,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "caa65332-5d8a-45cc-bf19-ce4d0264e326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 135,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "413858be-71b0-46c8-8b62-d16addd35867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 233,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6237d309-6333-4853-ac00-1ae5255aa150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5a3f87fc-7355-414a-ae19-18c3d628bc8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 220,
                "y": 71
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b63dde75-dd6e-43db-8eb4-90e7a6f34508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 188,
                "y": 24
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4e90e611-6127-4fa3-b753-ee28dd5ebf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 154,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d0b47314-2ac2-4a27-92c7-c2b7717918c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 143,
                "y": 24
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8a650b2a-f0cd-4435-b121-4b64559e7b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 27,
                "y": 24
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dcffe626-dcb1-4367-8c08-44012cecc190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 113,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "315084e3-c149-4543-9b52-289a40b01bdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "40b8bcc5-81eb-4278-8275-ecbeb11b6c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 54,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4dc1f943-2f1e-43ef-874e-0bdce2baa96d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 17,
                "y": 96
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a148c343-bcda-4c34-8f26-f8f59f9851f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 11,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 203,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4b485cbb-631e-4db2-ada8-57745e9c17b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a0d04166-b22a-4b12-89a9-cbef1b9ffc39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dee2393a-ab75-4672-bd9a-9e14c23c00a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 66,
                "y": 24
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0a3b01df-669d-4ed5-9546-d0a1c7756988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 2,
                "y": 96
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}