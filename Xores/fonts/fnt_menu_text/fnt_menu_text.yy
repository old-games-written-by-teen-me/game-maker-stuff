{
    "id": "36603fe4-cac5-43ec-8e39-d98b7b124cfd",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu_text",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a8510295-eb04-4ac4-9656-15b131ec541c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 37,
                "y": 86
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ee5960f2-9928-4c0e-b694-441d9e430837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 106,
                "y": 72
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "d6568f52-6273-4a6d-bb47-9bd4a23f50b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 24,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "468481dc-d452-4fe2-9784-a4c3e62f9ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ca705019-15fa-4c57-922b-2551e7b85ccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "457e7940-b861-4094-b5bd-c65ddcfd10e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7ba372a0-30f3-4f9b-b94d-b7fbd5e390d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 102,
                "y": 16
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "464396bf-15e2-4726-bfc5-a648d91ae624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 32,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f81df75f-58bc-467b-b3b4-d194d07b15df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 82,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "abb32b90-d5f5-4eb4-b82e-74c02bdce9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 76,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f331b509-4ebd-4c90-9f35-7b4eef69da5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 10,
                "y": 72
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3d979c17-6655-4601-b265-461a2584a943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0bec30b7-1f0d-4f13-831a-e00bf6527a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 116,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "977ad75b-e61c-4c24-8915-3010027e30f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8e6c38a9-6780-4bc4-9b6d-c8905cfdca2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 17,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "97b43e60-0574-4281-9f33-5edd94b7b3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 5,
                "x": 38,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ae4f46a5-0fa5-4189-8843-bc58cc3eaabb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5b306833-83c9-4ffa-838e-4cd71ccdbe15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 17,
                "y": 72
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "901fc36a-42cc-4a04-81b8-c7b0bfe84aed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "92f8df15-c1b8-4c3e-8165-dfbf2ca59c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "19325598-e432-4519-adab-c109ca3b4705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "7e1183c9-4339-4af5-81a9-112936cb6185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0d3a549a-47eb-4238-b9af-a66081b8207f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f1e756f3-c6ed-4d58-bcbc-291e293e3844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ffa54ad6-8a96-4aca-b64d-c7426fb8ffd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d2389cd5-96f3-4de7-b022-d3d9db6eb039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9912fb1a-6af0-4223-bcf0-00b0b06eeebd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 42,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "732a7ad5-8a57-41a5-a4b7-cec95c870255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 22,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5f07b5d4-aff8-4dcb-8e55-2d90e841805c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "631b66f7-c3e5-4512-83c1-badf305cec10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dc516846-8cd7-4844-b2d7-59ff8532f0e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "cbd05d0f-f0a8-43a3-a899-bf64230e6656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b8e6bae7-1c92-47b4-b0fb-590c77777215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cbae1a97-91f7-42a1-9069-64bd6878f324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 16
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3cc84d14-6922-4d33-849f-2b544d255774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "752f4820-31b7-459a-bea1-d6eadd962ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b271ccec-bc0e-40a3-83f4-6327894361e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 16
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4e8da118-a262-4728-84a0-e6b3f8e8ff08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2d128035-d147-4e86-bfb9-3d9d8fe7fbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "683616d6-3e44-40ea-8b10-9fa14c4f41c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "3086a8dd-eefa-4f5d-8f4b-1d7ad5b4cf6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d6136864-182b-421d-9c7b-f626891112cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 27,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "000e832f-d42b-4115-b9c1-1985bbd2a86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2ee96ebd-52fd-4edd-82d4-fc070717923b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5cee8115-8dcc-4cdf-8c16-0bf38330c36f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a81b861b-7782-4cfc-b8f9-f196498cab5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e71db58e-38f3-4800-bc69-2f71bbae0b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 32,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e76851e2-f2d8-42f4-9ddb-5c6783c6131e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0d9c5fd2-93c5-4960-8631-464a5762b05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d32b1adb-2cd2-41f8-b752-2d43f803181a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d2e7093e-2e13-468b-b7e8-cf90a47600c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "aa6f0c8d-6147-43cc-8c98-63b5acb4a011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "10d93d87-a7ed-484c-bd3d-fdb9d1fb86fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3141c5c3-9280-438f-bb2a-ccd7ef8302a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e380dd64-0608-4e36-a24a-6058e30e1988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4a736e1a-ba59-439e-b757-7c2788ddaaa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "79b878db-3186-41cc-bf5c-a2a8c7cb8092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 112,
                "y": 16
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c1a46d9f-c4a8-413f-99c4-1703af71a42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 9,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b9289175-9f52-4d28-b3d3-485cbdbe6c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "77a679d4-24bc-41f4-886f-37b2c97250e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 94,
                "y": 72
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bcc053a3-4b57-4a49-87fa-a833b69f8621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 5,
                "x": 45,
                "y": 72
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "38477609-3af2-4eea-b2f2-b74737b23ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 12,
                "y": 86
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "feef32e1-d8a8-4558-a4a8-c051054b889b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 118,
                "y": 44
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "497d8255-2064-41cf-8dd4-ea888e519b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 92,
                "y": 16
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "44c2a86b-0c0b-4a42-a22e-151dbe0676f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 86
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "969b945a-f690-4f9f-aa7e-93ee2406ece5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bd03123b-d8a1-4695-aced-4baa73fc01a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 30
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4dec8b5e-03a3-4ea1-adc6-936e5052a4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fc840804-64a9-47c1-9af9-c96c9c0360d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bfa3c838-710b-417d-99ab-f9b5d8835ac2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d4ffd7ee-f778-4b27-838e-093bd02809c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 52,
                "y": 72
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e1f9d63f-11ef-4cba-8a9c-f7a81228fa7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d1d93907-26b0-443f-aeec-45de639de233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 78,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "71763a84-41b7-49d0-8c83-06d28679edbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "db10f0f6-a133-46ef-8e25-5c29c926fb26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 70,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2fd8d96e-862c-4225-b099-9aeea49c3075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 7,
                "x": 110,
                "y": 30
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e2b06d1e-926a-4845-ba33-a60fa1cb6a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 121,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "cd88a685-481f-4464-9dca-61437ba65608",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c6a1f201-3a30-46a9-826e-eed4c4422872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 54,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "820ce18d-397c-4471-951b-e2e4d4ced15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6169587f-f33f-4483-b19b-811ec3726d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "481684b1-afe7-4450-89e3-bdcc28ccd939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "085b32b1-ae07-4bbb-bc80-71077ef9cd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 31,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "fe2dde47-b0c6-47e4-9e10-b21b4acc5bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f25b11b5-e5c5-426f-8127-8f1e245dfe44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 88,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3dff8436-7912-48fe-8883-153e50d35f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "83cf9a57-e897-4762-ae89-3f6ac078f45a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 70,
                "y": 44
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dcee28bd-9197-4f61-9618-a9c1a41ed808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9aae38a4-bbcb-43b4-a9c3-df86073695bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a2d83e5a-a899-4353-a610-ff9fa7fc1397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3f228e0f-d354-4c6b-9482-c2357d0ee5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "24d47570-4a82-40e0-9589-6550cb3fc823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 64,
                "y": 72
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3fe4ebb4-cf81-404f-8353-031411a38d19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 111,
                "y": 72
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8b30ded9-e703-4790-9172-c736e38c5218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "92bbdf42-c4e0-4a12-be2c-3b8eec560258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 11,
                "y": 44
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "6018c2a6-8fa4-4f49-9972-2dcdef3d9e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "c2455db8-fee6-482d-9767-3dffe127b9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c6531790-7536-4cf3-8f40-c85b82c5b3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "799bc2f7-c01f-4e98-9c33-faf7c3aed589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "08c68c8c-867b-48bd-9c4e-1e4a99f1320b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "943d42b9-f7f5-4cb1-9056-6db13c680cba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "7f91446a-94b6-4000-8e0d-f7f0f99f0d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "0b0141f6-f7ea-4a58-9cc0-b9bbb4c87978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "d50df6c1-13a7-4e82-be8e-914e1e6dea2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "b5e0460d-057d-4174-a3e5-830965572dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "bc662ae0-f347-446d-8bea-13790b18f2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}