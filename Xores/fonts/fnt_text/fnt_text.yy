{
    "id": "298de913-ad93-4228-84bb-63fad0aa249d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Coder's Crux",
    "glyphs": [
        {
            "Key": 113,
            "Value": {
                "id": "a4dc5743-3f3f-41a5-b718-0d7062501908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b1bef142-e395-4f98-afb8-788582616b9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 222,
                "y": 93
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f61d31d9-b941-4528-a311-69242a961f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 121
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1fa7c2d1-0ac9-46e9-a039-458e465f3366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 61,
                "y": 177
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4acb5239-4724-43f5-bfc3-d9e892e6445b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "dd472777-ee7b-477d-a096-63a8b9943404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 202,
                "y": 93
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f2363806-b214-437b-854d-bde9f5414840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 42,
                "y": 121
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "12fa000b-c5c3-4714-b163-5ea4ed916296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 121
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d599e9c7-d17c-460d-8e8d-7a44c8c8f583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 121
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b906a388-1997-41c1-a9a1-0ccc47f2f81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 121
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8e6fc14a-0b3d-4e8f-a232-98eb5f10934a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 48,
                "y": 177
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f57ad160-0922-4a5d-945e-04cf060709fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 93
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "422c7029-4be4-4969-ac89-0c634128af3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 93
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c01ebc20-aff6-4826-8d2f-e4441136422a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 93
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "87a23b86-2e13-4e82-bcdc-e6527b5a1d39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 15,
                "x": 182,
                "y": 149
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1f862eb9-699c-4d98-9cb2-bf67e3bfae2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 42,
                "y": 93
            }
        },
        {
            "Key": 32,
            "Value": {
                "id": "eea30ce7-b995-4e7f-985a-bc616b2872cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7630854b-8316-4e85-913d-109ed2aeac62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 35,
                "y": 177
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "24cd9717-654c-44d7-a75d-a626cdec6090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 102,
                "y": 93
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "015f9d58-c7ee-41ec-b65e-42a8f864d2c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 122,
                "y": 93
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e0ed6a5d-820c-4cf3-9fdf-0dad473353f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 149
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff0a2e1a-11e1-43ec-a1d4-06e6ef58e92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 162,
                "y": 93
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f1155b2d-c2d8-4896-800e-09c70932bc96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 149
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4b022bfe-77f1-46f6-b859-f2f388be7737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 102,
                "y": 121
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7eb2a0a2-28dc-4b1f-9299-0ea0c6f7199c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 7,
                "shift": 22,
                "w": 4,
                "x": 2,
                "y": 209
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fbea6be0-47f9-46da-8f00-917ea8e2581d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 142,
                "y": 149
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4321b25c-a2b2-478b-bbac-6e9ed6926315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 122,
                "y": 149
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4559e138-9ffc-4347-ad88-dad781850403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": 7,
                "shift": 22,
                "w": 4,
                "x": 248,
                "y": 177
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f7828aa1-26c8-4513-9149-4c64f99252f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "1a0c95a7-168d-4910-9417-e04d56fe8a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 162,
                "y": 121
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "556f8d4d-c34a-4edf-ad73-9b09912cce5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 182,
                "y": 121
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "36543b6d-cf4a-4f75-825d-0756b774f1f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "537a38d8-616f-4017-9c36-fafe96ed6e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 222,
                "y": 121
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f516f665-2c87-48f8-b4ec-fc1802e7c40e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 202,
                "y": 121
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d3bb4d82-f1b3-41c4-badf-416b528a6988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 122,
                "y": 121
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "06281031-5e54-4f8f-a95d-cec1bc6789f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 142,
                "y": 121
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "be8382e9-c27c-4b46-aa8e-3d113504d1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 42,
                "y": 149
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "43b91df4-ff1a-4797-92f1-f00259173467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8c60a986-660c-4310-b858-731a4f55f195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 122,
                "y": 37
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "00e3d2f0-b2c5-420f-a726-17df793a7972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 142,
                "y": 37
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "15289739-7036-4794-9228-f52478f7b575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 74,
                "y": 177
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "937b6f74-98ee-4f43-9400-29881e8a5525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c29d441c-d606-490d-abbc-fe4aab8c4df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 162,
                "y": 149
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ea6cc99e-ae77-4c4a-8aaa-9d6a5d5c3b54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 11,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 235,
                "y": 177
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f19c40b3-56fd-4d32-9c67-08013c8b8086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 37
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bf0fa942-6492-4c1e-a634-ba26d2e9ca20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 37
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ae620251-d608-456f-9c83-5d2fdfe3c2fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 102,
                "y": 37
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9284b7c7-74b1-4b84-91f5-ee62bb5f28db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 166,
                "y": 177
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "3f3612cb-f031-4aae-9eaa-25ff4f199ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 7,
                "shift": 22,
                "w": 4,
                "x": 8,
                "y": 209
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "24f12f47-1120-424b-a2d7-cab9daff57e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4ebe53dd-2ea2-4b4c-9b8d-3df81ec10f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 113,
                "y": 177
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "43a246c5-63e6-4333-909a-29f295e148c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 15,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fa188d79-b097-436a-b6f5-78855ae92e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "24434e85-0b83-4095-9231-5ae4fb23be6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "34f97e99-9118-4196-934d-9ee8d9be2331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 162,
                "y": 37
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "81cc2cc4-371b-46ce-82c5-d152bed75b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 142,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "36f69252-83f7-42d0-8cbb-0027eeb22ef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 162,
                "y": 65
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "4c4ce3e0-f1e0-47ff-b5e1-0a362aec9432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 102,
                "y": 65
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "26ea9027-8bc1-4efe-b4bd-fe7dc83d1486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "952c18fc-d5a0-4005-b4dd-7cdedaf57a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 122,
                "y": 65
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "73eb6715-025d-43ad-a5fe-2a39fd14a13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 222,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "42fdb79e-5ac8-4d40-8529-c3e5ce0aaa98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 0,
                "shift": 22,
                "w": 4,
                "x": 39,
                "y": 209
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f1069549-6737-4ce9-8dd4-4b658959c157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 11,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 186,
                "y": 177
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d0e3f637-3aa7-4f44-b1b3-616f8b95ccd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 4,
                "shift": 22,
                "w": 7,
                "x": 226,
                "y": 177
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0fb1aa8b-78f6-4f34-9703-5ed1be421a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 100,
                "y": 177
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "f2c86d4f-7920-42ec-91de-30505cb10e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 202,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "25f21c66-c3c0-4074-bb7c-09db3759de3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 222,
                "y": 37
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5c0ef067-b9b4-4ccd-946e-2253fefc7488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "02bd57c5-1e75-476b-815a-e8aa4f0074fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 15,
                "x": 199,
                "y": 149
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3eb79ae0-d7de-4c0c-8281-eb7c287f24b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 182,
                "y": 37
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d0c6ddc3-2634-4213-b709-78f5690bb5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 202,
                "y": 37
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6bbeeeee-c5a9-458e-b972-aae3d1dc8334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a680977e-de6c-42a0-8d77-08ada246a523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 82,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ff7dc512-7cce-4513-bbef-6c958b84ad17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 87,
                "y": 177
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eee6d1f1-aabe-4cb9-b33a-9e59265380ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 42,
                "y": 65
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "70f34461-06ae-4de1-96bb-0d987c504820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 65
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "54fd1d17-cc6f-41bc-9713-a1ed29198c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 14,
                "x": 232,
                "y": 149
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "25fdb85d-9a4f-480f-8813-99932218bd36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 15,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 126,
                "y": 177
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1546cfaf-b650-4a62-ab1b-deea97fc1b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 62,
                "y": 37
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "371a568a-ed30-4892-8f73-184f9bc484dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 142,
                "y": 93
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e22bdb0f-854f-47e8-aa4a-dcd01eecc10a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 14,
                "x": 216,
                "y": 149
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "425a133d-ef20-4fd9-ad72-211646724bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 146,
                "y": 177
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3b66f96a-3d12-4d61-b1b8-ad103c5d6b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 206,
                "y": 177
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "78d8c451-7dd7-4557-b234-ed13d2106588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 182,
                "y": 93
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bfca2989-ee9b-40e8-b01f-bec79febb4f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 4,
                "shift": 22,
                "w": 11,
                "x": 22,
                "y": 177
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7feb9e1f-ba49-42d5-8b1d-c975854b3522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "02f1c43d-78fc-4947-a379-5e97b3988a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 182,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d6f33e03-d281-4df9-bca4-9cf97736388b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 93
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7c9b186b-b291-47c6-9319-e43798af427e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f094aec6-b4c9-46dc-ad7e-3c506a496173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "70fa5135-ebcc-46ea-a266-f0211650db3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 7,
                "shift": 22,
                "w": 4,
                "x": 14,
                "y": 209
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b88f3970-8624-47a1-8daf-b9968d124ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 0,
                "shift": 22,
                "w": 11,
                "x": 20,
                "y": 209
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1f9ef44d-82a8-4113-9b06-0016bab3707a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 42,
                "y": 37
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8e2d13af-20b1-4060-b83d-eb61f3008afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 22,
                "y": 149
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ab9933bc-a970-4fbc-96d8-43f980f815bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 102,
                "y": 149
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2e127388-9c3d-4104-814f-294ee7581a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 7,
                "shift": 22,
                "w": 4,
                "x": 33,
                "y": 209
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 44,
    "styleName": "",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}