{
    "id": "48435433-1dc6-4398-b0ba-e385be7d34c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oITEMHatchet",
    "eventList": [
        {
            "id": "919133a8-fbb3-4c2d-ba23-39187ac5c80d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48435433-1dc6-4398-b0ba-e385be7d34c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a88727e3-c8ec-4f88-a5ff-fca28405c48a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4eab5155-978c-449a-bc56-c324205d9d0e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e7cbdc4e-e105-487e-932b-977036af6b3c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "2ecedb5d-112d-41a9-a432-6568b7779b2c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "2571476e-f2e9-4ba0-9549-0090b56bbc25",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "863fc26c-de79-44ec-8e7a-65610ec565bd",
    "visible": true
}