{
    "id": "cf7a34d4-af92-4cef-a1d2-7a930c7d4998",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oITEMWood",
    "eventList": [
        {
            "id": "3d0c1cbc-36a7-4fd0-9ea9-0af4d4d30fb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf7a34d4-af92-4cef-a1d2-7a930c7d4998"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a88727e3-c8ec-4f88-a5ff-fca28405c48a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "c5eb88df-0b95-4205-9c42-af894691526f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "f17d8e51-6d5a-4eab-8bb2-b6a243d63ff2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 0
        },
        {
            "id": "547bc1c2-1089-4f3b-98f5-7aeeb3a1c54e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 8,
            "y": 8
        },
        {
            "id": "108babe9-5f87-463f-b433-c76c5ba33630",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 8
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a262212c-eb70-401a-8f87-2913f89d8951",
    "visible": true
}