/// @description init inventoty values

depth= -9999;
totalSlots = 19;
curSlot = 0;

for(i = 0; i < totalSlots; i++) global.slot[i] = "noone"; //creating all of the slots as empty
for(i = 0; i < totalSlots; i++) global.slotCount[i] = 0; //creating all of the slots as 0 items in them
for(i = 0; i < totalSlots; i++) coordX[i] = 0; //creating the coordinate x system
for(i = 0; i < totalSlots; i++) coordY[i] = 0; //creating the coordinate y system

canDraw = false; //can draw the inventory

//global.points = 5;
quickSwapCount = 15;

initInventory(); //initializing all of the item values

spr = nosprite; //we need this sprite for moving objects around the inventory
slotTaken = -1; //used for trasferring objects in inventory