var cellWidth = 12;
var cellHeight = 12;
var hCells = 5;
var vCells = 3;
var hGap = 128;
var vGap = 72;

if(canDraw) //drawing inventory
{
    draw_set_color(c_gray);
    draw_set_alpha(0.8);
    
    var startX = __view_get( e__VW.XView, 0 )+64;
    var startY = __view_get( e__VW.YView, 0 )+36;
    var width = 128;
    var height = 72;
    
    draw_rectangle(startX, startY, startX+width, startY+height, false); //576x288
    
    for(xx = startX+8; xx < startX+width; xx += 25)
    for(yy = startY+24; yy < startY+height; yy += 16){
 
           coordX[curSlot] = xx;
           coordY[curSlot] = yy;
           
           draw_set_color(c_dkgray);
           draw_set_alpha(0.95);
           draw_rectangle(xx, yy, xx+cellWidth, yy+cellHeight, false);
           
           //drawing number
           draw_set_color(c_white);
           if(global.slotCount[curSlot] > 0)
           draw_text(xx, yy, string_hash_to_newline(string(global.slotCount[curSlot]))); 
           
           curSlot++;
           if(curSlot >= totalSlots-4) curSlot = 0;
    }
    
    //quick swap 1 slot
    coordX[15] = __view_get( e__VW.XView, 0 )+ 80;
    coordY[15] = __view_get( e__VW.YView, 0 )+ 42;
    
    //quick swap 2 slot
    coordX[16] = __view_get( e__VW.XView, 0 )+ 110;
    coordY[16] = __view_get( e__VW.YView, 0 )+ 42;
    
    //quick swap 3 slot
    coordX[17] = __view_get( e__VW.XView, 0 )+ 140;
    coordY[17] = __view_get( e__VW.YView, 0 )+ 42;
    
    //equip slot
    coordX[18] = __view_get( e__VW.XView, 0 )+ 170;
    coordY[18] = __view_get( e__VW.YView, 0 )+ 42;
    
    draw_set_color(c_white);
    draw_set_alpha(1);
    if(global.slotCount[18] > 0)
    draw_text(coordX[18], coordY[18], string_hash_to_newline(string(global.slotCount[18]))); 
    
    //quick swap slots
    draw_set_color(make_color_rgb(158,155,252));
    draw_rectangle(coordX[15], coordY[15], coordX[15]+cellWidth, coordY[15]+cellHeight, false);
    draw_set_color(c_black);
    draw_rectangle(coordX[15], coordY[15], coordX[15]+cellWidth, coordY[15]+cellHeight, true);
    
    draw_set_color(make_color_rgb(158,155,252));
    draw_rectangle(coordX[16], coordY[16], coordX[16]+cellWidth, coordY[16]+cellHeight, false);
    draw_set_color(c_black);
    draw_rectangle(coordX[16], coordY[16], coordX[16]+cellWidth, coordY[16]+cellHeight, true);
 
    draw_set_color(make_color_rgb(158,155,252));
    draw_rectangle(coordX[17], coordY[17], coordX[17]+cellWidth, coordY[17]+cellHeight, false);
    draw_set_color(c_black);
    draw_rectangle(coordX[17], coordY[17], coordX[17]+cellWidth, coordY[17]+cellHeight, true);
    
    //equip slot
    draw_set_color(c_gray);
    draw_rectangle(coordX[18], coordY[18], coordX[18]+cellWidth, coordY[18]+cellHeight, false);
    draw_set_color(c_black);
    draw_rectangle(coordX[18], coordY[18], coordX[18]+cellWidth, coordY[18]+cellHeight, true);
    
    updateInv(cellWidth, cellHeight);
}

draw_set_alpha(1);
draw_set_color(c_white);


