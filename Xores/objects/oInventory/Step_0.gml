/// @description drawing the gui switch

if(keyboard_check_pressed(ord("T")) && !canDraw){
canDraw = true;
return 0;
}
if(keyboard_check_pressed(ord("T")) && canDraw){
canDraw = false;
spr = nosprite;
return 0;
}

///quick swap 

if(keyboard_check_pressed(ord("E"))) //equipping towards right
{
    var slot15 = global.slot[15];
    var slot16 = global.slot[16];
    var slot17 = global.slot[17];
    
    var slot15Count = global.slotCount[15];
    var slot16Count = global.slotCount[16];
    var slot17Count = global.slotCount[17];
    
    var equipSlot = global.slot[18];
    var equipSlotCount = global.slotCount[18];

    global.slot[18] = slot17;
    global.slotCount[18] = slot17Count;
    
    global.slot[17] = slot16;
    global.slotCount[17] = slot16Count;
    
    global.slot[16] = slot15;
    global.slotCount[16] = slot15Count;
    
    global.slot[15] = equipSlot;
    global.slotCount[15] = equipSlotCount;
    
}else if(keyboard_check_pressed(ord("Q"))) //equipping towards left
{

    var slot15 = global.slot[15];
    var slot16 = global.slot[16];
    var slot17 = global.slot[17];
    
    var slot15Count = global.slotCount[15];
    var slot16Count = global.slotCount[16];
    var slot17Count = global.slotCount[17];
    
    var equipSlot = global.slot[18];
    var equipSlotCount = global.slotCount[18];

    global.slot[18] = slot15;
    global.slotCount[18] = slot15Count;
    
    global.slot[17] = equipSlot;
    global.slotCount[17] = equipSlotCount;
    
    global.slot[16] = slot17;
    global.slotCount[16] = slot17Count;
    
    global.slot[15] = slot16;
    global.slotCount[15] = slot16Count;
}


