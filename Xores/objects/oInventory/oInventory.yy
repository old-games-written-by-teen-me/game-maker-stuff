{
    "id": "2a24c897-72e3-4b51-8f0a-1211be6ca85c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInventory",
    "eventList": [
        {
            "id": "9e049e7f-63fc-40f6-a1c4-60deba5babaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a24c897-72e3-4b51-8f0a-1211be6ca85c"
        },
        {
            "id": "81da1259-f754-4acf-8d19-5d32443a539f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2a24c897-72e3-4b51-8f0a-1211be6ca85c"
        },
        {
            "id": "17a8d6bb-b5f9-4b8e-a2a8-694de9f9a69c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a24c897-72e3-4b51-8f0a-1211be6ca85c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}