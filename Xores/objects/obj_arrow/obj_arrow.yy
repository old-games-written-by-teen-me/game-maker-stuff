{
    "id": "520db01b-404e-4925-8a4e-417c88d5dac1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_arrow",
    "eventList": [
        {
            "id": "aa614b6f-9e51-4201-8e9d-64c3d564504c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "520db01b-404e-4925-8a4e-417c88d5dac1"
        },
        {
            "id": "5927e4c1-08e3-40ae-aeb8-ced2eae05388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "520db01b-404e-4925-8a4e-417c88d5dac1"
        },
        {
            "id": "4b9f47b1-048a-42e7-864c-5afab40951be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "520db01b-404e-4925-8a4e-417c88d5dac1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "243c040a-3e68-49a6-9745-712c19f4baf0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -1,
            "y": -1
        },
        {
            "id": "b1e99c8a-0512-4b0f-9ffc-18ab1cbc75b0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": -1
        },
        {
            "id": "9761fe6a-2e24-4c4e-a1e6-d4396ee13099",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 7,
            "y": 4
        },
        {
            "id": "ba248132-1f47-4dd7-889c-6b7a1aa4c94a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": -1,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e14c5ac6-e915-42fc-8a4a-e75cea3a9406",
    "visible": true
}