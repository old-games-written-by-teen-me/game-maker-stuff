{
    "id": "111f4444-93a7-4423-9566-46bc9106c721",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boulder_1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22b58df1-d18e-4f47-bdb0-bc8226480d1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "963f23d1-5e4f-46e9-bd95-2d8d8e4e0004",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 14
        },
        {
            "id": "101ee580-7d4b-471f-91b6-a46a24e14000",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 14
        },
        {
            "id": "3ee068a1-d529-4943-bef2-77f651fd9c6b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 38,
            "y": 31
        },
        {
            "id": "6bfc46aa-59df-4971-996a-072a8b6df661",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 31
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ffafb04-2854-43ac-8249-c6cdd7309050",
    "visible": true
}