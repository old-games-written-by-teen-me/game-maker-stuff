/// @description Initilize the slime
event_inherited();
image_speed = .3;
spd = 1.5;
hp = 5;
state = scr_enemy_idle_state;
alarm[0] = room_speed*irandom_range(2, 5);
sight = 128;
xaxis = 0;
yaxis = 0;
level = 1;
movement = IDLE;
face = RIGHT;

//Create the sprite array
sprite[RIGHT, MOVE] = spr_chinlin_right_run;
sprite[UP, MOVE] = spr_chinlin_up_run;
sprite[DOWN, MOVE] = spr_chinlin_down_run;
sprite[LEFT, MOVE] = spr_chinlin_right_run;

sprite[RIGHT, ATTACK] = spr_chinlin_right_run;
sprite[UP, ATTACK] = spr_chinlin_right_run;
sprite[DOWN, ATTACK] = spr_chinlin_right_run;
sprite[LEFT, ATTACK] = spr_chinlin_right_run;

sprite[RIGHT, IDLE] = spr_chinlin_right_stand;
sprite[UP, IDLE] = spr_chinlin_up_stand;
sprite[DOWN, IDLE] = spr_chinlin_down_stand;
sprite[LEFT, IDLE] = spr_chinlin_right_stand;
