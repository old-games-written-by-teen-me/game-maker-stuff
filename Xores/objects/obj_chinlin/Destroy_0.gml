/// @description  The loot
instance_create (x+random_range(-4, 4), y+random_range(-4, 4), obj_expr);
part_particles_create(global.ps, x, y, pt_blood, 20);
//Drop a health pack
if (scr_chance(.1)) {
    instance_create(x+random_range(-4, 4), y+ random_range(-4, 4), obj_health);
}

