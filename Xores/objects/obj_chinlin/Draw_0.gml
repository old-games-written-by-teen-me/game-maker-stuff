/// @description Insert description here
event_inherited();
//Draw Shadows
shdw_sprs[RIGHT, MOVE] = shdw_chinlin_right_run;
shdw_sprs[UP, MOVE] = shdw_chinlin_up_run;
shdw_sprs[DOWN, MOVE] = shdw_chinlin_down_run;
shdw_sprs[LEFT, MOVE] = shdw_chinlin_right_run;

shdw_sprs[RIGHT, ATTACK] = shdw_chinlin_right_run;
shdw_sprs[UP, ATTACK] = shdw_chinlin_right_run;
shdw_sprs[DOWN, ATTACK] = shdw_chinlin_right_run;
shdw_sprs[LEFT, ATTACK] = shdw_chinlin_right_run;

shdw_sprs[RIGHT, IDLE] = shdw_chinlin_right_stand;
shdw_sprs[UP, IDLE] = shdw_chinlin_up_stand;
shdw_sprs[DOWN, IDLE] = shdw_chinlin_down_stand;
shdw_sprs[LEFT, IDLE] = shdw_chinlin_right_stand;

draw_sprite(shdw_sprs[face, movement], image_index, x, y);