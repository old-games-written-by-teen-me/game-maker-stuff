{
    "id": "5aa38aba-c328-4498-87d4-a980075a8179",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chinlin",
    "eventList": [
        {
            "id": "b8ce71ed-ddc1-4816-9458-cdb6601468fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "fa2d0c6a-2264-4f1e-8f4c-ba74065b70a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "cf6dd4e7-6166-40b4-bfc5-b903b3691c82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "6824c2f8-f916-420c-a208-5bc9da621aaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "2ff9c4b5-5b19-4c2b-a72d-468ebc6193aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "80aa4d39-5bed-48ad-99dd-af3c2e0c25e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "f5aac23b-5134-45f5-99f6-07a4183db2c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "f3aa7dd1-7d4f-427e-b58d-cc5e799d5469",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a89d4848-2146-4513-96c8-835f2174e5ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "305cdd30-e9c0-4a87-8f58-76999294d9ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "97aac934-f2f2-490a-8d39-6c359f6b397c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        },
        {
            "id": "51ab9742-1db3-40cf-b76c-da067d58ee23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "5aa38aba-c328-4498-87d4-a980075a8179"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.3,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 5,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "b4178a55-d558-4d08-ba64-2284973b9103",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 14
        },
        {
            "id": "f9e0780c-f3f4-4ac1-a05e-66edc0958272",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 20,
            "y": 14
        },
        {
            "id": "e9c07151-1d77-4ec8-be7f-6e9499c39a9d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 20,
            "y": 23
        },
        {
            "id": "9ad23e7b-6279-413d-8ebb-f94ce65dae39",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 23
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9cc18ff9-642b-4219-afa7-25347658c732",
    "visible": true
}