/// @description Damage the player
if (other.id != creator) {
    obj_player_stats.hp -= damage;
	part_particles_create(global.ps, x, y, pt_blood, 10);
	obj_view.shaking = 20;
    
    ///Apply the knockback
    if (instance_exists (creator)) {
        var dir = point_direction (creator.x, creator.y, other.x, other.y);
    } else {
        var dir = point_direction (x, y, other.x, other.y);
    }
    var xforce = lengthdir_x (knockback, dir);
    var yforce = lengthdir_y (knockback, dir);
    with (other) {
        physics_apply_impulse(x, y, xforce, yforce);
    }
}

