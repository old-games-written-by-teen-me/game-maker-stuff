{
    "id": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_damage",
    "eventList": [
        {
            "id": "70f72343-0376-4ef7-900e-598f5ca32386",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4fd4ce39-2f93-4558-85d6-686f6c61259c"
        },
        {
            "id": "d1e115fe-faa4-4993-9621-192723e4828d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4fd4ce39-2f93-4558-85d6-686f6c61259c"
        },
        {
            "id": "7c9856d6-b310-4f68-9ee6-4e699cf84508",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4fd4ce39-2f93-4558-85d6-686f6c61259c"
        },
        {
            "id": "cdbb4461-55d5-4463-a7c6-6abec5298795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a89d4848-2146-4513-96c8-835f2174e5ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4fd4ce39-2f93-4558-85d6-686f6c61259c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "ab4fa232-b673-4532-8935-d52055365042",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        },
        {
            "id": "b8900643-a945-4203-bb44-a53f868d5103",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "284cfe25-5b79-4cd1-884c-c409e74eaa6b",
    "visible": false
}