{
    "id": "58512e27-973d-4dd0-8e10-03e5d37108f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialogue",
    "eventList": [
        {
            "id": "2c416d31-a080-4f4b-b598-cc40832bcad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "58512e27-973d-4dd0-8e10-03e5d37108f5"
        },
        {
            "id": "34a58b35-bd1c-477c-8309-beb6bfe8d310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "58512e27-973d-4dd0-8e10-03e5d37108f5"
        },
        {
            "id": "5bcd0552-7aad-4467-a3fd-9e4bc53717d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "58512e27-973d-4dd0-8e10-03e5d37108f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4970af00-dd45-4268-8085-0e7a16d4f4bf",
    "visible": true
}