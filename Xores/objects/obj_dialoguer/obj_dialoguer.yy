{
    "id": "a2ba9c42-0ba8-4c2d-9249-3d4f7165a3a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialoguer",
    "eventList": [
        {
            "id": "d1deda4a-3983-48c3-b094-889a6364c29f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2ba9c42-0ba8-4c2d-9249-3d4f7165a3a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "460523a2-85ea-4299-bcc5-4edae1fd74b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "93e7f39c-43f6-42cc-804f-cd6e5db83684",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "0a78418e-f3b9-4c81-a366-bd1d0449b8ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "aa000c20-b0b4-471a-96a6-4b92e67c0906",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "ac0896b3-7485-49b2-bc05-34b63beffdbe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "058e44b4-6ca2-43fa-b05f-dcf236c413f9",
    "visible": false
}