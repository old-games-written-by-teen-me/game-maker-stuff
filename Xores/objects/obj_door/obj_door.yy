{
    "id": "83540b27-a2f8-4947-8068-13033ed2e6f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_door",
    "eventList": [
        {
            "id": "40678382-df61-49ab-aa0a-048fa964aa98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "83540b27-a2f8-4947-8068-13033ed2e6f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": true,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "4bb52b34-5ca1-41a3-b178-41d09285ab36",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "0b546516-7b41-4d7b-aad9-85e609b4fb2a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        },
        {
            "id": "1fcbace9-6524-47bd-abd7-b2d615f48828",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "fc55ca70-90da-41e3-9404-5a59fbc2bcc0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "166daf08-8ac4-4d92-8c11-697e6d40d24a",
    "visible": false
}