/// @description Initilize the slime
visible = true;
event_inherited();
image_speed = .1;
spd = 2;
state = scr_enemy_idle_state;
alarm[0] = room_speed*irandom_range(2, 5);
sight = 64;
xaxis = 0;
yaxis = 0;
slime_blood_color = make_color_hsv(138, 56, 78);
level = 1;
image_alpha = .7;