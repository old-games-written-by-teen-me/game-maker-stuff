/// @description Draw self over the shadow
if (flash > 0) {
	shader_set(shd_flash);
	draw_self();
	shader_reset();
	flash--;
} else draw_self();
if (global.shadows == true)
	draw_sprite_ext(shdw_enemy_slime, image_index, x, y, image_xscale, 1, 1, c_white, 255);