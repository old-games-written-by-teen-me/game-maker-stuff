{
    "id": "d21ba479-ba4c-4093-8f10-11b1082e1df0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy_slime",
    "eventList": [
        {
            "id": "3aafffbe-a1b3-423b-b387-069364c32e93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "11a031f7-90b7-4ed4-a51f-72aae45b0911",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "d376c5f6-fe9d-4050-b102-0a7e97713a51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "881e0f38-31e8-45c0-85c0-1888c8496d74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "05ec84e2-b119-4772-adb1-ed737b520d82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "a0a89f54-5ed1-4ebf-9c14-35abc7ab4ea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "a5ce5232-57a3-4cd6-be9b-b10e881a3ef5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "ba559968-9998-43f0-9340-aebba5966af7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a89d4848-2146-4513-96c8-835f2174e5ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "30bffa65-6120-49a4-90b4-038b905009d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        },
        {
            "id": "3384bc8c-a581-4d4f-9456-7cd10481b838",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d21ba479-ba4c-4093-8f10-11b1082e1df0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 5,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "10b0b330-4f51-4786-bdc1-487ae01e86dd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 9
        },
        {
            "id": "f3e7d72b-5cf8-42ea-b179-9d500da8f1dc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 19,
            "y": 9
        },
        {
            "id": "bcf8b4c0-59c1-454b-818d-bab66fde4a3f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 19,
            "y": 14
        },
        {
            "id": "0e7e2e9c-a224-4145-b5a6-de8f12eba20d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 14
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec8bc24f-0d63-4200-80f3-2cfaa64ea7f8",
    "visible": true
}