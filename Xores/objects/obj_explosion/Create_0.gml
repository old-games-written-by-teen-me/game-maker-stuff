///@description Initilize
obj_view.shake = 10;
speed = random_range(1, 5);
friction = .25;
direction = random(360);

part_particles_create(global.ps, x-8+random(16), y-8+random(16), obj_ps.explo_particle_part, 1);
part_particles_create(global.ps, x-8+random(16), y-8+random(16), obj_ps.smoke_particle_part, 1);
