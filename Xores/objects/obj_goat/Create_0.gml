/// @description Initilize the slime
event_inherited();
image_speed = .6;
spd = 1.5;
hp = 7;
state = scr_enemy_idle_state;
alarm[0] = room_speed*irandom_range(2, 5);
sight = 128;
xaxis = 0;
yaxis = 0;
level = 1;
movement = IDLE;
face = RIGHT;

//Create the sprite array
sprite[RIGHT, MOVE] = spr_goat_run_right;
sprite[UP, MOVE] = spr_goat_run_up;
sprite[DOWN, MOVE] = spr_goat_run_down;
sprite[LEFT, MOVE] = spr_goat_run_right;

sprite[RIGHT, ATTACK] = spr_goat_run_right;
sprite[UP, ATTACK] = spr_goat_run_right;
sprite[DOWN, ATTACK] = spr_goat_run_right;
sprite[LEFT, ATTACK] = spr_goat_run_right;

sprite[RIGHT, IDLE] = spr_goat_stand_right;
sprite[UP, IDLE] = spr_goat_stand_up;
sprite[DOWN, IDLE] = spr_goat_stand_down;
sprite[LEFT, IDLE] = spr_goat_stand_right;
