/// @description Run the current state
visible = 1;
event_inherited();
script_execute(state);
if (alarm[3] = 0) {
    room_speed = normal_room_speed;
}

sprite_index = sprite[face, movement];
