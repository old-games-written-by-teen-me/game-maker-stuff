{
    "id": "b86918b2-7546-42f0-b421-7b01a5ef64bc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_goat",
    "eventList": [
        {
            "id": "53c91083-e2dd-4462-8461-666dd3c87a49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "7dad6d55-3955-48c4-8941-662fd0c5e1e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "93efdc5e-3759-4933-a7f1-0096d479af23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "a6bb3377-dd8f-4b05-96e5-00b3c6ef909d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "70ac18a2-9d74-45a7-bdff-83c8ca23e831",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "fe88bbdd-3333-4e1f-8a99-cc0711074d03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "96108c85-bfb1-4e4c-8e3e-f59693c94dbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "645ea906-8324-4cb6-96b0-3133180e9b44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a89d4848-2146-4513-96c8-835f2174e5ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        },
        {
            "id": "8b72a779-b0ef-43d7-8e60-a7d05513d10d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b86918b2-7546-42f0-b421-7b01a5ef64bc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.3,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 5,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "25c1107a-fdc8-4e70-83c3-efc0f2bcef8d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14,
            "y": 34
        },
        {
            "id": "579ceaa9-3e0e-41e0-ab04-452a4a06cd79",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 34
        },
        {
            "id": "376219bd-9e79-4c6f-90f3-03c1db28ee86",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 45
        },
        {
            "id": "97c6a58c-1da8-49be-b599-e486b72531c7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 14,
            "y": 45
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
    "visible": true
}