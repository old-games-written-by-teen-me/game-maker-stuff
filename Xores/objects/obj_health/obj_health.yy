{
    "id": "c9e4c460-7b48-4314-908b-302f93af8061",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_health",
    "eventList": [
        {
            "id": "05812533-d59c-4408-96e6-c86077df272f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9e4c460-7b48-4314-908b-302f93af8061"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "8f50cc49-7052-42b3-abfa-65189da2db25",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "5043751e-b395-4205-9783-92c503a8e148",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75b009db-9980-46fe-98f0-c8b1c00993d3",
    "visible": true
}