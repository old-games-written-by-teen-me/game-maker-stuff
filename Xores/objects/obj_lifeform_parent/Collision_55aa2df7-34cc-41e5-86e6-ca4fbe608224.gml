/// @description Collide
if (other.id == obj_player) {
current_face = face;
var xdir = lengthdir_x(x*0, current_face);
var ydir = lengthdir_y(y*0, current_face);
var collider = instance_place(x+xdir, y+ydir, other);
if (collider == noone) {
    if (other.direction*90 == UP && self.direction == UP){
        image_index = 0;
        image_speed = 0;
    }
}
}
