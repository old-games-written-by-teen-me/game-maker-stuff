/// @description Draw Self, flash, etc
draw_self();
if (flash > 0) {
	shader_set(shd_flash);
	image_alpha = 1;
	draw_self();
	shader_reset();
	flash--;
} else {
	image_alpha = img_alpha;	
}