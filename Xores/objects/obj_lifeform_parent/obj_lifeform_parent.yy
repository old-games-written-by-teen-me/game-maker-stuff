{
    "id": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lifeform_parent",
    "eventList": [
        {
            "id": "e2ea8ae6-8786-489d-857e-da42b9e991e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "d6e622c0-18bf-4cef-a999-5e2c59d91651",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "c26482ca-ab76-4770-a36e-47e913cbad2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "55aa2df7-34cc-41e5-86e6-ca4fbe608224",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "22b58df1-d18e-4f47-bdb0-bc8226480d1b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "8b83c54f-47fb-48ae-b1dc-be75cb11888c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "d3fce393-9e1b-4cf3-aa06-889617886608",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "7c5345fd-d536-4887-b2cb-bdf764746bbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        },
        {
            "id": "a67049ba-e67a-40ea-8217-bc99131f1f44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}