/// @description Control the menu
var down_key = keyboard_check(ord("S"));
var up_key = keyboard_check(ord("W"));
var menu_select_key = keyboard_check(vk_space);
if (alarm[0] <= 0) {
    if (down_key) {
        if (global.menu_index < array_length_1d(option)-1) {
            global.menu_index++;
        } else {
            global.menu_index = 0;
        }
        alarm[0] = room_speed/6;
    }
    
    if (up_key) {
        if (global.menu_index > 0) {
            global.menu_index--;
        } else {
            global.menu_index = array_length_1d(option)-1;
        }
        alarm[0] = room_speed/6;
    }
    if (menu_select_key) {
        switch (global.menu_index) {
            case 0:
                room_goto(rm_monter_s_house_outside);
                break;
            
            case 1:
                
                break;
            
            case 2:
            
                break;
                
            case 3:
                game_end();
                break;
                
            default:
                break;
        }
    }
}
switch (global.menu_index) {
    case 0:
        option[0].selected = true;
        option[1].selected = false;
        option[2].selected = false;
        option[3].selected = false;
            break;
            
    case 1:
        option[0].selected = false;
        option[1].selected = true;
        option[2].selected = false;
        option[3].selected = false;
            break;
            
    case 2:
        option[0].selected = false;
        option[1].selected = false;
        option[2].selected = true;
        option[3].selected = false;
            break;
                
    case 3:
        option[0].selected = false;
        option[1].selected = false;
        option[2].selected = false;
        option[3].selected = true;
            break;

}

