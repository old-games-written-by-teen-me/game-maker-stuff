{
    "id": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_main_menu",
    "eventList": [
        {
            "id": "1385314f-a8f2-4cd3-88e5-b0f5f4f8970c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb"
        },
        {
            "id": "9db2a902-2eed-4a72-96bc-d2a714cb2703",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb"
        },
        {
            "id": "8833b6a6-524e-48c2-bcff-6073f4e25b08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb"
        },
        {
            "id": "df8ba572-b2bc-4e68-8b16-96e876538a88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb"
        },
        {
            "id": "c62df231-e093-425e-9c85-1889c4ae9bd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "12d3607b-4b6a-4e5d-99e7-bb00b52ecdbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}