{
    "id": "c8c28c21-b221-4aa0-8252-6807d59b240c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu_button",
    "eventList": [
        {
            "id": "3aec65ee-dcb7-4b4a-bdbc-8b9b4e529516",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c8c28c21-b221-4aa0-8252-6807d59b240c"
        },
        {
            "id": "8e2a4818-3239-48b9-b722-b9a739616295",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c8c28c21-b221-4aa0-8252-6807d59b240c"
        },
        {
            "id": "87a6ee67-012e-48bd-9dd4-7ec36d07b1b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c8c28c21-b221-4aa0-8252-6807d59b240c"
        },
        {
            "id": "fd85942f-dbe7-4942-8cdf-c3d48d537ab9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c8c28c21-b221-4aa0-8252-6807d59b240c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2988d65-a09c-4f60-8a7a-310cc9d5929f",
    "visible": true
}