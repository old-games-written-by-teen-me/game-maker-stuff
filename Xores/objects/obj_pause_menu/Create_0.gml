/// @description  Initilize the menu
title = "Paused";

// Menu index
menu_index = 0;
draw_set_font(fnt_menu);

option[0] = instance_create(80, 42, obj_menu_button);
option[1] = instance_create(80, 74, obj_menu_button);
option[2] = instance_create(80, 106, obj_menu_button);
option[3] = instance_create(80, 138, obj_menu_button);


option[0].text = "Resume";
option[0].xx = 25;
option[0].yy = 6;

option[1].text = "Load Game";
option[1].xx =25;
option[1].yy =6;

option[2].text = "Options";
option[2].xx =32;
option[2].yy =6;

option[3].text = "Main Menu";
option[3].xx =38;
option[3].yy =6;


