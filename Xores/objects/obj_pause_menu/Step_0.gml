/// @description Control the menu
if (alarm[0] <= 0) {
    if (obj_input.down_key) {
        if (menu_index < array_length_1d(option)-1) {
            menu_index++;
        } else {
            menu_index = 0;
        }
        alarm[0] = room_speed/6;
    }
    
    if (obj_input.up_key) {
        if (menu_index > 0) {
            menu_index--;
        } else {
            menu_index = array_length_1d(option)-1;
        }
        alarm[0] = room_speed/6;
    }
    if (obj_input.menu_select_key) {
        switch (menu_index) {
            case 0:
                room_goto(obj_player_stats.previous_room);
                break;
            
            case 1:
                scr_save_game();
                break;
            
            case 2:
                break;
                
            case 3:
                game_end();
                break;
                
            default:
                break;
        }
    }
}
switch (menu_index) {
    case 0:
        option[0].selected = true;
        option[1].selected = false;
        option[2].selected = false;
        option[3].selected = false;
            break;
            
    case 1:
        option[0].selected = false;
        option[1].selected = true;
        option[2].selected = false;
        option[3].selected = false;
            break;
            
    case 2:
        option[0].selected = false;
        option[1].selected = false;
        option[2].selected = true;
        option[3].selected = false;
            break;
                
    case 3:
        option[0].selected = false;
        option[1].selected = false;
        option[2].selected = false;
        option[3].selected = true;
            break;

}

