{
    "id": "0f367bc6-58b0-4fae-b52f-ea82bf2e3ee8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_pause_menu",
    "eventList": [
        {
            "id": "dcb197f3-9ac4-4de7-81e5-1fc0486021de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f367bc6-58b0-4fae-b52f-ea82bf2e3ee8"
        },
        {
            "id": "917ee47b-264e-499a-baea-0877355c9b06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0f367bc6-58b0-4fae-b52f-ea82bf2e3ee8"
        },
        {
            "id": "0631ec97-c931-4136-a6a7-e5e0253a819d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f367bc6-58b0-4fae-b52f-ea82bf2e3ee8"
        },
        {
            "id": "7b68ace9-75ac-4ec2-bc71-821a2e3c87e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0f367bc6-58b0-4fae-b52f-ea82bf2e3ee8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}