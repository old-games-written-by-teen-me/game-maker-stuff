/// @description Go through the door
if (room_exists(other.new_room)) {
    if(other.door_level <= obj_player_stats.level) {
        if (other.requires_interact = true) {
            if (obj_input.interact_key) {
				room_goto(other.new_room);
                x = other.new_x;
                y = other.new_y;
            }
        } else {
             room_goto(other.new_room);
             x = other.new_x;
             y = other.new_y;
        }
    }
}

