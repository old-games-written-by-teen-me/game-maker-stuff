/// @description  Initilize the player
depth = 0;
sprite_index = spr_player_bottom_right;
spr_index = spr_player_right;
scr_part_blood_init();
event_inherited();
able_to_dash = true;
moving_state = "none";
able_to_move = true;
image_spd = .60;
looking_where_moving = true;
moving = false;//step event 2
hp = 3;
spd = 2.5;
hspd = 0;
vspd = 0;
xaxis = 0;
yaxis = 0;
len = 0;
dir = 0;
attacked = false;
image_speed = 0;
state = scr_move_state;
face = RIGHT;
current_face = face;
item_index = 1;
door_level = 0;
obj_player.persistent = true;
reply = 0;
movement = MOVE;
//timeline_index = tl_1_test;
//timeline_loop = false;
//timeline_running = true;

//Create the sprite array
sprite[RIGHT, MOVE] = spr_player_bottom_right;
sprite[UP, MOVE] = spr_player_bottom_up;
sprite[DOWN, MOVE] = spr_player_bottom_down;
sprite[LEFT, MOVE] = spr_player_bottom_left;

sprite[RIGHT, ATTACK] = spr_player_attack_right;
sprite[UP, ATTACK] = spr_player_attack_up;
sprite[DOWN, ATTACK] = spr_player_attack_down;
sprite[LEFT, ATTACK] = spr_player_attack_left;
