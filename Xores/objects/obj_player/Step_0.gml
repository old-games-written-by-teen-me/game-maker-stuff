/// @description  Move the player in the step event
depth = -y;

script_execute(state);

//Toggle inventory
/*
if(obj_input.inventory_key) {
	var xx = x;
	var yy = y;
    if (instance_exists(obj_inventory)) {
		with (obj_inventory) {
			if (can_draw) {
				can_draw = false;	
			} else {
				can_draw = true;
			}
		}
	} else {
		instance_create(x, y, obj_inventory);
		show_debug_message("No object for inventroy");
	}
}
if instance_exists(obj_inventory)
	if (obj_inventory.visible) item_index = 3;
*/
///Control the sprite
if (movement == MOVE) {
    sprite_index = sprite[face, movement];
}
if (movement == ATTACK) {
    sprite_index = sprite[face, movement];
}
//Check for death
if (obj_player_stats.hp <= 0) {
    instance_destroy();
}
//Item index

//1Button
if (instance_exists(obj_player)) {
    if(obj_input.item_cycle_key) {
		item_index++
    }
	if (item_index > 3) {
		item_index = 0;	
	}
	
	//Buttons
    if (obj_input.item_index_1_key) {
        item_index = 1;
    }
    
    if (obj_input.item_index_2_key) {
        item_index = 2;
    }
	
	if (obj_input.item_index_3_key) {
        item_index = 3;
    }
}
if (keyboard_check_pressed(ord("L"))) {
    if (able_to_move) {
        able_to_move = false;
    } else if (!able_to_move) {
        able_to_move = true;
    }
}


///Cutscene Moves
switch(moving_state){

    case "move_up":
        phy_position_y -= moving_spd;
        moving = true;
            
    case "move_down":
        phy_position_y += moving_spd;
        moving = true;
            
    case "move_right":
        phy_position_x -= moving_spd;
        moving = true;
            
    case "move_left":
        phy_position_x -= moving_spd;
        moving = true;

}
if (alarm[3] == 0) {
    moving_state = "wait";
    moving = false;
}

///If not able to move then do not move face
if (able_to_move) {
    current_face = face;
}
if (!able_to_move) {
    face = current_face;  
}


///Direction to Mouse
dir_to_mouse = point_direction(x, y, mouse_x, mouse_y);

//Buttom Sprite to Full Sprite
switch (sprite_index) {
	
	case spr_player_bottom_right:
		spr_index = spr_player_right;
			break;
			
	case spr_player_bottom_left:
		spr_index = spr_player_left;
			break;
			
	case spr_player_bottom_up:
		spr_index = spr_player_up;
			break;
			
	case spr_player_bottom_down:
		spr_index = spr_player_down;
			break;
			
}

if (keyboard_check(vk_alt)) {
	create_explosion(x, y);	
}
