{
    "id": "a89d4848-2146-4513-96c8-835f2174e5ae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "085b9777-4d0e-4fe6-9337-ab010e6b79b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "f3e54068-5a35-4601-954b-e910311c6b87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "de1a52aa-8108-4229-b0ca-851efad56a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "cfc7f4ac-549c-4287-9bb7-0cbfed4c0e65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "195c930c-6d8b-4472-a763-780cef19ac0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "97374759-bdf4-4fe3-9015-cb8ce454771b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "7fd5f24f-b465-4428-a545-3be8e236333e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c9e4c460-7b48-4314-908b-302f93af8061",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "c38d85dd-f468-4c2b-8667-0461f0c1f6de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0cbbb70a-afa1-41c8-8f7a-81c72d779264",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "75d11629-4859-4ef5-ad30-ce6effac11bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "83540b27-a2f8-4947-8068-13033ed2e6f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "4ad8ccca-18de-4cc4-a48c-8262c955dba2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "fa8633fb-1b86-4e6f-9ec5-b5d084ef4347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "1c1fb373-affd-439c-a455-4392b7dd2940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "4a219a59-8c53-4862-9fca-0c92cb69ce3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "4e5dbb27-1533-465d-a4c1-de91a7c47fe5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        },
        {
            "id": "cf5651ad-9fce-4ba4-be21-67778db6e169",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a88727e3-c8ec-4f88-a5ff-fca28405c48a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a89d4848-2146-4513-96c8-835f2174e5ae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 5,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6cfbc2fd-0685-4620-b2a7-79c9a45d7c7f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 10
        },
        {
            "id": "9427976c-c78a-445f-ada8-7edc580c7744",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 9,
            "y": 10
        },
        {
            "id": "159a8548-263d-4e56-805e-35ad1061344d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 9,
            "y": 18
        },
        {
            "id": "d1988fcf-bb9f-4c66-9a1c-aa140192c936",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 2,
            "y": 18
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
    "visible": true
}