/// @description Draw the stats
draw_sprite_ext(spr_health_bar, 1, 10, 15, hp/maxhp, 1, 0, c_white, 1);
draw_sprite(spr_health_bar_border, 1, 10, 15);

draw_sprite_ext(spr_stamina_bar, 1, 10, 30, stamina/maxstamina, 1, 0, c_white, 1);
draw_sprite(spr_stamina_bar_border, 1, 10, 30);

draw_text(32, 48, "LEVEL: " + string(level));