
///Control the stats
if (hp <= 0) {
    hp = 0;
}

//Stamina regen
if(room != rm_pause) {
    if (stamina < maxstamina and alarm[0] == -1) {
        stamina++;
    }
    
    if (!instance_exists(obj_player)) {
        if (keyboard_check(ord("R"))) {
            instance_create(260, 160, obj_player)
            hp = 5;
        }
    }
    visible = true
} else visible = false;

// Pause the game
if (obj_input.pause_key) {
    if (room != rm_pause and !room = rm_main_menu) {
        if (instance_exists(obj_player)) {
            obj_player.persistent = false;
        }
        previous_room = room;
        room_goto(rm_pause);
    }else {
        room_goto(previous_room);
    }
    obj_input.pause_key = false;
}

