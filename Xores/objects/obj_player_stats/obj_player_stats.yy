{
    "id": "1485f231-d7aa-4bb4-8211-27f13829029d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_stats",
    "eventList": [
        {
            "id": "5eb1ae66-c928-4998-b263-28af967769ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1485f231-d7aa-4bb4-8211-27f13829029d"
        },
        {
            "id": "52463e9e-b421-4eea-b02b-6feb6708c332",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1485f231-d7aa-4bb4-8211-27f13829029d"
        },
        {
            "id": "38744338-010c-419f-90c1-6df8cc0f61ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1485f231-d7aa-4bb4-8211-27f13829029d"
        },
        {
            "id": "9cbc3358-8a00-4302-a102-35944dcb6ae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "1485f231-d7aa-4bb4-8211-27f13829029d"
        },
        {
            "id": "b41eb0fb-a60d-464f-b5a5-215a9c8f5412",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1485f231-d7aa-4bb4-8211-27f13829029d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}