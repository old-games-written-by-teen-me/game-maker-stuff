/// @description Create fire Particles
if (!parted) {
	part_particles_create(global.ps, x, y, pt_fire, 25);
	alarm[1] = 1;
	parted = true;
}