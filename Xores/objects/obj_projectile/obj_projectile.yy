{
    "id": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_projectile",
    "eventList": [
        {
            "id": "614ea863-339b-4115-ad06-5e5366579c43",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "b9a2fbe4-4d07-4857-a004-848d519756b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "b44be7f2-3b80-41e1-bf7c-890799d41fd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "22b58df1-d18e-4f47-bdb0-bc8226480d1b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "812cfe64-62d8-4958-ac5a-d6f2a6a1c0a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "b271bdbd-1d82-4c06-8cb0-1f66f9daa463",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "74c814d4-c759-47e2-a8bc-6c081d729d59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        },
        {
            "id": "817a680b-4af1-4221-b6b7-7e7f6bbc2ee3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ca0cdef1-6482-44a8-94b2-c1f11a3de59b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0,
    "physicsDensity": 0.1,
    "physicsFriction": 0,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0,
    "physicsObject": true,
    "physicsRestitution": 0,
    "physicsSensor": true,
    "physicsShape": 0,
    "physicsShapePoints": [
        {
            "id": "baac7d93-a18d-47ce-b4b4-cbd62293a018",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 12
        },
        {
            "id": "dea2f1ae-7603-4280-8ba1-415817aef993",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 6,
            "y": 12
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "284cfe25-5b79-4cd1-884c-c409e74eaa6b",
    "visible": false
}