
{
    /// Create the particle system 
    global.ps = part_system_create();
    
    
    /// Initlize the particles
    scr_part_slime_blood_init();
	scr_part_blood_init();
	explo_particle_part = scr_create_part_type_sprite(spr_explo_part, true, 15, 20, .5, 1, -.01);
	smoke_particle_part = scr_create_part_type_sprite(spr_smoke_part, true, 15, 15, .5, 1, -.01);
	
}

