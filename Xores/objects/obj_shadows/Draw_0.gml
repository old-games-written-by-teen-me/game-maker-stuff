/// @description Draw all the shadows

//For player
if (instance_exists(obj_player))
switch(obj_player.face) {
	
	case RIGHT:
		draw_sprite_ext(shdw_player_right, obj_player.image_index, obj_player.x, obj_player.y, 1, 1, 1, c_white, 255);
			break;
			
	case LEFT:
		draw_sprite_ext(shdw_player_left, obj_player.image_index, obj_player.x, obj_player.y, 1, 1, 1, c_white, 255);
			break;
			
	case DOWN:
		draw_sprite_ext(shdw_player_down, obj_player.image_index, obj_player.x, obj_player.y, 1, 1, 1, c_white, 255);
			break;
			
	case UP:
		draw_sprite_ext(shdw_player_down, obj_player.image_index, obj_player.x, obj_player.y, 1, 1, 1, c_white, 255);
			break;
			
	default:
		show_error("ERROR: WRONG FACE/PLAYER(DRAWING_THE_SHADOW)", true);
			break;
}
