{
    "id": "0ed397d1-ff5f-4ea7-8455-908d8f241f11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shadows",
    "eventList": [
        {
            "id": "ccdd510a-6531-4be1-bf84-65e962fb78ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0ed397d1-ff5f-4ea7-8455-908d8f241f11"
        },
        {
            "id": "f0afb407-b785-4ea7-9e56-95f2c3c3b334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0ed397d1-ff5f-4ea7-8455-908d8f241f11"
        },
        {
            "id": "7c366d01-66e6-424a-96bc-872c239a0774",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0ed397d1-ff5f-4ea7-8455-908d8f241f11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}