{
    "id": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sign",
    "eventList": [
        {
            "id": "abff6f29-3672-4541-be97-b47139e95e92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38"
        },
        {
            "id": "deb2c72c-a8db-41ef-90bf-e27175aa4d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38"
        },
        {
            "id": "3ec176f4-7aa6-41d9-a0b3-b4ee16ed4e6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4fd4ce39-2f93-4558-85d6-686f6c61259c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38"
        },
        {
            "id": "4330ddff-d59c-4a32-bb1c-9c6cf89b7622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38"
        },
        {
            "id": "05caa6fc-84b0-4168-84f6-d7c303bedfe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "73e8d590-8d3e-4ffa-9056-f92f26a4bdca",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3c0fe8b4-5e6c-4702-920f-4e441d15fa38"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "460523a2-85ea-4299-bcc5-4edae1fd74b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "34caeb0c-92a7-4f3c-a033-6f760e70bea4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 12
        },
        {
            "id": "b517fda5-1ac1-484c-8aee-14cbca693266",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 12
        },
        {
            "id": "516bec21-e86b-4a08-8d92-c8ba0ff082ba",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "15fea3cc-bf24-44b7-bd0c-700d6b9b994b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0028c9e5-6603-4170-b0bd-a8a957e88542",
    "visible": true
}