{
    "id": "60bdb9f9-da3c-48d2-ae8f-8ff932915329",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_silhouette",
    "eventList": [
        {
            "id": "dc67dca2-0d87-460c-89fa-40e9a5205633",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "60bdb9f9-da3c-48d2-ae8f-8ff932915329"
        },
        {
            "id": "c175c6ec-a871-4951-a8bd-135c3243e236",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "60bdb9f9-da3c-48d2-ae8f-8ff932915329"
        },
        {
            "id": "f1add881-20e1-47f8-9829-12459aeacb51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "60bdb9f9-da3c-48d2-ae8f-8ff932915329"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}