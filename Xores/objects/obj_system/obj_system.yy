{
    "id": "9edd6091-5ed3-45e8-bcd7-b0f733f8cfae",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_system",
    "eventList": [
        {
            "id": "174187bb-6901-4bbb-8bac-8305c31aee34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 104,
            "eventtype": 9,
            "m_owner": "9edd6091-5ed3-45e8-bcd7-b0f733f8cfae"
        },
        {
            "id": "cf59a7ec-9ee1-4460-99f3-52b3ee461b52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "9edd6091-5ed3-45e8-bcd7-b0f733f8cfae"
        },
        {
            "id": "0857da2d-de64-4f7b-b60c-7f1a9fc1f040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9edd6091-5ed3-45e8-bcd7-b0f733f8cfae"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}