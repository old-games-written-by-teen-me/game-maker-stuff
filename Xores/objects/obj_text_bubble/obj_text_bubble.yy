{
    "id": "1c7be572-d244-4625-9a50-d7f5b8e517c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_text_bubble",
    "eventList": [
        {
            "id": "4e14a7a2-7ddb-40cb-b16a-4c3752e9c763",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1c7be572-d244-4625-9a50-d7f5b8e517c3"
        },
        {
            "id": "57df81cd-274f-4e38-a8ee-dc15653f0abf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1c7be572-d244-4625-9a50-d7f5b8e517c3"
        },
        {
            "id": "f12b6347-bde1-48d0-a1da-a042de1b7d2c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1c7be572-d244-4625-9a50-d7f5b8e517c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "64bab6c1-427f-44a3-930a-6b7d40fbccbc",
    "visible": true
}