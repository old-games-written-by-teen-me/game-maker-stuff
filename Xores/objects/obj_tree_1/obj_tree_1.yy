{
    "id": "782086c0-ad13-41ff-a3d4-0f9021494c26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tree_1",
    "eventList": [
        {
            "id": "1baa2258-126f-4926-af55-12796ec5c965",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "782086c0-ad13-41ff-a3d4-0f9021494c26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "22b58df1-d18e-4f47-bdb0-bc8226480d1b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "061a510f-d28b-4639-839c-f89844afbfc2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 41
        },
        {
            "id": "0a40f857-8221-493a-a042-7de97e6ce027",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 41
        },
        {
            "id": "48289cd1-249d-48a6-8843-5e2d50776019",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 37,
            "y": 62
        },
        {
            "id": "2f5c814d-17df-4e1b-ae06-8e0d1d90c6b9",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 12,
            "y": 62
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
    "visible": true
}