/// @description Move 10% towards the player

if (shake > 0) {
	x += random(6) - 3;
	y += random(6) - 3;
	shake--;
}
var target_x;


if (keyboard_check(vk_shift)) {
	if (instance_exists(obj_player)) {
		x = obj_player.x;
		y = obj_player.y;
		x += lengthdir_x(60, point_direction(obj_player.x, obj_player.y, mouse_x, mouse_y));
		y += lengthdir_y(60, point_direction(obj_player.x, obj_player.y, mouse_x, mouse_y));
	}
} else {
	if (instance_exists(obj_player)) {
		x += (obj_player.x - x) * .1;
		y += (obj_player.y - y) * .1;
	}
}