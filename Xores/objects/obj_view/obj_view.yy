{
    "id": "8b3ea6e5-6fdc-491f-ad9e-5b839f252e99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_view",
    "eventList": [
        {
            "id": "e8526452-3d36-46fc-a83d-7b47b6148895",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8b3ea6e5-6fdc-491f-ad9e-5b839f252e99"
        },
        {
            "id": "b773baf8-cc1e-4216-9b03-fda00c16c0de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8b3ea6e5-6fdc-491f-ad9e-5b839f252e99"
        },
        {
            "id": "55323d8c-7aef-43eb-bf85-29a7188cad6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8b3ea6e5-6fdc-491f-ad9e-5b839f252e99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}