// Initialise the global array that allows the lookup of the depth of a given object
// GM2.0 does not have a depth on objects so on import from 1.x a global array is created
// NOTE: MacroExpansion is used to insert the array initialisation at import time
gml_pragma( "global", "__global_object_depths()");

// insert the generated arrays here
global.__objectDepths[0] = 0; // obj_player
global.__objectDepths[1] = -1000; // obj_player_stats
global.__objectDepths[2] = 0; // obj_player_detector
global.__objectDepths[3] = 0; // obj_ko
global.__objectDepths[4] = 0; // obj_enemy_slime
global.__objectDepths[5] = -1828047873; // obj_silhouette
global.__objectDepths[6] = 0; // obj_animate_bg
global.__objectDepths[7] = 0; // obj_ps
global.__objectDepths[8] = 0; // obj_input
global.__objectDepths[9] = 0; // obj_dash_effect
global.__objectDepths[10] = 0; // obj_view
global.__objectDepths[11] = 0; // obj_lifeform_parent
global.__objectDepths[12] = 0; // obj_damage
global.__objectDepths[13] = 0; // obj_speaker
global.__objectDepths[14] = -99; // obj_text_bubble
global.__objectDepths[15] = -99; // obj_dialogue
global.__objectDepths[16] = -3002; // obj_inventory
global.__objectDepths[17] = 0; // obj_arrow
global.__objectDepths[18] = 0; // obj_projectile
global.__objectDepths[19] = 0; // obj_door
global.__objectDepths[20] = 0; // obj_dialoguer
global.__objectDepths[21] = 0; // obj_wall
global.__objectDepths[22] = 0; // obj_sign
global.__objectDepths[23] = 0; // obj_silhouette_collision
global.__objectDepths[24] = 0; // obj_expr
global.__objectDepths[25] = 0; // obj_health
global.__objectDepths[26] = 0; // obj_pause_menu
global.__objectDepths[27] = 0; // obj_main_menu
global.__objectDepths[28] = -999; // obj_text
global.__objectDepths[29] = 0; // obj_menu_button
global.__objectDepths[30] = 0; // obj_draw_bg


global.__objectNames[0] = "obj_player";
global.__objectNames[1] = "obj_player_stats";
global.__objectNames[2] = "obj_player_detector";
global.__objectNames[3] = "obj_ko";
global.__objectNames[4] = "obj_enemy_slime";
global.__objectNames[5] = "obj_silhouette";
global.__objectNames[6] = "obj_animate_bg";
global.__objectNames[7] = "obj_ps";
global.__objectNames[8] = "obj_input";
global.__objectNames[9] = "obj_dash_effect";
global.__objectNames[10] = "obj_view";
global.__objectNames[11] = "obj_lifeform_parent";
global.__objectNames[12] = "obj_damage";
global.__objectNames[13] = "obj_speaker";
global.__objectNames[14] = "obj_text_bubble";
global.__objectNames[15] = "obj_dialogue";
global.__objectNames[16] = "obj_inventory";
global.__objectNames[17] = "obj_arrow";
global.__objectNames[18] = "obj_projectile";
global.__objectNames[19] = "obj_door";
global.__objectNames[20] = "obj_dialoguer";
global.__objectNames[21] = "obj_wall";
global.__objectNames[22] = "obj_sign";
global.__objectNames[23] = "obj_silhouette_collision";
global.__objectNames[24] = "obj_expr";
global.__objectNames[25] = "obj_health";
global.__objectNames[26] = "obj_pause_menu";
global.__objectNames[27] = "obj_main_menu";
global.__objectNames[28] = "obj_text";
global.__objectNames[29] = "obj_menu_button";
global.__objectNames[30] = "obj_draw_bg";


// create another array that has the correct entries
var len = array_length_1d(global.__objectDepths);
global.__objectID2Depth = [];
for( var i=0; i<len; ++i ) {
	var objID = asset_get_index( global.__objectNames[i] );
	if (objID >= 0) {
		global.__objectID2Depth[ objID ] = global.__objectDepths[i];
	} // end if
} // end for