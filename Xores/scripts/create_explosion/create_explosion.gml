/// @description create_explosion(x, y)

var xx = argument0;
var yy = argument1;

audio_play_sound(snd_explosion, 5, false);
repeat(10) {
	instance_create(xx-16+random(30), yy-16+random(32), obj_explosion);	
}
//part_particles_create(global.ps, xx, yy, obj_ps.explo_center_part, 2);