///phy_room_goto(new_x, new_y, new_room)

var xx = argument[0];
var yy = argument[1];
var new_room = argument[2];

room_goto(new_room);

x = xx;
y = yy;