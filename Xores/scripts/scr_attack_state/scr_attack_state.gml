/// @description scr_attack_state
{
    image_speed = .60;
    movement = ATTACK;
    
	var mouse_dir = point_direction(x, y, mouse_x, mouse_y);
	var direct_to_mouse = point_direction(x, y, mouse_x, mouse_y)/90;
	
	    switch(round(direct_to_mouse)) {
    
        case 0:
            face = RIGHT;
                break;
                
        case 1:
            face = UP;
                break;
                
        case 2:
            face = LEFT;
                break;
                
        case 3:
            face = DOWN;
                break;
                
        case 4:
            face = RIGHT;
                break;
                
    }
	var x_offset = lengthdir_x(20, mouse_dir);
	var y_offset = lengthdir_y(20, mouse_dir);
	
    if (image_index >= 3 and attacked == false) {
        var xx = 0;
        var yy = 0;
		
		len = spd*3;
		phy_position_x += lengthdir_x (len, mouse_dir);
		phy_position_y += lengthdir_y (len, mouse_dir);
		audio_play_sound(snd_sword_attack, 7, false);
        var damage = instance_create (x+x_offset , y+y_offset , obj_damage);
        damage.sprite_index = spr_damage;
        damage.creator = id;
        damage.damage = obj_player_stats.attack;
        attacked = true;
    }
}