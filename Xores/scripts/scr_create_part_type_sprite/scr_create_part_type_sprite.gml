/// @description scr_create_part_type_sprite(sprite, blend, min_life, max_life, min_scale, max_scale, scaling);
var spr = argument[0];
var blend = argument[1];
var min_life = argument[2];
var max_life = argument[3];
var min_scale = argument[4];
var max_scale = argument[5];
var scaling = argument[6];

var pt = part_type_create();

part_type_alpha2(pt, 1, 0);
part_type_sprite(pt, spr, false, true, false);
part_type_blend(pt, blend);
part_type_size(pt, min_scale, max_scale, scaling, 0);
part_type_life(pt, min_life, max_life);
//part_type_orientation(pt, 0, 360, false, false, false);

return pt;