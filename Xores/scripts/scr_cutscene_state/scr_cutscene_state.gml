/// @description scr_cutscene_state(moving state, speed)
/// @function scr_cutscene_state
/// @param moving state
/// @param  speed

argument0 = moving_state;
argument1 = moving_spd;

switch (moving_state) {
    case wait:
        x = x;
        y = y;
            break;
            
    case move_up:
        phy_position_y -= moving_spd;
            break;
    
    case move_down:
        phy_position_y += moving_spd;
            break;
            
    case move_right:
        phy_position_x += moving_spd;
            break;
            
    case move_left:
        phy_position_x -= moving_spd;
            break;
}