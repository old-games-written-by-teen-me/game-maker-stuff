/// @description scr_dash_state

var mouse_dir = point_direction(x, y, mouse_x, mouse_y);
var mouse_dir_rounded = point_direction(x, y, mouse_x, mouse_y)/90;
switch(round(mouse_dir_rounded)) {
        case 0:
            face = RIGHT;
                break;
                
        case 1:
            face = UP;
                break;
                
        case 2:
            face = LEFT;
                break;
                
        case 3:
            face = DOWN;
                break;
                
        case 4:
            face = RIGHT;
                break; 
}

len = spd*5;

//Get the hspd and vspd
hspd = lengthdir_x(len, mouse_dir);
vspd = lengthdir_y(len, mouse_dir);

//Move
phy_position_x += hspd;
phy_position_y += vspd;

///Create the dash effect
var dash = instance_create(x, y, obj_dash_effect);
dash.sprite_index = spr_index;
dash.image_index = image_index;