/// @description scr_draw_silhouette();
/// @function scr_draw_silhouette
surface_set_target(surface);

//Make the surface_black
draw_clear(c_black);

//Draw the player white
d3d_set_fog(true, c_white, 0, 1);
with(obj_lifeform_parent) {
    draw_self();
}
d3d_set_fog(false, c_white, 0, 0);

//Draw the tree black with an alpha of .5
with (obj_tree_1) {
    draw_sprite_ext(sprite_index, image_index, x, y, image_xscale, image_yscale, image_angle, c_black, .5);
}

surface_reset_target();

//Draw the surface
//shader_set(shd_leave_gray);
//draw_surface(surface, 0, 0);
//shader_reset();