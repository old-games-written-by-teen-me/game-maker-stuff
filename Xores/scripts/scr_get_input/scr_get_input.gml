/// @description scr_get_input
right_key = keyboard_check(ord("D"));
left_key = keyboard_check(ord("A"));
up_key = keyboard_check(ord("W"));
down_key = keyboard_check(ord("S"));
dash_key = keyboard_check_pressed(vk_space);
attack_key = mouse_check_button_pressed(mb_left);
pause_key = keyboard_check_pressed(vk_escape);
magic_key = keyboard_check_pressed(ord("F"));
keyboard_set_map(vk_enter, vk_space);

// MAP WASD WITH ARROWS
keyboard_set_map(vk_right, ord("D"));
keyboard_set_map(vk_left, ord("A"));
keyboard_set_map(vk_up, ord("W"));
keyboard_set_map(vk_down, ord("S"));

menu_select_key = keyboard_check_pressed(vk_space);
item_cycle_key = keyboard_check_pressed(vk_tab);
item_index_1_key = keyboard_check(ord("1"));
item_index_2_key = keyboard_check(ord("2"));
item_index_3_key = keyboard_check(ord("3"));
interact_key = keyboard_check(ord("E"));
npc_interact_key = dash_key;
inventory_key = keyboard_check_pressed(ord("Q"));

//Get the axis
xaxis = (right_key - left_key);
yaxis = (down_key - up_key);