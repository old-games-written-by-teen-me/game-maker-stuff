/// @description scr_move_state
movement = MOVE;
//Ability to move
if (instance_exists(obj_text_bubble)){
    able_to_move = false;
}

if (obj_input.dash_key) {
	var mouse_dir = point_direction(x, y, mouse_x, mouse_y);
    var xdir = lengthdir_x(3, mouse_dir);
    var ydir = lengthdir_y(3, mouse_dir);
    var speaker = instance_place(x+xdir, y+ydir, obj_dialoguer);
    if (speaker != noone) {
        //Talk to it
        with(speaker) {
            if(!instance_exists(obj_dialogue)) {
                dialogue = instance_create(450, 450, obj_dialogue);
                dialogue.text = text;
            } else {
                    dialogue.text_page++;
                    dialogue.text_count = 0.5;
                 if (dialogue.text_page > array_length_1d(dialogue.text)-1) {
                    with(dialogue) {
                        instance_destroy();
                        movement = MOVE;
                    }
                }
            }
}
    } else if (obj_player_stats.stamina >= DASH_COST) {
        //Dash
        if (able_to_move){
                state = scr_dash_state;
        }
        alarm[0] = room_speed/7;
        obj_player_stats.stamina -= DASH_COST;
        obj_player_stats.alarm[0] = room_speed;
    }
}
    
    if (obj_input.attack_key) {
		switch (item_index) {
			case 1:
				if (able_to_move) {
					image_index = 0;
					state = scr_attack_state;
		        }
					break;
			case 2:
				scr_shoot_arrow();
					break;
		}
    }
	if (obj_input.magic_key) {
		if (able_to_move && obj_player_stats.stamina >= DASH_COST) {
			scr_projectile_attack();
		}
	}
    
    //Get direction
    if(able_to_move) {
        dir = point_direction(0, 0, obj_input.xaxis, obj_input.yaxis);
    }
    //Get the length
    if (obj_input.xaxis == 0 and obj_input.yaxis == 0) {
        len = 0;
    } else {
        if (able_to_move){
            len = spd;
        }
        scr_get_face(dir);
    }
	
    //Get the hspd and vspd
    hspd = lengthdir_x (len, dir);
    vspd = lengthdir_y (len, dir);
    
    //Move
    if (able_to_move == true) {
        phy_position_x += hspd;
        phy_position_y += vspd;
    }
    
    //Control the sprite
    if(!instance_exists(obj_text_bubble)){
        image_speed = image_spd;
    }
    if (len == 0) image_index = 0;