/// @description scr_part_blood_init()
/// @function scr_part_blood_init
{
	// Initilize the blood part
	pt_blood = part_type_create();
	var pt = pt_blood;
	
	// Set the settings for the blood particle
	part_type_shape(pt, pt_shape_square);
	part_type_size(pt, .025, .040, 0, 0);
	part_type_color1(pt, make_color_rgb(178, 35, 35));
	part_type_orientation(pt, -20, 20, .1, 0, false);
	part_type_speed(pt, 1, 2, -.1, .1);
	part_type_direction(pt, 240, 300, 0, 0);
	part_type_gravity(pt, 0, 270);
	part_type_life(pt, 25, 50);
}