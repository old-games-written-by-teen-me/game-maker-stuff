{
	// Initilize the fire part
	pt_fire = part_type_create();
	var pt = pt_fire;
	
	// Set the settings for the blood particle
	part_type_shape(pt, pt_shape_square);
	part_type_size(pt, .050, .075, 0, 0);
	part_type_color2(pt, c_orange, c_red);
	part_type_blend(pt, true);
	part_type_orientation(pt, -0.7, .7, .1, 0, 0);
	part_type_speed(pt, 1, 2, -.1, .1);
	part_type_direction(pt, 0, 360, 0, 0);
	part_type_gravity(pt, 0, 270);
	part_type_life(pt, 3, 5);
}