/// @description scr_part_slime_blood_init()
/// @function scr_part_slime_blood_init
{
     // Initilize the slime blood part
     pt_slime_blood = part_type_create();
     var pt = pt_slime_blood;
     
     // Set the settings for the slime blood particle
     part_type_shape(pt, pt_shape_square);
     part_type_size(pt, .025, .05, 0, 0);
     part_type_color1(pt, make_color_rgb(87, 198, 120));
	 part_type_alpha1(pt, .7);
	 part_type_orientation(pt, -20, 20, .1, 0, false);
     part_type_speed(pt, 1, 2, -.1, .1);
     part_type_direction(pt, 0, 360, 0, 0);
     part_type_gravity(pt, 0, 270);
     part_type_life(pt, 25,50);
     
}