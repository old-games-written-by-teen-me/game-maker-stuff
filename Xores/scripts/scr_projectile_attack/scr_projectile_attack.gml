/// @description scr_projectile_attack()
{
	
	var mouse_dir = point_direction(x, y, mouse_x, mouse_y);
	var direct_to_mouse = point_direction(x, y, mouse_x, mouse_y)/90;
    image_speed = .30;
    obj_player_stats.stamina -= DASH_COST;
	obj_player_stats.alarm[0] = room_speed;
		    switch(round(direct_to_mouse)) {
    
        case 0:
            face = RIGHT;
                break;
                
        case 1:
            face = UP;
                break;
                
        case 2:
            face = LEFT;
                break;
                
        case 3:
            face = DOWN;
                break;
                
        case 4:
            face = RIGHT;
                break;
                
    }
	

        var xforce = lengthdir_x(3, mouse_dir);
        var yforce = lengthdir_y(3, mouse_dir);
		var p = instance_create(x+xforce, y+yforce, obj_projectile);
        p.creator = id;
        with (p) {
            physics_apply_impulse(x, y, xforce, yforce);
		}
}