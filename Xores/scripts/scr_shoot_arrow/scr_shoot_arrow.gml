/// @description scr_shoot_arrow()
{
	
	var mouse_dir = point_direction(x, y, mouse_x, mouse_y);
    var xforce = lengthdir_x(6, mouse_dir);
    var yforce = lengthdir_y(6, mouse_dir);
	var p = instance_create(x+xforce, y+yforce, obj_arrow);
    p.creator = id;
	p.image_angle = mouse_dir;
    with (p) {
		physics_apply_impulse(x, y, xforce, yforce);
	}
}