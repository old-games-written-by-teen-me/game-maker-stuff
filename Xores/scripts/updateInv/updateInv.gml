///updateInv(cellWidth, cellHeight);


var cellWidth = argument0;
var cellHeight = argument1;
for(i = 0; i < totalSlots; i++){ //drawing the item into the correct slot
      drawInvSprite(i, "stone", 0, spr_stone)
      drawInvSprite(i, "wood",  1, spr_wood)
      drawInvSprite(i, "hatchet",  4, spr_hatchet)
}

    //<----------------------------------------------------------------------------->//

//editing the inventory and managing it
for(i = 0; i < totalSlots; i++){
  if(mouse_check_button_pressed(mb_left)){
        if(mouseOver(coordX[i], coordY[i], cellWidth, cellHeight)){
             clickSlot(i, "stone", spr_item_stone);
             clickSlot(i, "wood", spr_item_wood);
             clickSlot(i, "hatchet", spr_hatchet);
         }          
  }
  
  //dragging the item around
  if(mouse_check_button(mb_left)){
    draw_sprite(spr, 0, mouse_x-8, mouse_y-8);  
  }
}

//splitting an item into 2 piles
for(i = 0; i < totalSlots; i++){
  if(mouse_check_button_pressed(mb_right)){
        if(mouseOver(coordX[i], coordY[i], cellWidth, cellHeight)){
             splitInv(i, "stone", spr_item_stone);
             splitInv(i, "wood", spr_item_wood);
             splitInv(i, "hatchet", spr_hatchet);
         }          
  }
  
  //dragging the item around
  if(mouse_check_button(mb_left)){
    draw_sprite(spr, 0, mouse_x-8, mouse_y-8);  
  }
}

//releasing the mouse for when editing the objects
if(mouse_check_button_released(mb_left))
{
  for(i = 0; i < totalSlots; i++)
  {    
        var startX = 256;
        var startY = 144;
        var width = 576;
        var height = 288;
        
       var mouseX = floor(mouse_x/64)*64;
       var mouseY = floor(mouse_y/64)*64
        
        //release object from inventory
        if(!mouseOver(startX, startY, width, height) && oInventory.canDraw)
        {
             releaseSlot(0, spr_stone, oITEMStone);
             releaseSlot(1, spr_wood, oITEMWood);
             releaseSlot(4, spr_hatchet, oITEMHatchet);
        }
  
       if(global.slot[i] == "noone")
       {
           if(mouseOver(coordX[i], coordY[i], cellWidth, cellHeight))
           {
               //if all is successful in transferring the items 
              var tempSpr = spr;
              
              if(i == 15 || i == 16 || i == 17 || i == 18) //equip and quick swap slot
              {
                  transferItem(i, "hatchet", spr_hatchet); 
              }
              else
              {
                  transferItem(i, "stone", spr_item_stone);  
                  transferItem(i, "wood", spr_item_wood);  
                  transferItem(i, "hatchet", spr_hatchet);         
              }
              
          }
       }else
       {
           if(mouseOver(coordX[i], coordY[i], cellWidth, cellHeight))
           {
              //if all is successful in transferring the items 
              var tempSpr = spr;
              
              if(i == 15 || i == 16 || i == 17 || i == 18) //equip and quick swap slot
              {
                  transferItemCurrent(i, "hatchet", spr_hatchet); 
              }
              else
              {
                  transferItemCurrent(i, "stone", spr_item_stone);  
                  transferItemCurrent(i, "wood", spr_item_wood);  
                  transferItemCurrent(i, "hatchet", spr_hatchet);     
              }
            }
       }
       
  }
  spr = nosprite; 
}

checkInvCount();