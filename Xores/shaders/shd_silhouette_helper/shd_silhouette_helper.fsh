//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main() {
    // Get the original color of the pixel
    vec4 OC = texture2D(gm_BaseTexture, v_vTexcoord);
    
    float Average = (OC.r + OC.g + OC.b) / 3.0;
    float Alpha = 1.0;
    
    // Create the color
    vec4 Color = vec4(Average, Average, Average, Alpha);
    
    // Output the color
    gl_FragColor = Color;
}

