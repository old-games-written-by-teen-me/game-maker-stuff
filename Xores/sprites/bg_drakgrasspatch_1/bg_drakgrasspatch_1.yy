{
    "id": "beba6710-d104-4f98-bbe5-33a4ed11e57f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_drakgrasspatch_1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 90,
    "bbox_left": 12,
    "bbox_right": 99,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c90ca833-9e6e-4474-954c-700b4f61682b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beba6710-d104-4f98-bbe5-33a4ed11e57f",
            "compositeImage": {
                "id": "ea73fe7f-8ddc-4e2f-9796-1c576c73fdd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c90ca833-9e6e-4474-954c-700b4f61682b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63142e58-712d-4f24-9d38-e7887adbec48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c90ca833-9e6e-4474-954c-700b4f61682b",
                    "LayerId": "7133c4c6-66d2-4c9f-bda8-e1d0ec52701b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "7133c4c6-66d2-4c9f-bda8-e1d0ec52701b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beba6710-d104-4f98-bbe5-33a4ed11e57f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}