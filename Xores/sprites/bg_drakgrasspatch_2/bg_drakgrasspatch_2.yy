{
    "id": "597992ba-c56f-48d6-a262-b44d32d7b290",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_drakgrasspatch_2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 89,
    "bbox_left": 21,
    "bbox_right": 93,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6acb792-56d4-4b8a-8ef5-e218b41ec6d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "597992ba-c56f-48d6-a262-b44d32d7b290",
            "compositeImage": {
                "id": "59c147a5-b18d-428d-a244-35fc292d613b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6acb792-56d4-4b8a-8ef5-e218b41ec6d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a5a1358-9a55-463d-8f25-ff649de7de55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6acb792-56d4-4b8a-8ef5-e218b41ec6d5",
                    "LayerId": "e6618ee4-0b7a-461c-9254-b360e34d80fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "e6618ee4-0b7a-461c-9254-b360e34d80fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "597992ba-c56f-48d6-a262-b44d32d7b290",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}