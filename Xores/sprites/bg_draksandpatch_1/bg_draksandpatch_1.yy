{
    "id": "ac56488e-050f-447b-8003-8dad23f84979",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_draksandpatch_1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 90,
    "bbox_left": 12,
    "bbox_right": 99,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34b84f50-f406-4917-8425-6b4550caa8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac56488e-050f-447b-8003-8dad23f84979",
            "compositeImage": {
                "id": "bb1fff24-1c09-4c15-9f86-e3b4d502a4e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b84f50-f406-4917-8425-6b4550caa8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4459e0c4-d7e6-4381-8541-a22e03480574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b84f50-f406-4917-8425-6b4550caa8d4",
                    "LayerId": "e4ce6442-a453-4bed-a9bc-fcb30c6f071d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "e4ce6442-a453-4bed-a9bc-fcb30c6f071d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac56488e-050f-447b-8003-8dad23f84979",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}