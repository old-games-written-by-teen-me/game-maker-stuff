{
    "id": "55e4bcd0-d3f3-4e35-8253-438c683dc1c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_draksandpatch_2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 89,
    "bbox_left": 21,
    "bbox_right": 93,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7dd2dcd7-fd0a-402f-a856-ccc722690e6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55e4bcd0-d3f3-4e35-8253-438c683dc1c7",
            "compositeImage": {
                "id": "ec05a557-49fe-4092-b365-704db8f4f95c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd2dcd7-fd0a-402f-a856-ccc722690e6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97677bd8-0ee9-41d4-b31a-d2a1f425a875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd2dcd7-fd0a-402f-a856-ccc722690e6c",
                    "LayerId": "4c41e1db-596e-4321-ba6a-a82cce57887d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "4c41e1db-596e-4321-ba6a-a82cce57887d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55e4bcd0-d3f3-4e35-8253-438c683dc1c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 0,
    "yorig": 0
}