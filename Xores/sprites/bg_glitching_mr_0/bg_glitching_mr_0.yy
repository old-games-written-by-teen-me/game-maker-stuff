{
    "id": "d523df53-2c42-442e-aaf4-72e7b2682832",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_glitching_mr_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "781e7f91-ff82-4eb8-a8cb-238bca3c7478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d523df53-2c42-442e-aaf4-72e7b2682832",
            "compositeImage": {
                "id": "b41b537a-a19f-4058-b24a-631c03b0eb0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "781e7f91-ff82-4eb8-a8cb-238bca3c7478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62b565b8-1434-42b4-aab1-f7e23cfa14c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "781e7f91-ff82-4eb8-a8cb-238bca3c7478",
                    "LayerId": "c4dca636-2f0c-4055-bbab-8bc00dbb12e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "c4dca636-2f0c-4055-bbab-8bc00dbb12e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d523df53-2c42-442e-aaf4-72e7b2682832",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 0,
    "yorig": 0
}