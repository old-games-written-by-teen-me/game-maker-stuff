{
    "id": "04066564-7d68-400c-988c-8d0c5c47ba00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_glitching_mr_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba3b7b59-7536-4550-90a5-64277b0babf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04066564-7d68-400c-988c-8d0c5c47ba00",
            "compositeImage": {
                "id": "e4902d90-1cda-4da4-814b-e655ab9263ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba3b7b59-7536-4550-90a5-64277b0babf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cf6a4a3-b839-41d0-8317-a1e619a068f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba3b7b59-7536-4550-90a5-64277b0babf2",
                    "LayerId": "b6f51c20-e203-4061-98e7-7865989a05c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "b6f51c20-e203-4061-98e7-7865989a05c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04066564-7d68-400c-988c-8d0c5c47ba00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 0,
    "yorig": 0
}