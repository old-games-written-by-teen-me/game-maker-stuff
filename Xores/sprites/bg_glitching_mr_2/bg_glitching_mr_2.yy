{
    "id": "e8efdaba-6e22-45c0-ad8a-c602f563ecab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_glitching_mr_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1456706-15e2-4341-8ae4-a01006d3d97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8efdaba-6e22-45c0-ad8a-c602f563ecab",
            "compositeImage": {
                "id": "e70a39ad-41e5-4263-8daf-6127bcd16de6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1456706-15e2-4341-8ae4-a01006d3d97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc3e985-e6c9-4167-a7af-a5e8274c8fda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1456706-15e2-4341-8ae4-a01006d3d97d",
                    "LayerId": "d04f73b1-ec9e-455f-a250-e9f204383c81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "d04f73b1-ec9e-455f-a250-e9f204383c81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8efdaba-6e22-45c0-ad8a-c602f563ecab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 0,
    "yorig": 0
}