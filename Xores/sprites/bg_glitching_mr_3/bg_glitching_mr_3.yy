{
    "id": "e4b23add-ce09-4122-85a6-b11a1b5f98b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_glitching_mr_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "658ae891-af17-43a4-a14e-edf87b3b5233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4b23add-ce09-4122-85a6-b11a1b5f98b6",
            "compositeImage": {
                "id": "2f947d03-da27-41bf-a876-5a43251122b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "658ae891-af17-43a4-a14e-edf87b3b5233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64dd7100-4b0b-4293-96dd-1b8faa47429f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "658ae891-af17-43a4-a14e-edf87b3b5233",
                    "LayerId": "70f3ee84-9791-47fd-a009-54ac0741e3e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "70f3ee84-9791-47fd-a009-54ac0741e3e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4b23add-ce09-4122-85a6-b11a1b5f98b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 0,
    "yorig": 0
}