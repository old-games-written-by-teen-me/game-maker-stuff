{
    "id": "c55486a9-b217-4109-a90f-ed0973afb6d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_grass_1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc66f5b7-2ab2-4f3b-9fb8-dd1db98985c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c55486a9-b217-4109-a90f-ed0973afb6d5",
            "compositeImage": {
                "id": "e66b2cbd-b8c1-4c05-915e-e2aba366c807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc66f5b7-2ab2-4f3b-9fb8-dd1db98985c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "916ed2c8-4c51-4341-be36-ff1447e0d67e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc66f5b7-2ab2-4f3b-9fb8-dd1db98985c9",
                    "LayerId": "2e1ac30b-42f9-4301-bbbd-69d7929150ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "2e1ac30b-42f9-4301-bbbd-69d7929150ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c55486a9-b217-4109-a90f-ed0973afb6d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 0,
    "yorig": 0
}