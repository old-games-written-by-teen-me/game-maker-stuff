{
    "id": "2706bef9-29c4-42b3-b8d2-034ec1297855",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_grass_2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daa4ced1-a327-4290-9697-2ea2ad2fe1d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2706bef9-29c4-42b3-b8d2-034ec1297855",
            "compositeImage": {
                "id": "705d9b8b-8b22-40d1-bcf1-4d02200e43aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa4ced1-a327-4290-9697-2ea2ad2fe1d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231e7f30-4322-4923-a91d-1200648040bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa4ced1-a327-4290-9697-2ea2ad2fe1d3",
                    "LayerId": "6b84b78d-057f-4fe7-809d-fd225ff6f4be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "6b84b78d-057f-4fe7-809d-fd225ff6f4be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2706bef9-29c4-42b3-b8d2-034ec1297855",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}