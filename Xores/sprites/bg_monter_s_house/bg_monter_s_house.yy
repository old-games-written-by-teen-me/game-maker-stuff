{
    "id": "58abd67f-62e3-447b-9505-0dd15613540b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_monter_s_house",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59330148-3ebc-4626-b838-5ede133f2575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58abd67f-62e3-447b-9505-0dd15613540b",
            "compositeImage": {
                "id": "f75198e9-adb0-487f-a20e-3d5268df02be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59330148-3ebc-4626-b838-5ede133f2575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164784b4-e091-4d2a-babd-4bd5811d4b33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59330148-3ebc-4626-b838-5ede133f2575",
                    "LayerId": "341b2025-a134-4d4e-ae3b-2c7600c3bfdc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "341b2025-a134-4d4e-ae3b-2c7600c3bfdc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58abd67f-62e3-447b-9505-0dd15613540b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}