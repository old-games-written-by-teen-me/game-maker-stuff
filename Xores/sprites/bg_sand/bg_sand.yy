{
    "id": "19398c2c-0c9b-44a2-821d-101a78d07470",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_sand",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa00c9c7-26a3-4a5f-bfe0-0a072cfa859b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19398c2c-0c9b-44a2-821d-101a78d07470",
            "compositeImage": {
                "id": "3f30c290-87ff-4902-b66b-ae7a096a18d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa00c9c7-26a3-4a5f-bfe0-0a072cfa859b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b100d41-9bd8-41f9-8e2c-9de2484aabf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa00c9c7-26a3-4a5f-bfe0-0a072cfa859b",
                    "LayerId": "5c4563fa-2335-4d59-96db-ce3bd9c3a4ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "5c4563fa-2335-4d59-96db-ce3bd9c3a4ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19398c2c-0c9b-44a2-821d-101a78d07470",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 14,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 0,
    "yorig": 0
}