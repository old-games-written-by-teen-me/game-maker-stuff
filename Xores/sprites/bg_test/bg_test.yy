{
    "id": "848b91d6-4879-4d12-a161-4b561940cba1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_test",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07a0328e-d666-4cad-b685-c4f6ba73da87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "848b91d6-4879-4d12-a161-4b561940cba1",
            "compositeImage": {
                "id": "4a1cf7da-3501-40ad-91ca-a4d9bea4d389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a0328e-d666-4cad-b685-c4f6ba73da87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48d61594-882c-4498-a556-9fdf73b94d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a0328e-d666-4cad-b685-c4f6ba73da87",
                    "LayerId": "398c6dcd-cc9f-46c3-a3f5-f49e15aea7ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 360,
    "layers": [
        {
            "id": "398c6dcd-cc9f-46c3-a3f5-f49e15aea7ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "848b91d6-4879-4d12-a161-4b561940cba1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}