{
    "id": "dc0879a9-0094-4bad-903d-49ef6edecce7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fg_bed",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f98f2329-89db-46f8-966a-4fac00c42d9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc0879a9-0094-4bad-903d-49ef6edecce7",
            "compositeImage": {
                "id": "31f0c1e2-2979-42c2-ba90-d8a5046240f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98f2329-89db-46f8-966a-4fac00c42d9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb0aceb-acbc-47a2-9d63-e088f40adcdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98f2329-89db-46f8-966a-4fac00c42d9c",
                    "LayerId": "431f5894-03be-4837-a320-76792ce6fd67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "431f5894-03be-4837-a320-76792ce6fd67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc0879a9-0094-4bad-903d-49ef6edecce7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 0,
    "yorig": 0
}