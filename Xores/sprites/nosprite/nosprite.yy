{
    "id": "7506b416-9218-4962-830c-dc4d16728084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "nosprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85f0eb66-e6ab-438e-a19e-4698e4325669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7506b416-9218-4962-830c-dc4d16728084",
            "compositeImage": {
                "id": "efd34c56-4590-4dd6-ae86-5127b8d7bcb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f0eb66-e6ab-438e-a19e-4698e4325669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d2394f-cd52-4d7b-9940-8d3db1824895",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f0eb66-e6ab-438e-a19e-4698e4325669",
                    "LayerId": "6419137e-f8a7-4ab7-b204-1f3052a732a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6419137e-f8a7-4ab7-b204-1f3052a732a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7506b416-9218-4962-830c-dc4d16728084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}