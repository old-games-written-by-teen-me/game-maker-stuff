{
    "id": "6ba7523f-4a5f-488c-84ca-a30ad39c6927",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_down_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0e32077-a62d-4ae0-8364-fbb8d7f49475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba7523f-4a5f-488c-84ca-a30ad39c6927",
            "compositeImage": {
                "id": "12979eed-879e-4089-a8fb-04442ca380fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e32077-a62d-4ae0-8364-fbb8d7f49475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b405b5-e209-4daf-8246-3f6f998c2307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e32077-a62d-4ae0-8364-fbb8d7f49475",
                    "LayerId": "5fa159bf-7c61-4e97-8843-276c48f1d243"
                }
            ]
        },
        {
            "id": "80f973d5-8e70-4c7c-a355-c41fcb807e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba7523f-4a5f-488c-84ca-a30ad39c6927",
            "compositeImage": {
                "id": "af95af91-a03c-4fc1-9759-08ce29ce3356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80f973d5-8e70-4c7c-a355-c41fcb807e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "596ed3e2-05dc-48db-895e-4caa09823286",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80f973d5-8e70-4c7c-a355-c41fcb807e5b",
                    "LayerId": "5fa159bf-7c61-4e97-8843-276c48f1d243"
                }
            ]
        },
        {
            "id": "f916818f-7b8c-4aad-aa93-3a133998ed73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba7523f-4a5f-488c-84ca-a30ad39c6927",
            "compositeImage": {
                "id": "ecb500c4-f707-4497-a4b2-4f1cc1ccd680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f916818f-7b8c-4aad-aa93-3a133998ed73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7d34905-895d-4c75-a0eb-736f8b2f3ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f916818f-7b8c-4aad-aa93-3a133998ed73",
                    "LayerId": "5fa159bf-7c61-4e97-8843-276c48f1d243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5fa159bf-7c61-4e97-8843-276c48f1d243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ba7523f-4a5f-488c-84ca-a30ad39c6927",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}