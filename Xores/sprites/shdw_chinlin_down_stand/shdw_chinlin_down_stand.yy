{
    "id": "63236cb3-f311-4ab9-987f-1d24383e171c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_down_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40a69f36-5380-4aaf-8e5f-4ee2e468c8bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63236cb3-f311-4ab9-987f-1d24383e171c",
            "compositeImage": {
                "id": "addbe693-ca71-410b-b2e4-0e87a5266eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a69f36-5380-4aaf-8e5f-4ee2e468c8bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1799f63-e7c2-4cf5-b006-a9869551b983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a69f36-5380-4aaf-8e5f-4ee2e468c8bb",
                    "LayerId": "c626eaef-9c16-486f-b93a-cb67469f187c"
                }
            ]
        },
        {
            "id": "f22084cf-4d20-4987-9d44-59b39f949fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63236cb3-f311-4ab9-987f-1d24383e171c",
            "compositeImage": {
                "id": "1775027b-9cac-4012-aa65-b7ee0a2685ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22084cf-4d20-4987-9d44-59b39f949fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8ae5c49-06ad-4125-9963-ffa44054efcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22084cf-4d20-4987-9d44-59b39f949fdb",
                    "LayerId": "c626eaef-9c16-486f-b93a-cb67469f187c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c626eaef-9c16-486f-b93a-cb67469f187c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63236cb3-f311-4ab9-987f-1d24383e171c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}