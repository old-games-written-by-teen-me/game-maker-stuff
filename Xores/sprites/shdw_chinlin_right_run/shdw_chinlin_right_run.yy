{
    "id": "8db112a2-1387-4ea9-b5f0-fdf74f4df752",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_right_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 20,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c872e81-37a1-4f49-9926-4c00d89ccbb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8db112a2-1387-4ea9-b5f0-fdf74f4df752",
            "compositeImage": {
                "id": "334d379e-0648-42db-99e5-63bfd07c77a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c872e81-37a1-4f49-9926-4c00d89ccbb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97cffeab-963f-49f1-ab5e-5a20a02a2a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c872e81-37a1-4f49-9926-4c00d89ccbb6",
                    "LayerId": "8a3989bd-c988-4b80-b970-25b11df2cc6f"
                }
            ]
        },
        {
            "id": "b8a297d8-6cd1-4115-84f5-ba944bb804bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8db112a2-1387-4ea9-b5f0-fdf74f4df752",
            "compositeImage": {
                "id": "379cd794-4f64-42ee-9fe6-ef8e2c325462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8a297d8-6cd1-4115-84f5-ba944bb804bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ff1bbec-bc90-4acb-9a9f-80960e26f8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8a297d8-6cd1-4115-84f5-ba944bb804bc",
                    "LayerId": "8a3989bd-c988-4b80-b970-25b11df2cc6f"
                }
            ]
        },
        {
            "id": "c1d7d276-89ec-485e-bbf3-f45496568ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8db112a2-1387-4ea9-b5f0-fdf74f4df752",
            "compositeImage": {
                "id": "6506b869-9a7a-4da4-adcb-dba59664c0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1d7d276-89ec-485e-bbf3-f45496568ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "583f039a-fdfe-499a-a07a-75e86bec9a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1d7d276-89ec-485e-bbf3-f45496568ed0",
                    "LayerId": "8a3989bd-c988-4b80-b970-25b11df2cc6f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a3989bd-c988-4b80-b970-25b11df2cc6f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8db112a2-1387-4ea9-b5f0-fdf74f4df752",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}