{
    "id": "036f1197-c87d-45ba-8248-562f34f9a118",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_right_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 19,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "baecaeb0-7f59-46b3-ba5b-3cc75706e762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "036f1197-c87d-45ba-8248-562f34f9a118",
            "compositeImage": {
                "id": "0159ea18-6881-4b8e-b208-7366f65537f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baecaeb0-7f59-46b3-ba5b-3cc75706e762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b1af1a8-d81e-4709-a929-4eb91ddfd3cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baecaeb0-7f59-46b3-ba5b-3cc75706e762",
                    "LayerId": "103c8fa1-dd42-41ea-a331-17b1c477c8a8"
                }
            ]
        },
        {
            "id": "e064eb0b-bbf3-4749-8f48-16eeb51d9678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "036f1197-c87d-45ba-8248-562f34f9a118",
            "compositeImage": {
                "id": "24ff9f36-bd61-4f30-a94c-1e872d6d1fb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e064eb0b-bbf3-4749-8f48-16eeb51d9678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "000b44e9-2cdb-43dd-a4a9-fcfbc6b43097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e064eb0b-bbf3-4749-8f48-16eeb51d9678",
                    "LayerId": "103c8fa1-dd42-41ea-a331-17b1c477c8a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "103c8fa1-dd42-41ea-a331-17b1c477c8a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "036f1197-c87d-45ba-8248-562f34f9a118",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}