{
    "id": "a0271d56-defc-4d32-88f7-0beb88d7928d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_up_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "996f12f8-cad2-4146-82c8-155c61e1ecd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0271d56-defc-4d32-88f7-0beb88d7928d",
            "compositeImage": {
                "id": "782f3077-8d72-4482-9834-8d268f4560f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996f12f8-cad2-4146-82c8-155c61e1ecd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e53fff9-6d21-419a-b717-83b2553786cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996f12f8-cad2-4146-82c8-155c61e1ecd2",
                    "LayerId": "b792d427-7f31-4d7e-8eab-1afe78af316e"
                }
            ]
        },
        {
            "id": "7788768f-2049-45f9-8fc4-a78575cc6fe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0271d56-defc-4d32-88f7-0beb88d7928d",
            "compositeImage": {
                "id": "2e12ca13-5717-4061-b5cb-856420fb59e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7788768f-2049-45f9-8fc4-a78575cc6fe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24bb3a17-a9f1-49b4-9b6d-a0c298c97343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7788768f-2049-45f9-8fc4-a78575cc6fe6",
                    "LayerId": "b792d427-7f31-4d7e-8eab-1afe78af316e"
                }
            ]
        },
        {
            "id": "c71c90bf-8fcd-454b-b99e-688203e5f41f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0271d56-defc-4d32-88f7-0beb88d7928d",
            "compositeImage": {
                "id": "09b61c03-c84a-45a9-8cdc-ff71cd435d78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71c90bf-8fcd-454b-b99e-688203e5f41f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0174c724-ecaa-4913-ba63-00c6bd8b594c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71c90bf-8fcd-454b-b99e-688203e5f41f",
                    "LayerId": "b792d427-7f31-4d7e-8eab-1afe78af316e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b792d427-7f31-4d7e-8eab-1afe78af316e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0271d56-defc-4d32-88f7-0beb88d7928d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}