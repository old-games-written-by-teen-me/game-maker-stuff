{
    "id": "1872dd63-3987-4e03-beb4-c34cf5ff7c64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_chinlin_up_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd50fae8-5cfc-47a9-a8bb-d6eff5061248",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1872dd63-3987-4e03-beb4-c34cf5ff7c64",
            "compositeImage": {
                "id": "438d4ebb-9376-4a7e-a085-0d6b5349eb1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd50fae8-5cfc-47a9-a8bb-d6eff5061248",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f61682d-d2ff-4ab5-a3a1-a0f14ec86e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd50fae8-5cfc-47a9-a8bb-d6eff5061248",
                    "LayerId": "8b871bec-ed40-463e-a3f4-e73e34634857"
                }
            ]
        },
        {
            "id": "06d9cb57-f53b-40b8-b09a-4ae38a645cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1872dd63-3987-4e03-beb4-c34cf5ff7c64",
            "compositeImage": {
                "id": "a756edbf-f8cf-44b0-b01f-06695ac8611b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06d9cb57-f53b-40b8-b09a-4ae38a645cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd82729-a588-4e68-b91f-b8f8d41fffca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06d9cb57-f53b-40b8-b09a-4ae38a645cb3",
                    "LayerId": "8b871bec-ed40-463e-a3f4-e73e34634857"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b871bec-ed40-463e-a3f4-e73e34634857",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1872dd63-3987-4e03-beb4-c34cf5ff7c64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}