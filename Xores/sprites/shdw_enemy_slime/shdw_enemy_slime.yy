{
    "id": "ac671eea-7470-43b9-85e5-fb808a6e80b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_enemy_slime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 5,
    "bbox_right": 20,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b87e1f0-2e81-4043-8182-4f34e725617e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac671eea-7470-43b9-85e5-fb808a6e80b3",
            "compositeImage": {
                "id": "91489a5b-bc18-448a-83f3-4977a396c1f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b87e1f0-2e81-4043-8182-4f34e725617e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ead9fb9b-ed60-404b-bb27-db0e8b4772fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b87e1f0-2e81-4043-8182-4f34e725617e",
                    "LayerId": "7b85ab61-b5b8-46a1-874c-7155ca512d75"
                }
            ]
        },
        {
            "id": "4020a7b1-7de9-4a3e-a779-86150f6fea10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac671eea-7470-43b9-85e5-fb808a6e80b3",
            "compositeImage": {
                "id": "16d7e9c4-8e41-4add-9d40-9ee9dfb53d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4020a7b1-7de9-4a3e-a779-86150f6fea10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddbd83d2-fbf8-45ba-8073-faaa47aef90b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4020a7b1-7de9-4a3e-a779-86150f6fea10",
                    "LayerId": "7b85ab61-b5b8-46a1-874c-7155ca512d75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "7b85ab61-b5b8-46a1-874c-7155ca512d75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac671eea-7470-43b9-85e5-fb808a6e80b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 7
}