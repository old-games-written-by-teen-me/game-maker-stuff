{
    "id": "5b188aad-f11d-4b51-aa79-9d9594bca734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "847de49c-3f55-4add-8048-ea6a485b865f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "2ee03456-7655-4673-9b42-5b7f14e669b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847de49c-3f55-4add-8048-ea6a485b865f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb1694f-292c-4e9c-954b-b694be63b521",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847de49c-3f55-4add-8048-ea6a485b865f",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "7234fee7-9e7b-40a8-9141-7a080b99e729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "8a35d49b-c94e-4444-9c24-223436067c1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7234fee7-9e7b-40a8-9141-7a080b99e729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98197b5-751b-4948-bf58-4ced6affd1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7234fee7-9e7b-40a8-9141-7a080b99e729",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "920df4f1-45b7-4937-a17c-9b880f547225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "7ae11334-a83e-4244-b898-1147d2fd41c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "920df4f1-45b7-4937-a17c-9b880f547225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bceb9658-b512-4bd0-887b-2e8ce37916c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "920df4f1-45b7-4937-a17c-9b880f547225",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "445bf2d4-f58f-4a06-a9bd-46ab7ea42fff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "fb349b19-e6c6-4254-94d6-074404f964d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "445bf2d4-f58f-4a06-a9bd-46ab7ea42fff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0a4940-6db9-47f5-8abf-9c0898e13f86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "445bf2d4-f58f-4a06-a9bd-46ab7ea42fff",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "cb090142-2b92-4434-978a-bcc60332065a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "6d19a7b1-1329-4d50-95bb-a52bd257e343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb090142-2b92-4434-978a-bcc60332065a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c27a8c-57de-43d3-ab4a-6785938ae412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb090142-2b92-4434-978a-bcc60332065a",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "b78488cc-8762-4e8e-9eb1-f8af86a6a463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "9d191142-2637-42d2-b7cb-637c6cc03fe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b78488cc-8762-4e8e-9eb1-f8af86a6a463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1de1e4b-b84c-42de-9c98-33d44dd8deb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b78488cc-8762-4e8e-9eb1-f8af86a6a463",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "97a76a8a-685d-4baf-b099-da34140b89ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "e90f8f8c-84e5-4bd0-aa22-1fd1a5b1edc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a76a8a-685d-4baf-b099-da34140b89ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeb66424-9b48-4016-833c-5691dfeda75a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a76a8a-685d-4baf-b099-da34140b89ee",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "cb19b2ed-80c2-40c0-9510-e113ea53b028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "0cfe4027-9d55-4197-9aee-088a011ef256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb19b2ed-80c2-40c0-9510-e113ea53b028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb716de8-af09-4686-96c8-359d884cc989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb19b2ed-80c2-40c0-9510-e113ea53b028",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "dee5b200-581c-4c58-99c4-3882a219acb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "a7d834ca-90cf-4efa-917d-d9bdefea665c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee5b200-581c-4c58-99c4-3882a219acb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc847ab-5e23-4c42-b6be-4c0d3a31e274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee5b200-581c-4c58-99c4-3882a219acb0",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "fa0816fa-d4fc-4212-a2b9-55c367d98e09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "9c53c861-2833-49c5-981a-e5e43fb1bd5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0816fa-d4fc-4212-a2b9-55c367d98e09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435cd25e-d653-463a-bf46-64b1ad5436a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0816fa-d4fc-4212-a2b9-55c367d98e09",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "740c6daf-70b9-4c81-971c-a627d8a9e32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "2478c1b8-c714-47f7-9b5d-725584d1f297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "740c6daf-70b9-4c81-971c-a627d8a9e32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9daa76f9-12cc-4f5c-bb98-c8591be22c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "740c6daf-70b9-4c81-971c-a627d8a9e32c",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        },
        {
            "id": "7959217a-0271-402c-868f-80149679d5f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "compositeImage": {
                "id": "afa910c4-8201-42a2-9349-80839e88ed96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7959217a-0271-402c-868f-80149679d5f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf8877e5-7e8e-4d65-84e1-0a73a63086f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7959217a-0271-402c-868f-80149679d5f6",
                    "LayerId": "04fdebf9-5a55-478e-89e7-b8fdbdca698d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "04fdebf9-5a55-478e-89e7-b8fdbdca698d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b188aad-f11d-4b51-aa79-9d9594bca734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}