{
    "id": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0155c288-7917-48e1-9705-58ef86131377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "1a9955c2-af61-4a64-a564-da29f7dd9485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0155c288-7917-48e1-9705-58ef86131377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4676a4e6-acdf-430a-9041-29768fac5e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0155c288-7917-48e1-9705-58ef86131377",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "75f176f5-57c0-47d2-8186-22ceaf5a8b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "004c9d83-834f-40a5-92a3-2815fdbc1940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f176f5-57c0-47d2-8186-22ceaf5a8b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc01c00f-ee81-452c-9882-bdb5eb331b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f176f5-57c0-47d2-8186-22ceaf5a8b4e",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "934fd933-0728-4041-8151-c31f619711ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "b1c4fbb5-b899-4ebd-8e86-ec729525d981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934fd933-0728-4041-8151-c31f619711ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b101e2c-d085-4a56-b41b-0a44d80cf848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934fd933-0728-4041-8151-c31f619711ef",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "8d3842d1-08af-44f0-86b8-4b3bfe37a350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "e14e00b7-c7f9-41a6-97fa-fd33fab68ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3842d1-08af-44f0-86b8-4b3bfe37a350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00afd6d0-3a1a-4976-b265-8dcf88930840",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3842d1-08af-44f0-86b8-4b3bfe37a350",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "dcfbdd62-9943-4c84-91e0-23a205fc983f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "75255bd0-5865-4e0b-b1f8-904c3da3bc38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcfbdd62-9943-4c84-91e0-23a205fc983f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52f9e95c-1c8f-44ae-a71c-93b7a1c2d018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcfbdd62-9943-4c84-91e0-23a205fc983f",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "fe2caca8-64ce-47e9-97a7-b7beb5ec5330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "7797e6ba-86d3-45ca-8da5-d4d25c034f22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe2caca8-64ce-47e9-97a7-b7beb5ec5330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f03fec5b-da6b-4fd6-bd77-5172a16a9ea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe2caca8-64ce-47e9-97a7-b7beb5ec5330",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "bfe1fe6e-acc2-4c4e-b8c8-6e9bfe2b48b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "261b839d-add8-43c0-ac1d-a68bfeefd785",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe1fe6e-acc2-4c4e-b8c8-6e9bfe2b48b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56ef8fef-c043-4184-b23e-af3b339df3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe1fe6e-acc2-4c4e-b8c8-6e9bfe2b48b2",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "9f2a072d-d2f2-4f0c-958c-cdd1f651243b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "2dcd9306-5371-42bc-94a1-928c76f25e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f2a072d-d2f2-4f0c-958c-cdd1f651243b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a428985-4d6c-4b19-9920-afdea1b6d44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f2a072d-d2f2-4f0c-958c-cdd1f651243b",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "caad836d-79be-4302-b958-1e983253158e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "8546e56e-d289-433a-b4b6-f942a4951bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caad836d-79be-4302-b958-1e983253158e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb1c2227-6299-473e-9a52-394a8bddbdec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caad836d-79be-4302-b958-1e983253158e",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "fc922ea1-1ffd-4004-853b-4341320345c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "174fc423-54be-4007-bad5-8da81b78a4bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc922ea1-1ffd-4004-853b-4341320345c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5df03315-5380-4bdd-8fc1-5155fced5e37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc922ea1-1ffd-4004-853b-4341320345c4",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "ee1d73b3-a356-4b86-a6e2-48415129339d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "aab2951f-9fde-4d3b-a8b6-97ee5b5396b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee1d73b3-a356-4b86-a6e2-48415129339d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bb0955-aaef-4190-827c-259070f9e09b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee1d73b3-a356-4b86-a6e2-48415129339d",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        },
        {
            "id": "115670cd-ee35-4787-8c19-ea23e2abfc8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "compositeImage": {
                "id": "4a5a43fb-5992-4c6e-b3be-972fcf1842b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "115670cd-ee35-4787-8c19-ea23e2abfc8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "239276e3-860f-4f26-b497-f4c912724b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "115670cd-ee35-4787-8c19-ea23e2abfc8b",
                    "LayerId": "4389fa82-f238-4d77-92b1-d0b3878ddb81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "4389fa82-f238-4d77-92b1-d0b3878ddb81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7577730-831f-4bf3-978e-2f5b3e58d7bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}