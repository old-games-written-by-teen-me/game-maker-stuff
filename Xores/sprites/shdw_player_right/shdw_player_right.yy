{
    "id": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shdw_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e867f6b2-7dd8-4697-abf6-b7293d4ffe78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "ee2d2302-91a8-4f7d-8bba-7d0d19177db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e867f6b2-7dd8-4697-abf6-b7293d4ffe78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bb0a02d-c9c4-4993-9944-950dee885126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e867f6b2-7dd8-4697-abf6-b7293d4ffe78",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "ee88e84d-e239-45d1-bdd1-832631646e53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "1b8ae9d1-c1e9-4c46-8b9d-64d166d71aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee88e84d-e239-45d1-bdd1-832631646e53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788cda64-f0b1-426c-b56d-240d55ca0830",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee88e84d-e239-45d1-bdd1-832631646e53",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "4365902b-b9c3-4c3a-995b-fd88ef4f2480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "c7ceb0da-9185-414f-9f9b-e4d3a7a498db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4365902b-b9c3-4c3a-995b-fd88ef4f2480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79726b3-3367-4b0b-849e-0ac76e5e8295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4365902b-b9c3-4c3a-995b-fd88ef4f2480",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "341cb94a-a426-4eab-b729-9f7fa20e59c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "a4ae2df8-207b-4e3b-b73c-f9b3d3db97ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "341cb94a-a426-4eab-b729-9f7fa20e59c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff1792c2-e240-4fd4-9d6e-18d6537afbca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "341cb94a-a426-4eab-b729-9f7fa20e59c9",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "2aa35f88-043f-4eab-987f-705446488f8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "72450916-b118-4873-9c69-603223b1e5df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa35f88-043f-4eab-987f-705446488f8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434ab5b7-fa23-41e6-bb96-66db9dc160df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa35f88-043f-4eab-987f-705446488f8b",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "6811071d-5115-42de-9b4f-5c298a2d1f7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "bbe09456-c9e6-40c8-937e-54d64173e4d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6811071d-5115-42de-9b4f-5c298a2d1f7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d592acf8-e90c-4e27-9473-fc6fb6d8c8e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6811071d-5115-42de-9b4f-5c298a2d1f7f",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "a3118e1b-2bed-4a5c-b521-0edb3725f60f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "445660f6-e2f0-4b49-90bc-40fa0b2d49c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3118e1b-2bed-4a5c-b521-0edb3725f60f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35526f3e-c641-4564-a581-0db20a3754cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3118e1b-2bed-4a5c-b521-0edb3725f60f",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "889bd8d5-e8ce-4758-9287-69b4a7487a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "dc8df53f-d401-416b-b927-b0ca028a0743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "889bd8d5-e8ce-4758-9287-69b4a7487a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "812d1247-4ed8-4309-9fd2-2d6e33da05ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "889bd8d5-e8ce-4758-9287-69b4a7487a47",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "c46e1f7e-2c91-49e1-b5d4-112917fff00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "83319899-47d6-4052-8697-e7f6327ce991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46e1f7e-2c91-49e1-b5d4-112917fff00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d7c7574-bfa3-4514-9dcb-e6a7fe46a500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46e1f7e-2c91-49e1-b5d4-112917fff00c",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "50d4d83b-eb18-45e7-b593-661a60691e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "35c21e44-fbc0-46d5-b064-befe49c4761a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d4d83b-eb18-45e7-b593-661a60691e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb6615c-2b0d-4470-8272-7500dc45645c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d4d83b-eb18-45e7-b593-661a60691e89",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "0d1c4527-e6a4-4f53-9275-6e981c624ba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "e1a7724b-8af2-486d-afd5-3a0109512f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1c4527-e6a4-4f53-9275-6e981c624ba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab49932-1245-457c-b0bd-64a056e69143",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1c4527-e6a4-4f53-9275-6e981c624ba4",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        },
        {
            "id": "2606dea3-67f6-4137-92ca-2525d535509d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "compositeImage": {
                "id": "5db8470c-6e65-4964-9a5a-67c2c1bf5413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2606dea3-67f6-4137-92ca-2525d535509d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ed565cc-4173-4412-8e63-72344722b951",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2606dea3-67f6-4137-92ca-2525d535509d",
                    "LayerId": "62c60676-e12e-4d7d-9f10-0e603e585961"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "62c60676-e12e-4d7d-9f10-0e603e585961",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad85ecd0-be7d-4da7-92bb-78e35b4aedb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}