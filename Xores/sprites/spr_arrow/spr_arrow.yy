{
    "id": "e14c5ac6-e915-42fc-8a4a-e75cea3a9406",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7f43645-2cea-4478-beda-937a5f41b90f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e14c5ac6-e915-42fc-8a4a-e75cea3a9406",
            "compositeImage": {
                "id": "294c72c8-eea7-46be-9ba0-ebe1b92530b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f43645-2cea-4478-beda-937a5f41b90f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff71d46-b5b8-4a4c-82ee-451cd0b75be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f43645-2cea-4478-beda-937a5f41b90f",
                    "LayerId": "0d2152d8-c7fe-4746-b864-c3b303a60132"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "0d2152d8-c7fe-4746-b864-c3b303a60132",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e14c5ac6-e915-42fc-8a4a-e75cea3a9406",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 1
}