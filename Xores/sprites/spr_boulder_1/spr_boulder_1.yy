{
    "id": "8ffafb04-2854-43ac-8249-c6cdd7309050",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boulder_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8f0cea5-f8f2-4bdd-9d3b-58d0ba3b612a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ffafb04-2854-43ac-8249-c6cdd7309050",
            "compositeImage": {
                "id": "494c84dc-61c4-447f-b156-d7d1e024daae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8f0cea5-f8f2-4bdd-9d3b-58d0ba3b612a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8b975f-dc4a-4144-946a-4b6d86fff6b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8f0cea5-f8f2-4bdd-9d3b-58d0ba3b612a",
                    "LayerId": "8ae6d37f-a4ab-43c8-a327-f1e06006dba3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "8ae6d37f-a4ab-43c8-a327-f1e06006dba3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ffafb04-2854-43ac-8249-c6cdd7309050",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 15
}