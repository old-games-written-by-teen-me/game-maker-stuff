{
    "id": "f2988d65-a09c-4f60-8a7a-310cc9d5929f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 2,
    "bbox_right": 97,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2caf8235-6c83-4d32-88d0-87f0a9382a31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f2988d65-a09c-4f60-8a7a-310cc9d5929f",
            "compositeImage": {
                "id": "4f71283a-954c-488c-986c-ec77f82c4615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2caf8235-6c83-4d32-88d0-87f0a9382a31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f26f0fe-b651-4b29-9fc9-a6223b04753c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2caf8235-6c83-4d32-88d0-87f0a9382a31",
                    "LayerId": "b12c54e3-ac52-4bbf-ba6b-f25b4588d409"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "b12c54e3-ac52-4bbf-ba6b-f25b4588d409",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f2988d65-a09c-4f60-8a7a-310cc9d5929f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 12
}