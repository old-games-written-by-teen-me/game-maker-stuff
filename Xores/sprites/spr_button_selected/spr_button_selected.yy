{
    "id": "bc0efcae-9842-44a6-83b0-de83d43b3986",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button_selected",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 98,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52b3406d-1ca9-442b-9aa9-c0accce32024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc0efcae-9842-44a6-83b0-de83d43b3986",
            "compositeImage": {
                "id": "48c0537c-c277-41e7-a1ee-2e3484f8df7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b3406d-1ca9-442b-9aa9-c0accce32024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2742d38e-ee3a-4667-a67a-08dbedca0c3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b3406d-1ca9-442b-9aa9-c0accce32024",
                    "LayerId": "06af238f-faa8-45a1-a065-ae0f4dd91771"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "06af238f-faa8-45a1-a065-ae0f4dd91771",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc0efcae-9842-44a6-83b0-de83d43b3986",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 50,
    "yorig": 12
}