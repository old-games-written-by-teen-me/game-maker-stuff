{
    "id": "f7ee6465-5ef2-43e9-a46f-7d77019696ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chinlin_down_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a54268c-0a77-4fcb-9ece-f648e1ffc1fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ee6465-5ef2-43e9-a46f-7d77019696ce",
            "compositeImage": {
                "id": "aeabb57e-cdcc-46e1-997e-ba1e129bbc81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a54268c-0a77-4fcb-9ece-f648e1ffc1fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6260b3b-67db-4ff9-81d2-9ea8b3979875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a54268c-0a77-4fcb-9ece-f648e1ffc1fc",
                    "LayerId": "553de51a-0929-495f-a3c6-0a79e7e1682c"
                }
            ]
        },
        {
            "id": "6fd37e73-82e1-46ad-8b0e-ff7ef5a71ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ee6465-5ef2-43e9-a46f-7d77019696ce",
            "compositeImage": {
                "id": "0fb608d4-288b-4d7a-aa17-64f308bed94d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd37e73-82e1-46ad-8b0e-ff7ef5a71ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0aecd7-36fd-4493-87b9-f3b04b5975d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd37e73-82e1-46ad-8b0e-ff7ef5a71ee5",
                    "LayerId": "553de51a-0929-495f-a3c6-0a79e7e1682c"
                }
            ]
        },
        {
            "id": "1f4a47c2-1a02-4dc2-be2f-b19afb3906b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ee6465-5ef2-43e9-a46f-7d77019696ce",
            "compositeImage": {
                "id": "b252c1be-a3c0-4b4e-9ffe-7b7369518589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4a47c2-1a02-4dc2-be2f-b19afb3906b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a402bd-d99c-47f8-821e-fe40d6f62eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4a47c2-1a02-4dc2-be2f-b19afb3906b2",
                    "LayerId": "553de51a-0929-495f-a3c6-0a79e7e1682c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "553de51a-0929-495f-a3c6-0a79e7e1682c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7ee6465-5ef2-43e9-a46f-7d77019696ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}