{
    "id": "2b512c73-e085-49ef-9e47-cf8161a67521",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chinlin_down_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcb6c81b-6cbc-4fbc-ae7c-2f1f754a740d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b512c73-e085-49ef-9e47-cf8161a67521",
            "compositeImage": {
                "id": "3aa6babc-22f5-4483-845f-6173d66376d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb6c81b-6cbc-4fbc-ae7c-2f1f754a740d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944fbfea-d200-42c3-87e0-9ac111ca755d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb6c81b-6cbc-4fbc-ae7c-2f1f754a740d",
                    "LayerId": "3495b36c-4ecb-4b62-ac33-b44f04cee248"
                }
            ]
        },
        {
            "id": "ca11cc16-71a8-4e24-9232-62bb78454548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b512c73-e085-49ef-9e47-cf8161a67521",
            "compositeImage": {
                "id": "c9d1254f-5763-4a4c-bc40-c36a8da33442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca11cc16-71a8-4e24-9232-62bb78454548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dee8a77-b798-45a5-b8de-2036baee980e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca11cc16-71a8-4e24-9232-62bb78454548",
                    "LayerId": "3495b36c-4ecb-4b62-ac33-b44f04cee248"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3495b36c-4ecb-4b62-ac33-b44f04cee248",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b512c73-e085-49ef-9e47-cf8161a67521",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}