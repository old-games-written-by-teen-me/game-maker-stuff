{
    "id": "9cc18ff9-642b-4219-afa7-25347658c732",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chinlin_right_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 3,
    "bbox_right": 21,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a633ebf8-6d2b-4b9a-8e9c-69ef2183f2a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cc18ff9-642b-4219-afa7-25347658c732",
            "compositeImage": {
                "id": "b3e09892-769f-4261-9214-e89ff0434866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a633ebf8-6d2b-4b9a-8e9c-69ef2183f2a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90ae34a6-1fd5-431c-99d8-7fddee6a3ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a633ebf8-6d2b-4b9a-8e9c-69ef2183f2a6",
                    "LayerId": "9850fd56-5432-460d-8976-41f98e4d711d"
                }
            ]
        },
        {
            "id": "a3626f25-e512-4414-864a-76b0972816a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cc18ff9-642b-4219-afa7-25347658c732",
            "compositeImage": {
                "id": "297db5fd-a469-410a-86f4-98c3321947cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3626f25-e512-4414-864a-76b0972816a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a245969-9910-45c2-a10a-a2e7d8d140a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3626f25-e512-4414-864a-76b0972816a9",
                    "LayerId": "9850fd56-5432-460d-8976-41f98e4d711d"
                }
            ]
        },
        {
            "id": "bde06f98-3c92-4943-b1f9-1c66f8dc242e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cc18ff9-642b-4219-afa7-25347658c732",
            "compositeImage": {
                "id": "c8404c46-fd34-4bc6-9e98-88294692f04b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde06f98-3c92-4943-b1f9-1c66f8dc242e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "355ff0c8-a4a3-4e15-a446-f82750cce6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde06f98-3c92-4943-b1f9-1c66f8dc242e",
                    "LayerId": "9850fd56-5432-460d-8976-41f98e4d711d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "9850fd56-5432-460d-8976-41f98e4d711d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cc18ff9-642b-4219-afa7-25347658c732",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}