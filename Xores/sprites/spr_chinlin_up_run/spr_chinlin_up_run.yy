{
    "id": "2e65d87a-61a4-4522-b03f-a169ba0d3a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chinlin_up_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f18e07fb-7651-435c-892f-1335b3a14619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e65d87a-61a4-4522-b03f-a169ba0d3a5c",
            "compositeImage": {
                "id": "9f573e05-f7f7-487e-94fc-3103e9a21fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f18e07fb-7651-435c-892f-1335b3a14619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4ee8c3-991a-4185-8c0c-24009eda0305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f18e07fb-7651-435c-892f-1335b3a14619",
                    "LayerId": "b8ad3c7c-bf10-4d51-a481-179d3e4f9fb9"
                }
            ]
        },
        {
            "id": "b5877ed4-8679-43d1-9be8-b791455e0f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e65d87a-61a4-4522-b03f-a169ba0d3a5c",
            "compositeImage": {
                "id": "b70f3577-05a0-468b-a9a7-cac217542ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5877ed4-8679-43d1-9be8-b791455e0f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5218a25-e61b-416d-afd3-9a0e0697af09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5877ed4-8679-43d1-9be8-b791455e0f40",
                    "LayerId": "b8ad3c7c-bf10-4d51-a481-179d3e4f9fb9"
                }
            ]
        },
        {
            "id": "7e4ec81f-c970-4358-8347-3708efe5dfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e65d87a-61a4-4522-b03f-a169ba0d3a5c",
            "compositeImage": {
                "id": "9c0891c6-0b80-412f-8875-8e2eb7a36741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e4ec81f-c970-4358-8347-3708efe5dfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14aab5c-9379-4ef3-8f62-211d3f95f703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e4ec81f-c970-4358-8347-3708efe5dfc2",
                    "LayerId": "b8ad3c7c-bf10-4d51-a481-179d3e4f9fb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b8ad3c7c-bf10-4d51-a481-179d3e4f9fb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e65d87a-61a4-4522-b03f-a169ba0d3a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}