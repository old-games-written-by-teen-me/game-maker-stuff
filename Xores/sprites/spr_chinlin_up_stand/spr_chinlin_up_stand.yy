{
    "id": "3252cb7a-97e5-472c-8b62-a9b5881fde72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chinlin_up_stand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 6,
    "bbox_right": 17,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab86e5c4-e4d4-4b7a-9ad9-6d8eef9b1815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3252cb7a-97e5-472c-8b62-a9b5881fde72",
            "compositeImage": {
                "id": "008ba6ec-4563-47cc-a44b-fdbeb461a14f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab86e5c4-e4d4-4b7a-9ad9-6d8eef9b1815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58184fee-ca12-436e-9bf4-564ce9751baf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab86e5c4-e4d4-4b7a-9ad9-6d8eef9b1815",
                    "LayerId": "c5d31405-fa67-4731-9784-0ebb68a98892"
                }
            ]
        },
        {
            "id": "3a583287-bbea-4489-9c5a-7667eda4c924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3252cb7a-97e5-472c-8b62-a9b5881fde72",
            "compositeImage": {
                "id": "f0f25d9d-0c68-4003-bd5a-0a9e2eee079f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a583287-bbea-4489-9c5a-7667eda4c924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c6f2f7-1d5d-44fb-890a-13d053a75670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a583287-bbea-4489-9c5a-7667eda4c924",
                    "LayerId": "c5d31405-fa67-4731-9784-0ebb68a98892"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "c5d31405-fa67-4731-9784-0ebb68a98892",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3252cb7a-97e5-472c-8b62-a9b5881fde72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}