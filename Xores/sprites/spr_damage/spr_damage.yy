{
    "id": "284cfe25-5b79-4cd1-884c-c409e74eaa6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1468c1ef-8e85-494e-ad17-bf1a4f51d358",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "284cfe25-5b79-4cd1-884c-c409e74eaa6b",
            "compositeImage": {
                "id": "3abb96d9-8c29-4e62-b36a-f528f613d5e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1468c1ef-8e85-494e-ad17-bf1a4f51d358",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d3ac18c-7237-4c40-8ee4-98ca76ae64d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1468c1ef-8e85-494e-ad17-bf1a4f51d358",
                    "LayerId": "e994c41f-7475-461c-a6d6-eddfec637a9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e994c41f-7475-461c-a6d6-eddfec637a9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "284cfe25-5b79-4cd1-884c-c409e74eaa6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}