{
    "id": "64bab6c1-427f-44a3-930a-6b7d40fbccbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialogue_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b1b032c-fdff-4005-a72a-434021bb9fac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64bab6c1-427f-44a3-930a-6b7d40fbccbc",
            "compositeImage": {
                "id": "326848a4-55cc-4fe2-bda4-18a3ac8c963c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1b032c-fdff-4005-a72a-434021bb9fac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84fe64bb-fef1-4f4d-ad06-1177052f801a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1b032c-fdff-4005-a72a-434021bb9fac",
                    "LayerId": "5adbd377-fde1-4635-b18b-4c81e18c0296"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "5adbd377-fde1-4635-b18b-4c81e18c0296",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64bab6c1-427f-44a3-930a-6b7d40fbccbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}