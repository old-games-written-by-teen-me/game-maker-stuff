{
    "id": "058e44b4-6ca2-43fa-b05f-dcf236c413f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dialouge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04f822f6-5119-4dbc-a414-eb615bba556f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "058e44b4-6ca2-43fa-b05f-dcf236c413f9",
            "compositeImage": {
                "id": "cabf5707-3aaf-4df3-b488-fc83f44b46f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f822f6-5119-4dbc-a414-eb615bba556f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1727079d-17de-4f37-9a18-e2eb4a3c3468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f822f6-5119-4dbc-a414-eb615bba556f",
                    "LayerId": "9cf7f954-c8cb-45a8-9e21-823e5a27f581"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9cf7f954-c8cb-45a8-9e21-823e5a27f581",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "058e44b4-6ca2-43fa-b05f-dcf236c413f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}