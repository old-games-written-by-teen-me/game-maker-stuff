{
    "id": "166daf08-8ac4-4d92-8c11-697e6d40d24a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2849896b-3221-4752-ab3e-7b1991439c4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "166daf08-8ac4-4d92-8c11-697e6d40d24a",
            "compositeImage": {
                "id": "373125a3-350b-4eca-bfbb-77f589b00eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2849896b-3221-4752-ab3e-7b1991439c4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e318f1-7f85-4029-a893-a096a1cb2393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2849896b-3221-4752-ab3e-7b1991439c4b",
                    "LayerId": "22d30e30-492e-40c6-abe7-6d90d7948283"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "22d30e30-492e-40c6-abe7-6d90d7948283",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "166daf08-8ac4-4d92-8c11-697e6d40d24a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}