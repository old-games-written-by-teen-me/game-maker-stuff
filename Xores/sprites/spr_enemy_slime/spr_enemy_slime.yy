{
    "id": "ec8bc24f-0d63-4200-80f3-2cfaa64ea7f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_slime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 4,
    "bbox_right": 20,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89b9dcdc-edb1-4d0e-8d55-9ad9e79d8f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec8bc24f-0d63-4200-80f3-2cfaa64ea7f8",
            "compositeImage": {
                "id": "16ed5713-cd78-43dc-9a38-7b0100f641c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b9dcdc-edb1-4d0e-8d55-9ad9e79d8f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8163e34-c79e-4927-9850-b76db28e8525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b9dcdc-edb1-4d0e-8d55-9ad9e79d8f83",
                    "LayerId": "4d4d7c46-58eb-4471-a717-9ed140399d78"
                }
            ]
        },
        {
            "id": "59479dc0-b5da-4af4-963b-1363bab13c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec8bc24f-0d63-4200-80f3-2cfaa64ea7f8",
            "compositeImage": {
                "id": "f6d09cc6-d178-47f1-a1d4-023c6db48e84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59479dc0-b5da-4af4-963b-1363bab13c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77796eea-39a7-4ddc-9fd3-f81e91f84ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59479dc0-b5da-4af4-963b-1363bab13c5c",
                    "LayerId": "4d4d7c46-58eb-4471-a717-9ed140399d78"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4d4d7c46-58eb-4471-a717-9ed140399d78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec8bc24f-0d63-4200-80f3-2cfaa64ea7f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 8
}