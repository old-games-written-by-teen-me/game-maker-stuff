{
    "id": "d62874bb-2dca-4ff1-8272-3e9b2304531c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_expr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3520d322-4315-4766-ae1a-fc38643a5193",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d62874bb-2dca-4ff1-8272-3e9b2304531c",
            "compositeImage": {
                "id": "65494ec4-0218-4ee6-9e1f-fd4fb6a12284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3520d322-4315-4766-ae1a-fc38643a5193",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d79677-6508-40ba-a669-4e2e2f469a15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3520d322-4315-4766-ae1a-fc38643a5193",
                    "LayerId": "fca638a5-30ca-4693-a3e6-06be49911df9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fca638a5-30ca-4693-a3e6-06be49911df9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d62874bb-2dca-4ff1-8272-3e9b2304531c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}