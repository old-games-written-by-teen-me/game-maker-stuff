{
    "id": "8d619919-91b5-407c-b3b8-a15c00ce8236",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "899926bf-0ee1-4dbf-b466-64a13546b719",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "cc57ff0a-5e79-4223-bdd4-597362fb69b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899926bf-0ee1-4dbf-b466-64a13546b719",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "475f00fb-bef9-4800-bcf2-bc8aa063e86c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899926bf-0ee1-4dbf-b466-64a13546b719",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        },
        {
            "id": "7c43dd39-3c6d-43a0-96a4-f8929a57dd63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "d8bd3ae4-1a7c-4114-8379-3d166dd742e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c43dd39-3c6d-43a0-96a4-f8929a57dd63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc855a07-19be-4f58-812e-26d8ce23daf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c43dd39-3c6d-43a0-96a4-f8929a57dd63",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        },
        {
            "id": "a557ccf9-8ddd-404f-9f39-28cfa712f75d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "e3474acf-d81a-4fa8-a61f-b49d59e45c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a557ccf9-8ddd-404f-9f39-28cfa712f75d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bc8dbeb-05a1-49a0-88e8-28b681dfabda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a557ccf9-8ddd-404f-9f39-28cfa712f75d",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        },
        {
            "id": "a74df9fb-e122-4e33-81fe-57f8f2e5dc91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "1e81a395-211f-40e0-a94e-5e34ed139642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74df9fb-e122-4e33-81fe-57f8f2e5dc91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db919dce-fde1-4da1-9b5c-4254ce739d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74df9fb-e122-4e33-81fe-57f8f2e5dc91",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        },
        {
            "id": "fdaed9cd-0224-4813-b015-ff1cfc354fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "232f9df7-c0e3-4e6e-a3d9-e359b0f511e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdaed9cd-0224-4813-b015-ff1cfc354fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "648b0820-9e1d-4715-83ed-ab025bb777c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdaed9cd-0224-4813-b015-ff1cfc354fc1",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        },
        {
            "id": "9186bd15-c679-4b87-9e94-1314597b7c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "compositeImage": {
                "id": "7456d3aa-1194-44f0-8296-bfd19b468d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9186bd15-c679-4b87-9e94-1314597b7c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf4b5ac-2294-4c50-8fa7-fc524fee59ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9186bd15-c679-4b87-9e94-1314597b7c1e",
                    "LayerId": "a88c170d-4f41-419a-a72b-47c1901c0383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a88c170d-4f41-419a-a72b-47c1901c0383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d619919-91b5-407c-b3b8-a15c00ce8236",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}