{
    "id": "d934f574-144a-4be9-85e8-413ad4694a47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 6,
    "bbox_right": 38,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78851ada-6635-45c4-937c-e5f566163e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "bd6acb01-f1f9-407c-ad3f-71885bdad679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78851ada-6635-45c4-937c-e5f566163e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c99f4cb-af51-4e16-b976-53e20b9d3cd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78851ada-6635-45c4-937c-e5f566163e2a",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        },
        {
            "id": "45ced86c-ba0a-4594-85f6-1f06b379f0b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "72e121f8-69fd-40ca-ac55-d538e56fa738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ced86c-ba0a-4594-85f6-1f06b379f0b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a39fd0c-7d37-4d69-969a-fb27a56ac4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ced86c-ba0a-4594-85f6-1f06b379f0b1",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        },
        {
            "id": "819e7002-10d9-4186-93b7-984e8a711547",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "d66eac61-525e-499f-98bd-33174241c8bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "819e7002-10d9-4186-93b7-984e8a711547",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cd67325-5c13-4026-9837-3a5f8c2a0fa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "819e7002-10d9-4186-93b7-984e8a711547",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        },
        {
            "id": "fc48fb27-8f79-435b-bcb6-5700e8877939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "0a0fc56d-2848-4a30-9401-322d3df9fb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc48fb27-8f79-435b-bcb6-5700e8877939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668bbac0-58d3-4f27-9513-16f1c70423c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc48fb27-8f79-435b-bcb6-5700e8877939",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        },
        {
            "id": "e42e9a3e-29fe-46c2-92e0-36d67f45ee74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "aa269174-7e03-4356-bf92-31c78a37689e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e42e9a3e-29fe-46c2-92e0-36d67f45ee74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1832e16e-44c2-4bb3-8c3c-57e674245fd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e42e9a3e-29fe-46c2-92e0-36d67f45ee74",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        },
        {
            "id": "76e06fce-d54a-4dbd-941c-d187009c7f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "compositeImage": {
                "id": "2dd6600a-b59f-40e8-9727-fc1b729a405c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e06fce-d54a-4dbd-941c-d187009c7f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88a48e7-f577-48e0-b5bb-1c19e8cd9150",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e06fce-d54a-4dbd-941c-d187009c7f66",
                    "LayerId": "0dca713d-a760-49ba-88f3-f06929f80acb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "0dca713d-a760-49ba-88f3-f06929f80acb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d934f574-144a-4be9-85e8-413ad4694a47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}