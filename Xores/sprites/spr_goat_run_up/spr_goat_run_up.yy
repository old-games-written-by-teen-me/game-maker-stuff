{
    "id": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acb334d4-b303-42c9-9227-e667ea1b9c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "03e37c03-1c29-47ea-bee8-e8f3f5fce693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acb334d4-b303-42c9-9227-e667ea1b9c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152b026c-5a03-4be9-b96e-323e71c7c551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acb334d4-b303-42c9-9227-e667ea1b9c9a",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        },
        {
            "id": "75ed06b7-6ea8-4ca3-b034-4c1e977447f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "07c1f316-f7c2-40cb-8977-190461de64e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ed06b7-6ea8-4ca3-b034-4c1e977447f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11331d2-cb90-4bdb-a885-dc1d6c8d78fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ed06b7-6ea8-4ca3-b034-4c1e977447f7",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        },
        {
            "id": "a4689daa-152d-4a88-8350-0d9f602f0484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "e76617b3-dc18-478a-b1ad-30e2a02093f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4689daa-152d-4a88-8350-0d9f602f0484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e54d1dbd-8058-409f-8fbe-4739ddd87f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4689daa-152d-4a88-8350-0d9f602f0484",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        },
        {
            "id": "e8425323-c1d7-41f0-ac8b-89baf8ff4c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "311d40a1-35e1-4f94-89a1-b92a4fe7a0a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8425323-c1d7-41f0-ac8b-89baf8ff4c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e33154ef-c5bb-4fa7-8491-bb5ecb7e9fab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8425323-c1d7-41f0-ac8b-89baf8ff4c76",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        },
        {
            "id": "91c26e13-3f51-45d8-9038-f3bc2fd6cd5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "9ea94a2e-8091-4772-b48a-65edba09a98d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c26e13-3f51-45d8-9038-f3bc2fd6cd5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25177358-55ca-47f1-ba8b-2290a8f718e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c26e13-3f51-45d8-9038-f3bc2fd6cd5b",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        },
        {
            "id": "f8d80851-d007-42f8-8ae9-4f2f5bad1996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "compositeImage": {
                "id": "d86ef8a3-951f-4d80-b0ea-9ab6a74e5be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d80851-d007-42f8-8ae9-4f2f5bad1996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f5ad040-6e97-4698-9768-84e4cd169096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d80851-d007-42f8-8ae9-4f2f5bad1996",
                    "LayerId": "06b4e6f7-f13a-4565-9ac6-050340759ef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "06b4e6f7-f13a-4565-9ac6-050340759ef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e46e55de-d0a8-4d9d-9f28-72fdbd825518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}