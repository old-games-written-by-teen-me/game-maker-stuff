{
    "id": "dd21e207-59cb-4329-865b-4d7e3308f835",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_stand_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "123e498e-1aa1-43bf-8c45-2e2f546f14d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd21e207-59cb-4329-865b-4d7e3308f835",
            "compositeImage": {
                "id": "7a7949fd-d901-4ee5-8061-ca52bd7036f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "123e498e-1aa1-43bf-8c45-2e2f546f14d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25e04d88-cd42-4c68-8a79-9a06abf9bbf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "123e498e-1aa1-43bf-8c45-2e2f546f14d7",
                    "LayerId": "93d82219-2e04-40a1-8547-51eb9271d56b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "93d82219-2e04-40a1-8547-51eb9271d56b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd21e207-59cb-4329-865b-4d7e3308f835",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}