{
    "id": "a5856a54-05bc-4b8c-a4f4-d4a7b87123ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_stand_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7adc3ad6-833a-4600-b5ff-dde22a0ba9de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5856a54-05bc-4b8c-a4f4-d4a7b87123ea",
            "compositeImage": {
                "id": "e0927649-eddb-44cd-8100-f46ab10c3aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adc3ad6-833a-4600-b5ff-dde22a0ba9de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19008cda-dfa8-4922-b6be-e2a142c78e8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adc3ad6-833a-4600-b5ff-dde22a0ba9de",
                    "LayerId": "e068ce57-4739-4b5b-b45c-536f36c7f74d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "e068ce57-4739-4b5b-b45c-536f36c7f74d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5856a54-05bc-4b8c-a4f4-d4a7b87123ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 32
}