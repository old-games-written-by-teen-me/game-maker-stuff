{
    "id": "e8b5aff2-0400-42df-8922-f795e76eaacf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_goat_stand_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 18,
    "bbox_right": 29,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01690e09-c4f8-49f0-a694-340ffdd881f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8b5aff2-0400-42df-8922-f795e76eaacf",
            "compositeImage": {
                "id": "b8c3bd43-7bc8-4a77-b0c3-07d798b64f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01690e09-c4f8-49f0-a694-340ffdd881f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1790ada1-98e4-4b63-9b95-f45ff4a3b054",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01690e09-c4f8-49f0-a694-340ffdd881f1",
                    "LayerId": "d3f3617f-7e69-48fd-813a-947bf5d7c688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "d3f3617f-7e69-48fd-813a-947bf5d7c688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8b5aff2-0400-42df-8922-f795e76eaacf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}