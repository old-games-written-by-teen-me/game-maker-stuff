{
    "id": "863fc26c-de79-44ec-8e7a-65610ec565bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hatchet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb85cf0c-c5bd-4294-9b2e-26284449a83f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863fc26c-de79-44ec-8e7a-65610ec565bd",
            "compositeImage": {
                "id": "e83031ff-6264-4205-853d-eaa7afd36f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb85cf0c-c5bd-4294-9b2e-26284449a83f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2539683d-c60e-4d63-b9bc-a706252e8d8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb85cf0c-c5bd-4294-9b2e-26284449a83f",
                    "LayerId": "fcaf315a-e60b-47a4-a336-2dbdc4df1c54"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "fcaf315a-e60b-47a4-a336-2dbdc4df1c54",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "863fc26c-de79-44ec-8e7a-65610ec565bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}