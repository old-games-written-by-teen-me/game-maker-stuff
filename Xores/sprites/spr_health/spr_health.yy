{
    "id": "75b009db-9980-46fe-98f0-c8b1c00993d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cfbbef3-e743-41f3-8db0-875bd476a661",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75b009db-9980-46fe-98f0-c8b1c00993d3",
            "compositeImage": {
                "id": "e173232b-9a60-48b4-b804-41d8996d2ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cfbbef3-e743-41f3-8db0-875bd476a661",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b46da963-d329-4c44-b0b8-ea3a3f28f291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cfbbef3-e743-41f3-8db0-875bd476a661",
                    "LayerId": "46c8b683-1194-4eb7-be8b-1f8b983a9981"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "46c8b683-1194-4eb7-be8b-1f8b983a9981",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75b009db-9980-46fe-98f0-c8b1c00993d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}