{
    "id": "adcb8970-5314-44d2-8f9b-5e9353dccf7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_health_bar_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 7,
    "bbox_right": 62,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc96d99b-3bd8-41f6-a330-62de904e1b95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adcb8970-5314-44d2-8f9b-5e9353dccf7a",
            "compositeImage": {
                "id": "79b9e198-61c0-4af3-a35b-739b79d3b166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc96d99b-3bd8-41f6-a330-62de904e1b95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e764e1a3-e34a-4f2f-84f7-ac69f7b87184",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc96d99b-3bd8-41f6-a330-62de904e1b95",
                    "LayerId": "0834d2b2-b7f1-4219-a09e-c729d5ff1755"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "0834d2b2-b7f1-4219-a09e-c729d5ff1755",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adcb8970-5314-44d2-8f9b-5e9353dccf7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 11,
    "yorig": 2
}