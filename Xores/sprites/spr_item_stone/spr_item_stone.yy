{
    "id": "39af0bb8-102e-44e0-a6c9-93c72c4b039b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c67862dc-4e47-4342-88a2-7fc9a615d2ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39af0bb8-102e-44e0-a6c9-93c72c4b039b",
            "compositeImage": {
                "id": "4ae50e66-9fe0-460b-93a2-6a4b0ed31e49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c67862dc-4e47-4342-88a2-7fc9a615d2ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ae295e2-1177-47d2-9759-9fb4ebdebdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c67862dc-4e47-4342-88a2-7fc9a615d2ca",
                    "LayerId": "f39c7f11-43ab-4e25-9f8f-ca5f4b1f16c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f39c7f11-43ab-4e25-9f8f-ca5f4b1f16c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39af0bb8-102e-44e0-a6c9-93c72c4b039b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}