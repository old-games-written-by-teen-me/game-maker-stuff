{
    "id": "badd6593-a5da-4dc5-bd12-472da841d3ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_item_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6494de66-30ca-4a3b-82c8-002ba44e381e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "badd6593-a5da-4dc5-bd12-472da841d3ca",
            "compositeImage": {
                "id": "2f0a1cb2-0f31-4eeb-ac85-beb84b74950a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6494de66-30ca-4a3b-82c8-002ba44e381e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e6f764-42bc-4f8a-8118-737f1c504d1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6494de66-30ca-4a3b-82c8-002ba44e381e",
                    "LayerId": "01e9461e-31f8-454f-ac0b-91e9e2a2276e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "01e9461e-31f8-454f-ac0b-91e9e2a2276e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "badd6593-a5da-4dc5-bd12-472da841d3ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}