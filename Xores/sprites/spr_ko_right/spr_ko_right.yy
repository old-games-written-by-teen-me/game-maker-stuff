{
    "id": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ko_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b899890-0f0d-422d-9a07-80bbf88ba8d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "770c7170-4d2e-4e68-8684-d4c896d4920f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b899890-0f0d-422d-9a07-80bbf88ba8d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d17af1d-9b1f-4045-920e-36a45f5caa13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b899890-0f0d-422d-9a07-80bbf88ba8d8",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        },
        {
            "id": "ce6a82ac-e23f-4d86-887f-a39d655198a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "090916f2-57ea-4609-8185-b6c34bb1f778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6a82ac-e23f-4d86-887f-a39d655198a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0373ec1-fed5-4aad-9407-251df0d02980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6a82ac-e23f-4d86-887f-a39d655198a1",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        },
        {
            "id": "13912ccb-42f2-4788-8255-d03f33405563",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "1e4774ae-44d5-4c9b-ae56-8bdef37df73c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13912ccb-42f2-4788-8255-d03f33405563",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "706ac4af-bcb7-4bd8-981e-4b990cfc1088",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13912ccb-42f2-4788-8255-d03f33405563",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        },
        {
            "id": "1f32938f-9a00-443f-b8a8-d993f453ca39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "a1aaede0-0c72-497e-a0da-eb2e2d8f5a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f32938f-9a00-443f-b8a8-d993f453ca39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e99f13-2e38-4e22-a4cb-4dc310a3a622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f32938f-9a00-443f-b8a8-d993f453ca39",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        },
        {
            "id": "bedc7e77-1e59-4b7d-b05b-60930782f818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "828a4b05-4519-4cde-89e1-9de4acce7cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bedc7e77-1e59-4b7d-b05b-60930782f818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1aa7ed7-a0c8-4d4c-8604-d409c50e7777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bedc7e77-1e59-4b7d-b05b-60930782f818",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        },
        {
            "id": "edbd886b-1bdf-4ea9-a145-5ae0686e4bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "compositeImage": {
                "id": "0aca94c9-780d-4ba9-81d5-b2aa71e0d0f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edbd886b-1bdf-4ea9-a145-5ae0686e4bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb73fd39-3bfe-4d2a-a2b6-d9b420e5354c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edbd886b-1bdf-4ea9-a145-5ae0686e4bc3",
                    "LayerId": "0d88c868-a427-49d4-9209-c26d98debdda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d88c868-a427-49d4-9209-c26d98debdda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e5a5dd-1846-4f9a-98c2-1da5ff45f78d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}