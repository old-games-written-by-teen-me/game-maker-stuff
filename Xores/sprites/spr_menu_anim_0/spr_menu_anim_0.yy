{
    "id": "07deb351-8197-4617-b7a5-4251c3e95e5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_anim_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 167,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e28b13f0-ac38-4343-a096-7762f8133cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "868edac5-7aaa-4caf-9c51-8fddbf73d06c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28b13f0-ac38-4343-a096-7762f8133cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cbbb994-8618-49de-a7b5-b1a6158f09c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28b13f0-ac38-4343-a096-7762f8133cc7",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "36ce1c4b-b411-47aa-a383-067d35422b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "c1c27d28-f7b6-445a-979b-e9da6479d9fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ce1c4b-b411-47aa-a383-067d35422b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e43983-b6f9-4a21-893d-2619381b5287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ce1c4b-b411-47aa-a383-067d35422b0f",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "9d339d57-d2b2-44a4-afaa-6ade57df2255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "25f9955b-9ae2-433e-b7de-d5cf809c31d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d339d57-d2b2-44a4-afaa-6ade57df2255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33712895-e156-42a6-9b05-834a58d6471d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d339d57-d2b2-44a4-afaa-6ade57df2255",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "6fe6d464-3064-41eb-93d9-2a50c34120e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "94098080-5596-4933-ae95-b62ed095be17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe6d464-3064-41eb-93d9-2a50c34120e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab1271e-4d26-471a-8bea-7651859658ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe6d464-3064-41eb-93d9-2a50c34120e5",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "fc9f2d21-8685-44be-8c6d-4031006e48f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "f856762d-c4dd-411d-b3be-910ad4677276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc9f2d21-8685-44be-8c6d-4031006e48f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd113c6d-eb78-40d7-9311-e25ec1b635f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc9f2d21-8685-44be-8c6d-4031006e48f3",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "cc46db87-793e-446b-a15d-2a8c7a40ced4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "63e7a094-7f90-4d49-aabe-05683b30f02a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc46db87-793e-446b-a15d-2a8c7a40ced4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc684306-9f84-431a-baaa-ee6b4a8ff6af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc46db87-793e-446b-a15d-2a8c7a40ced4",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "ec1df6e3-979e-4206-a4a4-85d4551a084c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "91a82163-1409-4f68-a8b5-7a906547d756",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec1df6e3-979e-4206-a4a4-85d4551a084c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "491e7ad7-e9fc-4b20-a5fd-0b1f62b9426e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec1df6e3-979e-4206-a4a4-85d4551a084c",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "9e550a28-87dc-42b3-a7d9-661da1437126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "03bb6c1e-a744-4925-aedf-54ae033ee08a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e550a28-87dc-42b3-a7d9-661da1437126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "036f18f5-af7b-4de1-a646-7929ae0351ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e550a28-87dc-42b3-a7d9-661da1437126",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "1c7d4c9e-f056-474f-8913-ff564c93420e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "bed5dcdd-6878-493f-9a13-85f73dff4e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c7d4c9e-f056-474f-8913-ff564c93420e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f234be0-e788-4914-9d72-a1c95ebcb416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c7d4c9e-f056-474f-8913-ff564c93420e",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "83c4d645-f84c-432a-ba3a-4a06acc87d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "d77c44f7-d496-4c20-8079-de5aa0869c77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83c4d645-f84c-432a-ba3a-4a06acc87d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a032f22-25ac-4644-8c0f-2c781536bcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83c4d645-f84c-432a-ba3a-4a06acc87d32",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "d5cf4c80-d9fb-428f-bec7-69e99b35516b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "e74a2a80-74f0-43f0-80d7-e6eee3784234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5cf4c80-d9fb-428f-bec7-69e99b35516b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "321fd483-648d-4a1f-bebd-797d96dc2962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5cf4c80-d9fb-428f-bec7-69e99b35516b",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "af859d9d-e1d2-4908-afef-8544407885cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "17fa2848-3d00-48b4-950c-c148b9b13dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af859d9d-e1d2-4908-afef-8544407885cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e7d1973-ea7a-4777-a96d-0da6bbe8a070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af859d9d-e1d2-4908-afef-8544407885cf",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "befb99c1-04e4-4266-8676-7927738192f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "09706c52-e398-4861-8e00-92f3a5dffcd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "befb99c1-04e4-4266-8676-7927738192f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f31c1fcf-0c4e-4e2d-8344-7cf7b1284b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "befb99c1-04e4-4266-8676-7927738192f5",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "3fb9aca4-0fb2-4a03-ae07-ca0d1f8ad6e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "db5f59a0-b935-4d9d-bfc4-9794cf8fbf70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb9aca4-0fb2-4a03-ae07-ca0d1f8ad6e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ca3ba6-e162-49d1-b1cf-1a7775a27532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb9aca4-0fb2-4a03-ae07-ca0d1f8ad6e7",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "6fbdb855-3937-4895-89a7-cec5dbd20a87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "b8d65788-f6dc-4cd9-9872-6fe253433eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbdb855-3937-4895-89a7-cec5dbd20a87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79dea57f-55e9-4d09-a9c2-a54eddd9363e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbdb855-3937-4895-89a7-cec5dbd20a87",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "368008f0-6ea2-4f03-87ab-e4039d1a2d57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "522af107-6f31-4bc0-b517-a894b57b9ee6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368008f0-6ea2-4f03-87ab-e4039d1a2d57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97142388-e606-45e0-8a8b-c8d53f0c2f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368008f0-6ea2-4f03-87ab-e4039d1a2d57",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "73284485-9f88-478f-b13d-36572df78a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "6f32a62c-9002-41d5-8b0a-e03a413d46f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73284485-9f88-478f-b13d-36572df78a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b437c8-93b2-4a98-8394-ec4acbf63f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73284485-9f88-478f-b13d-36572df78a1f",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        },
        {
            "id": "349410f9-bec3-49d7-950a-81077da57490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "compositeImage": {
                "id": "a9f459e6-fc5e-41ed-a1c0-bff1f2a0e983",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349410f9-bec3-49d7-950a-81077da57490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88d447c1-2dd3-4c03-b7c7-699cd25473da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349410f9-bec3-49d7-950a-81077da57490",
                    "LayerId": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 55,
    "layers": [
        {
            "id": "878d4597-5d51-47d6-9e9e-3a34b0ae6eb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07deb351-8197-4617-b7a5-4251c3e95e5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 0,
    "yorig": 0
}