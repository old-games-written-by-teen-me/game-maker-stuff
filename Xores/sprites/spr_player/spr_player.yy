{
    "id": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "276b0a6d-3a25-4fe1-bb50-619880bf39ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "fe78542c-04da-40ee-9c6b-02b6939d9c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "276b0a6d-3a25-4fe1-bb50-619880bf39ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b612b7-772e-487f-b0a2-7a0189b023dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "276b0a6d-3a25-4fe1-bb50-619880bf39ee",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "6957ad75-b072-4c54-b9ad-bec522151ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "0587e40a-a51b-4542-98a7-c0594cb12d66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6957ad75-b072-4c54-b9ad-bec522151ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdacac17-3327-4ca1-a927-a9580f73f509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6957ad75-b072-4c54-b9ad-bec522151ced",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "c13c88c5-e6e8-4710-b4bf-1084ebd060cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "6a95a8ed-cc94-482f-85d6-13000606b57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c13c88c5-e6e8-4710-b4bf-1084ebd060cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68d367f1-9163-44bd-b0e0-e30306d0a52f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c13c88c5-e6e8-4710-b4bf-1084ebd060cc",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "79bd6bdf-b04d-418c-8638-263078bc78b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "ce7b51b1-8190-4a22-bcf8-963adab65b5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79bd6bdf-b04d-418c-8638-263078bc78b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f09da3-69a6-4ccf-ba73-7e0ac5e39158",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79bd6bdf-b04d-418c-8638-263078bc78b3",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "3727a88a-9e7c-4401-b335-dbd20b3bb476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "5138d9d2-59dc-4bf0-8a6e-f576e8d631bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3727a88a-9e7c-4401-b335-dbd20b3bb476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b8b4e0-cd18-4976-a94b-8fbde3ad65dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3727a88a-9e7c-4401-b335-dbd20b3bb476",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "f6374ed1-7ab9-4763-8664-aca18260eddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "ebd557be-a689-4728-acfa-60b5f300ead4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6374ed1-7ab9-4763-8664-aca18260eddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d97f84-1a84-47e6-a6c1-dd13b120154c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6374ed1-7ab9-4763-8664-aca18260eddf",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "74a886ec-1c60-4fd0-9f5d-df5856c19647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "55356cbd-26ee-4b7d-988b-83141727c7b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74a886ec-1c60-4fd0-9f5d-df5856c19647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd5e321-d298-49f1-97d2-475fe8609733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74a886ec-1c60-4fd0-9f5d-df5856c19647",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "aeed2492-857d-4d92-80fc-1d7a1a483da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "b3b8624f-6032-4551-ad16-22c7b548419b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeed2492-857d-4d92-80fc-1d7a1a483da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7dc5a3d-5bd1-43c7-b0b0-7ffda9eb0d41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeed2492-857d-4d92-80fc-1d7a1a483da4",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "98430eb8-9b10-47e3-aed0-a6109659245a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "191cd263-1b3c-49d7-912c-d93c160f8a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98430eb8-9b10-47e3-aed0-a6109659245a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f756aabb-9eac-460f-ac0e-649092823dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98430eb8-9b10-47e3-aed0-a6109659245a",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "19735233-1145-40a7-ac45-6aaa844d3197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "641726dc-684a-4bd1-83fd-6ad813f2cb12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19735233-1145-40a7-ac45-6aaa844d3197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c18a3867-81b0-4783-a51d-cf176564868e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19735233-1145-40a7-ac45-6aaa844d3197",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "03c44a90-3d37-4742-be82-6a171e2d0975",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "ecbe194a-b41b-484a-b7f0-c3b1bb2d84db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c44a90-3d37-4742-be82-6a171e2d0975",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f54b28-8257-463b-a887-b2daec2a8823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c44a90-3d37-4742-be82-6a171e2d0975",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        },
        {
            "id": "56665c98-6808-4729-a0b8-ee2220cb5348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "compositeImage": {
                "id": "370cff66-1f49-471c-9a19-fe170c218556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56665c98-6808-4729-a0b8-ee2220cb5348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caf2e25d-bbca-42c3-a3f4-65c80badbc2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56665c98-6808-4729-a0b8-ee2220cb5348",
                    "LayerId": "ff739df2-030e-48c3-ac2f-400353f91745"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ff739df2-030e-48c3-ac2f-400353f91745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8995c08e-cf0e-4fa9-a4f7-17260c9897a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}