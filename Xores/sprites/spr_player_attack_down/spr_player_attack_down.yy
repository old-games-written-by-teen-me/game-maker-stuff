{
    "id": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6b0c3b8-7d61-4741-8a86-f85bf2dc2196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "3b14a67c-d54a-4fa7-bb4e-a129297bed20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b0c3b8-7d61-4741-8a86-f85bf2dc2196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4bdd8f3-523a-4fa5-bd07-8d4747917594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b0c3b8-7d61-4741-8a86-f85bf2dc2196",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        },
        {
            "id": "b71e8e34-780f-4211-aa0c-7be064a8abb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "74c98277-b1b4-484f-9fc5-048dd79dcc80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b71e8e34-780f-4211-aa0c-7be064a8abb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6634fc0c-b2ea-4048-9853-c6a0757e7929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b71e8e34-780f-4211-aa0c-7be064a8abb7",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        },
        {
            "id": "13278219-35c4-48dd-a853-367a8bfad757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "9a83edf7-4f3c-4d6d-ba19-16fd7afafbad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13278219-35c4-48dd-a853-367a8bfad757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd4c52fa-c8f6-4e51-a4c5-90ece3d83b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13278219-35c4-48dd-a853-367a8bfad757",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        },
        {
            "id": "5342fb72-144f-46af-b500-f219db868ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "1f0b6a7e-1f31-4349-8399-3f5e461435ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5342fb72-144f-46af-b500-f219db868ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e456cd-d0c7-457d-9f56-3a690ff8037a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5342fb72-144f-46af-b500-f219db868ecc",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        },
        {
            "id": "4e0cd8fa-bbfe-49a9-b923-196eddb0e7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "350d16a0-1890-4d24-b77d-abdf2004c4a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e0cd8fa-bbfe-49a9-b923-196eddb0e7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47ebb25d-580e-4a11-896f-05b86fc0c5a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e0cd8fa-bbfe-49a9-b923-196eddb0e7b7",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        },
        {
            "id": "53eda158-8972-471f-9647-5c83a3fb4ca4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "compositeImage": {
                "id": "d3ad21fe-2386-4397-af3b-9199947a340a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53eda158-8972-471f-9647-5c83a3fb4ca4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896436c2-803a-4eae-aaa4-e807a6e3c850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53eda158-8972-471f-9647-5c83a3fb4ca4",
                    "LayerId": "9a650607-5dee-4afd-95e3-3ea4997ba780"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "9a650607-5dee-4afd-95e3-3ea4997ba780",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdceb7ec-b7d0-40eb-8385-9b0b13aa5e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 9
}