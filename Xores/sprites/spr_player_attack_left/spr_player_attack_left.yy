{
    "id": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbd35ac4-20a5-4097-b5f8-da4daf253c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "61ec2598-2550-4cbc-a4ee-39012beea5c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd35ac4-20a5-4097-b5f8-da4daf253c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f1fb82-b357-4080-896c-63bd3698b97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd35ac4-20a5-4097-b5f8-da4daf253c4e",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        },
        {
            "id": "45c23779-05ac-4d09-99e2-0adb58853ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "2ee27e31-2e78-4890-aed7-482c3f1fba98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45c23779-05ac-4d09-99e2-0adb58853ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35fb17c6-286b-4102-908e-aaab62dc0921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45c23779-05ac-4d09-99e2-0adb58853ec2",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        },
        {
            "id": "b3ee9ae6-9461-49cd-af27-42afca634c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "83385c0c-2ccf-4558-bdf6-069eb6b3f274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ee9ae6-9461-49cd-af27-42afca634c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d2d84d-24e9-49d8-9c87-20546d577d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ee9ae6-9461-49cd-af27-42afca634c01",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        },
        {
            "id": "56799dae-3108-4888-8e21-c937a467dbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "895bb74a-b226-4788-801e-8afadcacd377",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56799dae-3108-4888-8e21-c937a467dbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c08292ce-5d6e-4017-bc45-dd38593d6769",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56799dae-3108-4888-8e21-c937a467dbea",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        },
        {
            "id": "a5b47410-c51f-4243-83f0-20aa9f615db8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "8b10a249-fa51-4612-ac8c-0af0ce64905a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b47410-c51f-4243-83f0-20aa9f615db8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77cb7b1-cd69-4e32-8dd9-41b41e45e961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b47410-c51f-4243-83f0-20aa9f615db8",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        },
        {
            "id": "421b0121-939d-42ea-9fbe-09eaf072e5bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "compositeImage": {
                "id": "87fddcd2-01c9-47ff-9ae3-5142f2daece5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "421b0121-939d-42ea-9fbe-09eaf072e5bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e318e794-e577-4cff-b740-b2affc9c92b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "421b0121-939d-42ea-9fbe-09eaf072e5bc",
                    "LayerId": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "1b8a54e0-d124-4d0d-bb5c-42c10b7c005f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd6bd6e6-8957-44e5-aa9e-875a24a6dcea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 18,
    "yorig": 9
}