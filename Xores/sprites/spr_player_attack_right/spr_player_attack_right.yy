{
    "id": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "274223b0-8b0b-4186-9eb3-1bc53aed87fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "fa17d747-50f6-4125-8be6-83722373efdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "274223b0-8b0b-4186-9eb3-1bc53aed87fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a1d945d-bdfd-43ae-9c18-6ad597215f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "274223b0-8b0b-4186-9eb3-1bc53aed87fe",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        },
        {
            "id": "0875155d-64f5-41b9-9d36-acb22ac6eb61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "9816d1e4-ccce-4a35-9189-255114d33860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0875155d-64f5-41b9-9d36-acb22ac6eb61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b9173aa-724f-4301-b956-94e9825b8ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0875155d-64f5-41b9-9d36-acb22ac6eb61",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        },
        {
            "id": "5989ab0f-0d0e-4b8e-9a3c-3de71714b9ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "17636576-2356-4d17-93cb-8ca95bd82883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5989ab0f-0d0e-4b8e-9a3c-3de71714b9ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580c47e0-c3bf-4e1e-9f06-c5c08e477eca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5989ab0f-0d0e-4b8e-9a3c-3de71714b9ec",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        },
        {
            "id": "3776200a-2eec-4618-9455-c73d09775fbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "75b86392-3a85-460d-9bef-5540ebfd4b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3776200a-2eec-4618-9455-c73d09775fbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3965b33-eea2-4041-aca1-00612dd23ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3776200a-2eec-4618-9455-c73d09775fbb",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        },
        {
            "id": "84a1af56-631b-4113-ba63-7fb3a789eee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "df26cb86-81a8-4c0f-a305-5e6d552b873f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a1af56-631b-4113-ba63-7fb3a789eee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c149fbed-d8f4-49ad-b36b-fdfa3f1d77b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a1af56-631b-4113-ba63-7fb3a789eee6",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        },
        {
            "id": "2fe2fac0-810f-4319-b716-63e7f7b3af3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "compositeImage": {
                "id": "9e15e6a2-d48e-4730-bcea-e68ebb8396cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fe2fac0-810f-4319-b716-63e7f7b3af3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60caf08a-5e98-4703-b72f-70fe5563bda4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fe2fac0-810f-4319-b716-63e7f7b3af3c",
                    "LayerId": "2f4c9e3b-afc6-4dbb-8458-5fb467143771"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "2f4c9e3b-afc6-4dbb-8458-5fb467143771",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f7e3768-646a-41cc-b55e-cc77bb055d59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 6,
    "yorig": 9
}