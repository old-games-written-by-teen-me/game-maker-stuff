{
    "id": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65f5020f-15e4-48b1-a048-35a321d0b2fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "9215b7b6-cf4b-48ab-9720-f2f5b73e57ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f5020f-15e4-48b1-a048-35a321d0b2fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d95fdf50-4d9d-41d1-a350-dd8e0000c903",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f5020f-15e4-48b1-a048-35a321d0b2fd",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        },
        {
            "id": "919bd461-f489-45bb-b2b9-1636878115ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "c89826bf-9265-416b-8dd8-776bd8c4d780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "919bd461-f489-45bb-b2b9-1636878115ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18a3360d-3283-40f7-9aa3-367ff8c6e6a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "919bd461-f489-45bb-b2b9-1636878115ea",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        },
        {
            "id": "02ca858c-0e85-46b5-938d-2b2d4583db15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "63fc3187-92e7-4943-a058-8769c893a084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02ca858c-0e85-46b5-938d-2b2d4583db15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6051df39-2ee5-4712-b5e4-b6e9af4408ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02ca858c-0e85-46b5-938d-2b2d4583db15",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        },
        {
            "id": "8d3d205f-1108-4e4b-b904-82f6cddd80e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "bad6e17c-f33a-44de-80af-41616fe45770",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3d205f-1108-4e4b-b904-82f6cddd80e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5cdc7b9-4264-490f-8520-6b9620b23899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3d205f-1108-4e4b-b904-82f6cddd80e1",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        },
        {
            "id": "bf81be64-7af2-4bf2-8f0e-32c60605ec3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "ffac6c2a-b8d8-4c9a-923d-7fd750a2e305",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf81be64-7af2-4bf2-8f0e-32c60605ec3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dcf7fc2-c83d-4b0d-b262-21163dce77e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf81be64-7af2-4bf2-8f0e-32c60605ec3f",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        },
        {
            "id": "6732c86a-a283-455b-9614-62d5b287ce9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "compositeImage": {
                "id": "46ba571b-4d77-4706-bef8-4119a738adb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6732c86a-a283-455b-9614-62d5b287ce9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "018f4bea-5f22-4676-96bd-5bdc2f773407",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6732c86a-a283-455b-9614-62d5b287ce9d",
                    "LayerId": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "8b2c65ce-85ee-4bc0-a71e-bd2e0684921a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e76ec3d9-d553-4d3a-8fa8-8e2363f2c737",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 16
}