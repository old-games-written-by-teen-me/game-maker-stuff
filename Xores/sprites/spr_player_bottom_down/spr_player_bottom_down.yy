{
    "id": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bottom_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 3,
    "bbox_right": 7,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4f95032-6c03-44ba-ac82-c4267fb83f93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "bca4ebbc-a6a0-4c32-9407-78427c535aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4f95032-6c03-44ba-ac82-c4267fb83f93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43ad9b68-5115-4dd4-af18-6648b45cf102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4f95032-6c03-44ba-ac82-c4267fb83f93",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "f4416bc0-9482-4b2d-b307-020f2c929cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "e8123fd8-999f-46fe-a8d7-17eb0fc68c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4416bc0-9482-4b2d-b307-020f2c929cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f376d58-b3b6-45f8-9697-a174d340a182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4416bc0-9482-4b2d-b307-020f2c929cd1",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "3ec5c288-c64b-485f-b36a-2dcd5a694e15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "0793950a-171c-48e6-b4c6-ca649bab8f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ec5c288-c64b-485f-b36a-2dcd5a694e15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ec93c2-9bbc-4a68-8f00-72c1255051a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ec5c288-c64b-485f-b36a-2dcd5a694e15",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "062ec9e5-93a4-4beb-9476-6f2cd92cc994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "a8346604-e6b0-44d0-ae0f-5f7a93e82d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062ec9e5-93a4-4beb-9476-6f2cd92cc994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "940726ab-8d7e-46b9-93cb-98123040482a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062ec9e5-93a4-4beb-9476-6f2cd92cc994",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "00f835f7-7291-4dcc-81e8-2cb618cf5c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "0ba4b3b3-5bbe-4da6-9a1e-b3b028da7012",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00f835f7-7291-4dcc-81e8-2cb618cf5c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d113370-5916-444f-9b2a-b8d6b2983695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00f835f7-7291-4dcc-81e8-2cb618cf5c96",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "e1abe195-6f66-4126-ba20-c91a6810f11c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "2daf800b-b603-4348-8647-ad947de2af98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1abe195-6f66-4126-ba20-c91a6810f11c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ceb706f3-2d90-4814-8c1c-2c1a284021ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1abe195-6f66-4126-ba20-c91a6810f11c",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "6050680e-4721-47b7-8660-9b642c4f1625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "93daf87b-26ff-489e-9c14-86b75a1eaadb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6050680e-4721-47b7-8660-9b642c4f1625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe79a11-db54-407b-89b6-987c37c06352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6050680e-4721-47b7-8660-9b642c4f1625",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "ed18e159-a78d-4db7-8437-3928ea778351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "970ef9b9-11c0-417f-b65c-4732c77cc271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed18e159-a78d-4db7-8437-3928ea778351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72944f42-e186-45dc-b258-0ae44d8e8329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed18e159-a78d-4db7-8437-3928ea778351",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "17f673d1-394c-4fc4-841d-d162efd21dec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "38809edc-2aa1-4dc7-9ac2-0a8e24659ed0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f673d1-394c-4fc4-841d-d162efd21dec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63565f53-ce8b-4ad4-b1fe-aedcd600bd82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f673d1-394c-4fc4-841d-d162efd21dec",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "4640085d-4314-481e-a562-dab36b151c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "6c421c0f-477f-403e-bb7d-b9ed197204b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4640085d-4314-481e-a562-dab36b151c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bd07dd6-0dec-4e4e-a7d3-4047f4c40f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4640085d-4314-481e-a562-dab36b151c9b",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "bc5fc0d2-1b5a-41ea-9c07-7df31e5b9019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "daf62b43-4800-46e2-8b1c-833c67de8616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5fc0d2-1b5a-41ea-9c07-7df31e5b9019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e28253-6f5d-403a-a123-ae143342f729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5fc0d2-1b5a-41ea-9c07-7df31e5b9019",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "3981ec5f-07cb-4e3c-af78-1926248cdc0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "b0cfb51a-a4f3-4d13-812e-c5f14d9f2879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3981ec5f-07cb-4e3c-af78-1926248cdc0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16bb6ad3-ab65-4462-bfeb-5f049654ad1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3981ec5f-07cb-4e3c-af78-1926248cdc0c",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        },
        {
            "id": "472a0373-12c2-4eb0-88db-e01bef9e7677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "compositeImage": {
                "id": "5c72f110-0335-489e-8a8e-f39b3d9f99dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472a0373-12c2-4eb0-88db-e01bef9e7677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4643d8d-e640-4010-850d-caf33eec9697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472a0373-12c2-4eb0-88db-e01bef9e7677",
                    "LayerId": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "ab6122dd-aafa-4fc2-88a7-9e3ba91231f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "223e4b06-d52b-4ac8-988c-cf45f54feb3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}