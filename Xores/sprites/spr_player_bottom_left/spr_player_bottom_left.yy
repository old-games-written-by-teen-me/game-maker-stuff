{
    "id": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bottom_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2014709-b9c8-4e80-a984-07591eb7867c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "9a8df612-1055-40dc-80dd-c96b7a8f49d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2014709-b9c8-4e80-a984-07591eb7867c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b620b713-941a-44a9-9978-b03e30c72f58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2014709-b9c8-4e80-a984-07591eb7867c",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "13c3ae77-7a14-479a-8eb5-93bad74d895c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "84c3f94b-9d76-4021-b997-bedf351fa4a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c3ae77-7a14-479a-8eb5-93bad74d895c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9708824f-c66d-478a-a897-f9cbfc708f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c3ae77-7a14-479a-8eb5-93bad74d895c",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "def7775d-acb5-47e1-bdec-89c7583652d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "f900b616-c7e7-4523-a872-c09c79b360aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def7775d-acb5-47e1-bdec-89c7583652d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a25786-0ed4-48e6-b7a7-3e356fb32814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def7775d-acb5-47e1-bdec-89c7583652d6",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "8194ba4e-03f2-41f0-aa96-9b33dd6d9ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "25dd9e7b-bdd9-4641-a766-c710b9f80c49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8194ba4e-03f2-41f0-aa96-9b33dd6d9ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3479490b-c80c-4e2f-8b87-9d5ad6ceb6db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8194ba4e-03f2-41f0-aa96-9b33dd6d9ad1",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "c45c31fe-b530-4678-9954-8cd48f76541a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "19a08b9b-7203-422c-b098-d12ce92d0de3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c45c31fe-b530-4678-9954-8cd48f76541a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39b39419-88be-4796-9dcf-3bca245c19ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c45c31fe-b530-4678-9954-8cd48f76541a",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "a7e069d8-d047-4c55-80c0-069ba24dd7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "06f95e2a-c599-4a5e-87b1-f8e6584c31c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7e069d8-d047-4c55-80c0-069ba24dd7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218d2ad0-997b-4087-8532-6b2307159485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7e069d8-d047-4c55-80c0-069ba24dd7a7",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "aed8192c-64ff-4785-808b-4a4bcabf2ccd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "e2fe8a52-ade9-40f1-983b-7f165bf242a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aed8192c-64ff-4785-808b-4a4bcabf2ccd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81cdab6d-a643-4b89-98ca-fb1059d7aa24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aed8192c-64ff-4785-808b-4a4bcabf2ccd",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "9adac651-dd96-4690-8594-2e6fa168dec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "4f03f878-60cc-4189-8a53-b74a62488119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9adac651-dd96-4690-8594-2e6fa168dec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9469454a-e70b-450f-8b52-39c569e97ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9adac651-dd96-4690-8594-2e6fa168dec6",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "dde908ca-3a41-44c9-a6ca-1c5e88330a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "53932084-cc60-4433-a26f-3275ba6eac04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dde908ca-3a41-44c9-a6ca-1c5e88330a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b072c728-3b41-4daa-8853-6ce7edd1aa43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dde908ca-3a41-44c9-a6ca-1c5e88330a32",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "5b7b781f-ae7f-4941-971e-45e9f2302ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "34d89558-adae-46fb-9315-2804d58c10c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7b781f-ae7f-4941-971e-45e9f2302ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d4c52c9-8c7b-49b9-b05c-00194859fe63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7b781f-ae7f-4941-971e-45e9f2302ff7",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "0dcde025-4ab1-4096-b450-ba07f80ea6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "e1e509fa-6e28-4990-993e-b4760c063002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dcde025-4ab1-4096-b450-ba07f80ea6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e19b2bac-e12d-4901-81db-255d85ca1a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dcde025-4ab1-4096-b450-ba07f80ea6b2",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        },
        {
            "id": "c1b0fabd-f8dd-4036-98ac-224906883cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "compositeImage": {
                "id": "fd6cc436-dca9-4e27-a94c-ff595ecf5a76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1b0fabd-f8dd-4036-98ac-224906883cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54f991d5-998d-465d-86c9-d2872bcd055d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1b0fabd-f8dd-4036-98ac-224906883cc1",
                    "LayerId": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "adb5a689-fa40-47ce-a2fd-4ce9b2ce3538",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae3859f4-6623-4c97-92ff-eb58b1d20eb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}