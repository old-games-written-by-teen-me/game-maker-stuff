{
    "id": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bottom_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffab3d7e-d1c4-4f5c-a838-61ab0585a6be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "7cd41879-c48f-4483-9238-0251dfa2b116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffab3d7e-d1c4-4f5c-a838-61ab0585a6be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a7ef9e5-51c3-4402-a0ed-4e342d80bdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffab3d7e-d1c4-4f5c-a838-61ab0585a6be",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "f0938a7c-9e47-4ce5-8704-2d5d7eeed09e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "861fb3e6-d060-459b-9c17-e25b25f7c786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0938a7c-9e47-4ce5-8704-2d5d7eeed09e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8add990c-c3a6-49fb-9c32-1774531a85a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0938a7c-9e47-4ce5-8704-2d5d7eeed09e",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "a9f60cb7-cd05-4b64-a601-c10172589728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "68186d87-0e59-40b5-a54e-754be0083798",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9f60cb7-cd05-4b64-a601-c10172589728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f2d151-6703-44a6-a60f-09dcc5cb014d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9f60cb7-cd05-4b64-a601-c10172589728",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "7cde6c8b-dde5-4e46-85ae-6856622d31b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "8bee30e7-008f-40a6-b068-9b17893d5e56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cde6c8b-dde5-4e46-85ae-6856622d31b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eea3d63-2084-4750-86f4-17302700c8b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cde6c8b-dde5-4e46-85ae-6856622d31b9",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "d2d7ce72-1afe-47ea-af32-b0a3b1f13d6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "f6629911-e362-4660-bfbc-45aacebca0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2d7ce72-1afe-47ea-af32-b0a3b1f13d6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e949696a-de18-400a-ac86-98ebcd339c67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2d7ce72-1afe-47ea-af32-b0a3b1f13d6f",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "f860e67d-2c53-4201-8010-0ce32038f1e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "80a547e5-332a-4de9-8d12-206c3a9fa8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f860e67d-2c53-4201-8010-0ce32038f1e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10ad8959-9837-45f2-8482-cac7eadf0cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f860e67d-2c53-4201-8010-0ce32038f1e3",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "cded4cfa-a1e3-4865-af5d-af431c4bd48e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "e146ed65-1d59-4d59-8d39-d7658112aae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cded4cfa-a1e3-4865-af5d-af431c4bd48e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88e53407-b193-4c61-b25c-448a3cbe8a8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cded4cfa-a1e3-4865-af5d-af431c4bd48e",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "f0879f96-23aa-4513-8f87-6eed4f679cd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "8acf30aa-a5cc-435b-8691-9d38cd5c029d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0879f96-23aa-4513-8f87-6eed4f679cd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d309f2d-5351-463a-842e-6744e044c6cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0879f96-23aa-4513-8f87-6eed4f679cd6",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "4fcc7837-f1d5-4ec1-880c-e9b58476ad9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "ccf1d0a7-1788-48b4-ae4a-7e7ee0a56c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fcc7837-f1d5-4ec1-880c-e9b58476ad9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a08c9f3-f1e6-44ef-af20-3a3e2f344a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fcc7837-f1d5-4ec1-880c-e9b58476ad9a",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "925af5c9-3271-49a4-9a05-dd658209f3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "0c5f2be6-fedd-491a-b4e1-9c6809ca1cdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925af5c9-3271-49a4-9a05-dd658209f3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465596a9-d835-4381-bf1c-8da4e16c28c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925af5c9-3271-49a4-9a05-dd658209f3c1",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "dba6e080-801d-43fd-8e6a-953f2eed2745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "449acfb4-4910-4357-a3d8-11d8bf274ee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dba6e080-801d-43fd-8e6a-953f2eed2745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c88e828-13af-499c-8e62-9ed0141d07a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dba6e080-801d-43fd-8e6a-953f2eed2745",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        },
        {
            "id": "0829c865-7a00-4449-8a84-6950585e6dba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "compositeImage": {
                "id": "9765de02-395c-4817-820c-98d990d2fa32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0829c865-7a00-4449-8a84-6950585e6dba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20135a97-9466-4750-8f7f-1e985271ccdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0829c865-7a00-4449-8a84-6950585e6dba",
                    "LayerId": "eb58bf93-ad48-4fe1-92bf-2e84a841b067"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "eb58bf93-ad48-4fe1-92bf-2e84a841b067",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f38c717-eb33-4bb9-bff8-ecbe31db54e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}