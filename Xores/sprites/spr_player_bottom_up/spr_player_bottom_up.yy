{
    "id": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_bottom_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 3,
    "bbox_right": 7,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f963b63-84a6-4a1c-b037-0a818350e1aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "94fc48ce-af75-4d6e-89b3-e760b1401b79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f963b63-84a6-4a1c-b037-0a818350e1aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41afe7b7-a4d5-4122-b58b-dad48e859f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f963b63-84a6-4a1c-b037-0a818350e1aa",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "8f83dffa-b954-441f-a5cb-914ce3ee3b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "0f0f3128-59d2-4dae-8275-50698cbfe1bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f83dffa-b954-441f-a5cb-914ce3ee3b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "208ebb0f-7b86-4c03-8cb5-a7bd2bc77ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f83dffa-b954-441f-a5cb-914ce3ee3b58",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "1b8598d8-1b61-4eda-8386-58bf97662181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "a613672a-1f5c-4095-b71e-107f62b214e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8598d8-1b61-4eda-8386-58bf97662181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f11febe-9556-4af0-a150-9099de863eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8598d8-1b61-4eda-8386-58bf97662181",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "beefbf37-5565-4d22-8315-84f24f228967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "4fe219c6-e3bb-498a-9d8d-6b17b4be0a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beefbf37-5565-4d22-8315-84f24f228967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafeaddd-e045-40d4-8645-e34e928584c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beefbf37-5565-4d22-8315-84f24f228967",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "6f3f40ed-0302-4896-ac50-7993aa1a380f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "2a154406-5b5f-408b-85e3-5364c6549c1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3f40ed-0302-4896-ac50-7993aa1a380f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baa97d4c-f068-4478-9a48-a4e073b8c5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3f40ed-0302-4896-ac50-7993aa1a380f",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "795a1f9c-c4af-47d1-acb3-a0fbf72ad518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "1eff5156-1b60-4fe1-9f32-1117fd59b3a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "795a1f9c-c4af-47d1-acb3-a0fbf72ad518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb4b0fe8-b471-4b26-87fd-2db7385f7a77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "795a1f9c-c4af-47d1-acb3-a0fbf72ad518",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "aaf6ce14-58d8-49db-b098-549886b2a0a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "2f9c9206-e025-4074-84cf-22055b13af85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf6ce14-58d8-49db-b098-549886b2a0a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57bf8e63-ce2c-4308-b8fd-21654c9203b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf6ce14-58d8-49db-b098-549886b2a0a2",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "3bfc3e8b-fedd-4bef-b2d3-5647c5ee97bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "d8432b2e-f071-4865-b820-09b2fdba6a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bfc3e8b-fedd-4bef-b2d3-5647c5ee97bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e76da0-44a4-47cd-aa49-c02f2f0ea8cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bfc3e8b-fedd-4bef-b2d3-5647c5ee97bf",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "f54f99a3-3227-4120-9db9-3dc6a24b3ecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "98452997-d93f-4135-8656-db6326e42b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f54f99a3-3227-4120-9db9-3dc6a24b3ecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04f03401-452a-454e-a548-fb213b472bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f54f99a3-3227-4120-9db9-3dc6a24b3ecc",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "74fd9be2-054c-4d35-bb90-f7bd7891de3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "9ddeb49f-370e-4c5c-ba3d-4c0a94b285bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74fd9be2-054c-4d35-bb90-f7bd7891de3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5c51eb-82bc-4753-84c0-20b3d9b51532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74fd9be2-054c-4d35-bb90-f7bd7891de3c",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "bbadb143-9727-41c9-b3e3-e88a102df3ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "94d5aad3-7e9b-4ce0-9644-aa699a1c62fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbadb143-9727-41c9-b3e3-e88a102df3ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef9607b-62ec-4b69-8d27-d9db8fc59203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbadb143-9727-41c9-b3e3-e88a102df3ad",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        },
        {
            "id": "5b7e6bad-5069-4c1a-90ac-e2ca6b4fd41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "compositeImage": {
                "id": "7001a0e4-c03a-4136-a4ae-80ef8cdf2013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b7e6bad-5069-4c1a-90ac-e2ca6b4fd41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "675ee1aa-4d67-4ce3-9a12-70bf0800d631",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b7e6bad-5069-4c1a-90ac-e2ca6b4fd41e",
                    "LayerId": "c42204a0-89d2-4f53-82c4-98ba7c8898a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "c42204a0-89d2-4f53-82c4-98ba7c8898a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be46ec60-a686-4c41-8f21-365bccdc8cfa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}