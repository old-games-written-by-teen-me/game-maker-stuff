{
    "id": "735edb46-2f80-4f30-a6a2-c7281803fd5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_detector_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 4,
    "bbox_right": 5,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4767da10-2976-4455-8aa0-c597c5cfcd45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "735edb46-2f80-4f30-a6a2-c7281803fd5e",
            "compositeImage": {
                "id": "0d65393b-81b6-4769-bb83-f904a0a24cb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4767da10-2976-4455-8aa0-c597c5cfcd45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9fcc43e-1a77-48dd-bd6b-d3c33cc37495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4767da10-2976-4455-8aa0-c597c5cfcd45",
                    "LayerId": "2ae19121-99f9-4fba-b76c-717fbdeefec7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "2ae19121-99f9-4fba-b76c-717fbdeefec7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "735edb46-2f80-4f30-a6a2-c7281803fd5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 9
}