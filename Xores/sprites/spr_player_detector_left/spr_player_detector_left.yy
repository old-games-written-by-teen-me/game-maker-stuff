{
    "id": "b6f2dfad-7037-431d-8aa8-68f53258bfe9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_detector_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2892d470-27c7-430a-a165-36c2f8a077f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6f2dfad-7037-431d-8aa8-68f53258bfe9",
            "compositeImage": {
                "id": "c97bd8aa-d5a0-4748-90ec-f17ee3af5704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2892d470-27c7-430a-a165-36c2f8a077f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56f88aab-594c-457e-b94e-6e3efda00317",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2892d470-27c7-430a-a165-36c2f8a077f8",
                    "LayerId": "e2d98a0a-f884-4a67-bc61-39f4b90a0187"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "e2d98a0a-f884-4a67-bc61-39f4b90a0187",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6f2dfad-7037-431d-8aa8-68f53258bfe9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 9
}