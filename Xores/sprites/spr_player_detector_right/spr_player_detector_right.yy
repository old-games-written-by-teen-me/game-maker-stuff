{
    "id": "c30a0939-adc2-4203-8aa0-b3db6bbf087b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_detector_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 9,
    "bbox_right": 9,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09920933-e7f5-4b1b-aa32-451d03b39fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c30a0939-adc2-4203-8aa0-b3db6bbf087b",
            "compositeImage": {
                "id": "36e80705-1f9c-41d5-aa60-01eac22d7060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09920933-e7f5-4b1b-aa32-451d03b39fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fb64bae-05b5-43ba-9cbc-27ba665cc291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09920933-e7f5-4b1b-aa32-451d03b39fdb",
                    "LayerId": "efcd34ab-d8c2-419e-993e-70ce95b62bb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "efcd34ab-d8c2-419e-993e-70ce95b62bb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c30a0939-adc2-4203-8aa0-b3db6bbf087b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 9
}