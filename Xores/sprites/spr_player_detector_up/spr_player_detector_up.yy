{
    "id": "3939fd5b-30ad-4e70-80f1-eaedd0dca5a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_detector_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 4,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9057e505-938d-40e5-a778-99a7583a10f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3939fd5b-30ad-4e70-80f1-eaedd0dca5a8",
            "compositeImage": {
                "id": "66204c19-6402-4dd7-9d7b-664ce51c66d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9057e505-938d-40e5-a778-99a7583a10f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "170a0032-ce96-4a8c-ae21-36c7c2fa7172",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9057e505-938d-40e5-a778-99a7583a10f0",
                    "LayerId": "36bedb1c-385d-4548-a393-836920f0ad7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "36bedb1c-385d-4548-a393-836920f0ad7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3939fd5b-30ad-4e70-80f1-eaedd0dca5a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 9
}