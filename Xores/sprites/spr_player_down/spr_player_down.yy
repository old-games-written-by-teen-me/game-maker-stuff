{
    "id": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff7aadd5-a861-4c77-948e-8082261e8e69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "cf564700-202a-4111-8f91-ea9083cc82a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff7aadd5-a861-4c77-948e-8082261e8e69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5585c189-4a84-47ce-bd7f-b28a92e337e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff7aadd5-a861-4c77-948e-8082261e8e69",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "21cfefdc-e66a-40a1-842c-20ae89b63ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "621eb742-0b86-479c-8b2b-33cf601676ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cfefdc-e66a-40a1-842c-20ae89b63ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc4a186-a2e0-49a4-8d70-d0b7bf5413a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cfefdc-e66a-40a1-842c-20ae89b63ffb",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "ed54bf38-88a2-492b-833a-1c2de7d33574",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "fdc25b43-4b60-4af3-8918-491c5e59d521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed54bf38-88a2-492b-833a-1c2de7d33574",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257ed297-d2a0-4f88-9db7-181b00a52705",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed54bf38-88a2-492b-833a-1c2de7d33574",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "b4b73b05-5639-4fa0-97de-5bc56c6b7949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "ca88790f-0456-4bdf-a3f0-9809d0b1c40f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b73b05-5639-4fa0-97de-5bc56c6b7949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef751a4-2dcf-4178-a692-ffcc222daa14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b73b05-5639-4fa0-97de-5bc56c6b7949",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "d10832aa-c3df-41c8-bb64-47ff74fbf0c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "43091a4e-87f3-43ff-887e-96390dc0dae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d10832aa-c3df-41c8-bb64-47ff74fbf0c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa1b3b20-362e-4d4e-af38-c5e1e61a162e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d10832aa-c3df-41c8-bb64-47ff74fbf0c7",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "e17af951-811e-4780-a572-a07f31cf5b71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "5acefaeb-065e-4bd1-8023-6a25b9b7f6fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e17af951-811e-4780-a572-a07f31cf5b71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632e1030-1181-47e0-9909-3275f0509205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e17af951-811e-4780-a572-a07f31cf5b71",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "b42df6ec-f758-4634-842e-696f0d590d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "d0e6f17b-2d3f-4706-8b60-dd3642f53e5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b42df6ec-f758-4634-842e-696f0d590d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1351a744-ae05-428f-a18f-15a47f967479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b42df6ec-f758-4634-842e-696f0d590d87",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "7885fc41-5e92-4aca-b20b-3981b5adcebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "d6044502-710d-49e3-862a-cd6ba8bfeec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7885fc41-5e92-4aca-b20b-3981b5adcebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "726470e6-0a12-4019-9b12-124593b8dace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7885fc41-5e92-4aca-b20b-3981b5adcebb",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "31a866d1-c1c9-42fb-90cf-37cea7c241ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "68c559b8-89fe-466a-9c9a-9bd874a15063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31a866d1-c1c9-42fb-90cf-37cea7c241ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "288940ff-0a1d-4fdc-b55b-d6e14d68edad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31a866d1-c1c9-42fb-90cf-37cea7c241ba",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "721313fc-95c1-4f99-9fca-8bfbb214d6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "7024c6f2-e645-4d8b-a520-d3c507418f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "721313fc-95c1-4f99-9fca-8bfbb214d6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f55143-f6b4-48a0-8822-b5f40157c808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "721313fc-95c1-4f99-9fca-8bfbb214d6b5",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "7ef67e22-9c91-4fce-9213-122eae80fd9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "01279303-55bf-4314-a190-d4b884ad3f12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef67e22-9c91-4fce-9213-122eae80fd9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcec3aa8-fd27-48fe-867e-780aac77771b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef67e22-9c91-4fce-9213-122eae80fd9c",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        },
        {
            "id": "bb0ee953-7bac-491a-8477-a6f29b6083cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "compositeImage": {
                "id": "1e7a1081-885a-4e5d-aac2-93166fe8c385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb0ee953-7bac-491a-8477-a6f29b6083cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a6dec5c-0f1c-4562-9699-46eab911d6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb0ee953-7bac-491a-8477-a6f29b6083cd",
                    "LayerId": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "eca2bce9-bee1-4e69-88e5-86ae3b939f5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cc2a8b8-821d-49dc-8c4b-a46b560a342d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}