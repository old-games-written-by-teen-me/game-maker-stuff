{
    "id": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6fc1bd5-6d74-4658-88c5-9facc08e1e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "9a3558dc-886c-497e-a80b-e8ce7d66a259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6fc1bd5-6d74-4658-88c5-9facc08e1e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dea808e-84f4-4c80-bea1-12e8af9a7f5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6fc1bd5-6d74-4658-88c5-9facc08e1e44",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "f6010f26-7df3-4083-be80-b6ff75764c0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "1d3a2912-1ab6-4fa2-a4d2-5d94fa9716ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6010f26-7df3-4083-be80-b6ff75764c0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32ba7133-5adb-4d37-848b-38938aec0a27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6010f26-7df3-4083-be80-b6ff75764c0d",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "f93630eb-6096-46c9-8f7d-6ed516940cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "f782fceb-6325-4ce2-8a8e-1d79992f61a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f93630eb-6096-46c9-8f7d-6ed516940cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a789160d-a6c4-4394-9b11-940b27db9777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f93630eb-6096-46c9-8f7d-6ed516940cc4",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "9195bd90-9f38-4473-a11e-d03a0d6b0308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "d52a5621-9b0e-4443-9dc6-b324ba603127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9195bd90-9f38-4473-a11e-d03a0d6b0308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d6f9cc-4ff1-4585-aa0f-2853ce88a436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9195bd90-9f38-4473-a11e-d03a0d6b0308",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "4ba0fe3f-ae60-41a3-80a0-131237202b26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "d91fbf55-e54d-44cb-90d1-d8f99fc13589",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ba0fe3f-ae60-41a3-80a0-131237202b26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ddb976-b729-4c34-b131-38525cbb5d23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ba0fe3f-ae60-41a3-80a0-131237202b26",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "9ab261b4-091b-404a-99f3-76f39bf98241",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "72eebc10-4013-46e6-9b44-3c674743121b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab261b4-091b-404a-99f3-76f39bf98241",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2112af89-fb61-4f75-becb-2e60f4d05271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab261b4-091b-404a-99f3-76f39bf98241",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "494372c5-cb54-4b50-a89c-075beb3ad1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "85a0f6b1-c022-49b2-b4d7-60dc4de61b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "494372c5-cb54-4b50-a89c-075beb3ad1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552c1383-4d2e-4fed-9df0-bcfb96439a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "494372c5-cb54-4b50-a89c-075beb3ad1f6",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "4cf360e4-6b0c-420d-bf09-452a8c26a92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "1df3c511-dd9d-4a47-8268-a64fcc6029eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf360e4-6b0c-420d-bf09-452a8c26a92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "686bc7e9-c918-4147-906d-1b78e38d44f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf360e4-6b0c-420d-bf09-452a8c26a92f",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "5151e10a-bfef-43dc-8da3-91031e780f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "0a0b6148-0bfd-4c2f-a51b-e628095b07d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5151e10a-bfef-43dc-8da3-91031e780f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0311f4ef-db7c-4299-85d2-7d9080d1505f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5151e10a-bfef-43dc-8da3-91031e780f35",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "193612a3-4c1e-4338-950b-7119f16e3936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "62e2582d-9744-4f6c-a9f9-d37ef6a6f801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "193612a3-4c1e-4338-950b-7119f16e3936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b9c449-6dca-4b5b-8cd7-ff33ddf299c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "193612a3-4c1e-4338-950b-7119f16e3936",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "3af2a385-eb77-408d-aed7-a8924630fb5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "6b3a1c84-974e-46a3-a391-dd6f942c7c3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af2a385-eb77-408d-aed7-a8924630fb5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ac3b7f-3936-41c0-9274-b9f55d3420c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af2a385-eb77-408d-aed7-a8924630fb5d",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        },
        {
            "id": "e0726f7d-0938-415f-96d9-783470a79d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "compositeImage": {
                "id": "324b6779-2a71-433b-93f2-6475e34b1065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0726f7d-0938-415f-96d9-783470a79d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a25cf402-55a1-48cf-8a52-514f145b6c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0726f7d-0938-415f-96d9-783470a79d63",
                    "LayerId": "53437918-ab73-463c-b93e-a4bb718c7c4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "53437918-ab73-463c-b93e-a4bb718c7c4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97ef4fa3-c01a-4822-86f1-a1cd04eea619",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}