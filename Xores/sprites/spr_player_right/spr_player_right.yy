{
    "id": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd15ac82-d081-4b17-9a4e-86b93adaf16a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "95e17237-74bc-4d2f-890e-f504ac537cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd15ac82-d081-4b17-9a4e-86b93adaf16a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e664d3c-de03-401a-85cd-4a8aeb547b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd15ac82-d081-4b17-9a4e-86b93adaf16a",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "4c7ee459-9ebe-4f22-9206-ba3aa83ef036",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "eab7019e-5290-415f-8493-221de968fe8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c7ee459-9ebe-4f22-9206-ba3aa83ef036",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a2336f-a3d8-4cb2-8b96-5d8ea8beec66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c7ee459-9ebe-4f22-9206-ba3aa83ef036",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "235e1426-0b13-4456-be1c-c9226465ee5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "7bcb1829-bac5-4cd5-b546-a08bd0f55a0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "235e1426-0b13-4456-be1c-c9226465ee5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b245c2e0-a6de-4f07-a6ae-71f1b5d4713f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "235e1426-0b13-4456-be1c-c9226465ee5d",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "2cf96d2a-20c7-4ffc-a2b8-1a9ffa10a74b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "fa2a48bf-873e-44ec-89e7-c6d37f7c1c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cf96d2a-20c7-4ffc-a2b8-1a9ffa10a74b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bd9a4a-2674-4cef-8d6f-fda0bb8079d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cf96d2a-20c7-4ffc-a2b8-1a9ffa10a74b",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "8870ea08-81e0-4d1d-8385-6ecde841ad3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "7ef08e01-bf75-40c4-8cd5-762c0c80b734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8870ea08-81e0-4d1d-8385-6ecde841ad3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c61366f-d74e-4449-ab0a-9d253c143629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8870ea08-81e0-4d1d-8385-6ecde841ad3e",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "9a781dfa-e113-4f74-99ff-dfb6f9c05398",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "7f0cec26-6b2c-44e6-bfa7-d8cff680efd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a781dfa-e113-4f74-99ff-dfb6f9c05398",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "700bb551-a5eb-42e2-b5b5-9e68e497e168",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a781dfa-e113-4f74-99ff-dfb6f9c05398",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "8806e1fe-5fe5-46b9-80a3-2e38a44fd03e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "8e927552-b935-475c-963a-6c3edd2ca849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8806e1fe-5fe5-46b9-80a3-2e38a44fd03e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bd8cca-32bc-4391-89b8-187431468e95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8806e1fe-5fe5-46b9-80a3-2e38a44fd03e",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "34f0e74a-fd82-4427-a84a-5dcd1fd30f0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "282cbca1-13b1-4d9a-952d-39cd687257bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f0e74a-fd82-4427-a84a-5dcd1fd30f0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f798dd-ff73-44ee-851d-ecbb2f079982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f0e74a-fd82-4427-a84a-5dcd1fd30f0f",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "c84f8bd3-9c26-485e-8f2d-1faa9e976b02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "74deff96-f05a-49e9-995a-e583e146efa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c84f8bd3-9c26-485e-8f2d-1faa9e976b02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a65a23-bc24-4b71-aa81-4c9fdd7174ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c84f8bd3-9c26-485e-8f2d-1faa9e976b02",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "223c73b3-970d-4b96-b5fb-9bc70cd09489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "27f053da-c9f3-46dd-982b-358012db29c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "223c73b3-970d-4b96-b5fb-9bc70cd09489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c16e390-bbf8-47fc-b94b-f74936442cee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "223c73b3-970d-4b96-b5fb-9bc70cd09489",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "5d0d5134-6ec7-4369-9130-75b0cda32897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "696e998d-956a-4ff3-bce6-71bc86e0c0a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d0d5134-6ec7-4369-9130-75b0cda32897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad756f5-dd5e-444b-9bcd-422af4c79af8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d0d5134-6ec7-4369-9130-75b0cda32897",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        },
        {
            "id": "e5d7a680-4b65-438b-8f98-dfc75c0f3dfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "compositeImage": {
                "id": "73e7298d-ec8a-44c4-8e3d-c47f18c6ef13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d7a680-4b65-438b-8f98-dfc75c0f3dfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0443570d-be64-4bf1-92b8-086afdfcfb11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d7a680-4b65-438b-8f98-dfc75c0f3dfb",
                    "LayerId": "69191d8c-1ff1-47fe-ab00-e8a880713b96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "69191d8c-1ff1-47fe-ab00-e8a880713b96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33f686ef-ace3-430a-b588-fd6c05bcbd80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}