{
    "id": "86c68a76-31c0-4e53-81d8-5207db9f2338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 8,
    "bbox_right": 16,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3543e98f-09a0-462d-b5bb-46536069163b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c68a76-31c0-4e53-81d8-5207db9f2338",
            "compositeImage": {
                "id": "6071f5ca-589d-4316-b74c-b975eef1eff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3543e98f-09a0-462d-b5bb-46536069163b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed3824e-115a-4b58-b67d-fd0d88c45510",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3543e98f-09a0-462d-b5bb-46536069163b",
                    "LayerId": "3173f169-1b60-4bea-a696-2160c442ec4d"
                }
            ]
        },
        {
            "id": "5fdc9d29-8ddc-4ece-8185-b5c76586aa9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86c68a76-31c0-4e53-81d8-5207db9f2338",
            "compositeImage": {
                "id": "f6fe47a2-af85-4772-bc0e-fb853a25a64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fdc9d29-8ddc-4ece-8185-b5c76586aa9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e2f9a2-5b30-4f00-bcd2-dfef5542c405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fdc9d29-8ddc-4ece-8185-b5c76586aa9d",
                    "LayerId": "3173f169-1b60-4bea-a696-2160c442ec4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "3173f169-1b60-4bea-a696-2160c442ec4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86c68a76-31c0-4e53-81d8-5207db9f2338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}