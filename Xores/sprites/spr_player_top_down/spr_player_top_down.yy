{
    "id": "087cc46f-db68-4f41-8aa3-c535eab0378c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "680bacaf-6069-4f02-8aa7-f62be43ffe34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "d6af019a-8d77-4158-bf12-3363ae909d6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680bacaf-6069-4f02-8aa7-f62be43ffe34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07ac04c0-5303-4d97-819a-235d9624ea33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680bacaf-6069-4f02-8aa7-f62be43ffe34",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "f06ab501-25b0-4b37-a064-399fe70d8ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "212609d2-9bfb-4203-9e01-f396a4bbbe05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06ab501-25b0-4b37-a064-399fe70d8ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b52d3af-5f80-4b21-83b3-b5c154e3dfef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06ab501-25b0-4b37-a064-399fe70d8ea7",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "5e467f84-24b9-4af7-a9a3-04a8f0c22ea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "ef7f08ec-2b65-4a5c-af60-cfa543ba6090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e467f84-24b9-4af7-a9a3-04a8f0c22ea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62fdbe55-a788-4b48-b176-862ca5206d3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e467f84-24b9-4af7-a9a3-04a8f0c22ea3",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "97a4fd46-fbea-43b4-92bf-333ecf700a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "cbbe4f9d-ec5a-4a96-a158-51d7f5ba7a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a4fd46-fbea-43b4-92bf-333ecf700a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da50f7d-5bfa-4ebe-8b62-b2c011381aec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a4fd46-fbea-43b4-92bf-333ecf700a93",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "07c65e01-9d5e-4f70-8a4f-91b6254b2b91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "46742125-04e2-450d-8660-c8bc9b30be03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c65e01-9d5e-4f70-8a4f-91b6254b2b91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb0dfea0-d55a-41eb-bc64-6389ea50d10f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c65e01-9d5e-4f70-8a4f-91b6254b2b91",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "96313a4f-c9f4-4533-b1cd-369d79bcae02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "58fafd05-9b25-484e-8390-9b10a16647d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96313a4f-c9f4-4533-b1cd-369d79bcae02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af14c969-a3e9-4002-a2ec-da6f5045c30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96313a4f-c9f4-4533-b1cd-369d79bcae02",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "07da75e7-c305-4974-aa4c-6be9a30af535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "8916e354-0ede-475f-afea-6b454eb18399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07da75e7-c305-4974-aa4c-6be9a30af535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f99d23e5-6bee-471c-8273-8fa2972f330d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07da75e7-c305-4974-aa4c-6be9a30af535",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "9bff11e2-8c90-408d-b980-3d4317f34d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "9bdd2af2-3df4-4666-b9ed-bec7656b83d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bff11e2-8c90-408d-b980-3d4317f34d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b7d70d8-0612-4119-af2a-a7ce301c4dde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bff11e2-8c90-408d-b980-3d4317f34d39",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "f5d2cd67-8fdd-44ac-a9ea-fa35ca25b9d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "1582e1ca-f0a1-47c7-9557-165beff6416c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5d2cd67-8fdd-44ac-a9ea-fa35ca25b9d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601ba03e-b485-4d16-8f1b-201e00e93328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5d2cd67-8fdd-44ac-a9ea-fa35ca25b9d4",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "a5823268-e82c-43b8-a334-44118a4aa6a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "f8ed2b17-def2-4436-838b-eb30d9ca8341",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5823268-e82c-43b8-a334-44118a4aa6a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e16e44-fdc1-4b3a-9cc5-dd87edcadf50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5823268-e82c-43b8-a334-44118a4aa6a0",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "8b99065e-2ef7-401e-8647-b4eb489f2cba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "484279ad-6b42-48ee-82d6-d9520eee3b1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b99065e-2ef7-401e-8647-b4eb489f2cba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "326c6577-fc06-447f-83b1-27756659bc71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b99065e-2ef7-401e-8647-b4eb489f2cba",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "6f2483c7-18bb-4159-bfe9-e59d49d9a922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "a0795039-151c-4228-b80a-ad6b2f71665d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2483c7-18bb-4159-bfe9-e59d49d9a922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c55d7c19-6798-4b74-b315-293c9d7931c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2483c7-18bb-4159-bfe9-e59d49d9a922",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        },
        {
            "id": "47496e26-fea3-4f81-b119-4ba20972943e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "compositeImage": {
                "id": "0fbaf6a2-b28b-4c2e-9c1f-a8de51c9f556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47496e26-fea3-4f81-b119-4ba20972943e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f6c1735-c775-4117-b386-e2c03c83c1f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47496e26-fea3-4f81-b119-4ba20972943e",
                    "LayerId": "087b2065-9e44-4caa-8248-1255206a76eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "087b2065-9e44-4caa-8248-1255206a76eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "087cc46f-db68-4f41-8aa3-c535eab0378c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}