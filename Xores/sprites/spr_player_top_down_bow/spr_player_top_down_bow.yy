{
    "id": "842c247b-c4e3-440c-9cbe-6825edd039a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_down_bow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da041365-1a96-48ea-8161-d387fcb4fc35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "842c247b-c4e3-440c-9cbe-6825edd039a8",
            "compositeImage": {
                "id": "34cc5eb0-6d92-4aec-a0d1-3420e65dbbca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da041365-1a96-48ea-8161-d387fcb4fc35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bbfcb9-b07b-446f-a1ca-972f71a1a9dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da041365-1a96-48ea-8161-d387fcb4fc35",
                    "LayerId": "a0ff898c-4d66-42da-94ac-da849cef953e"
                }
            ]
        },
        {
            "id": "db7ab7d6-5993-4fe4-917d-fc219cd9db8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "842c247b-c4e3-440c-9cbe-6825edd039a8",
            "compositeImage": {
                "id": "158b1262-2b84-4197-9cf1-8787cc6aa01e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7ab7d6-5993-4fe4-917d-fc219cd9db8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2ad5c5-aa4f-419a-8ea3-26fe0234441c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7ab7d6-5993-4fe4-917d-fc219cd9db8a",
                    "LayerId": "a0ff898c-4d66-42da-94ac-da849cef953e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "a0ff898c-4d66-42da-94ac-da849cef953e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "842c247b-c4e3-440c-9cbe-6825edd039a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}