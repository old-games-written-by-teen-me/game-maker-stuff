{
    "id": "ad59f455-ddc8-4ae5-866d-0755a9662fd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_down_pro_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81446cda-7bd9-4bd6-9b63-63f3610a2088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad59f455-ddc8-4ae5-866d-0755a9662fd1",
            "compositeImage": {
                "id": "a1dc15f5-f56c-4f78-a889-691d85fe885d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81446cda-7bd9-4bd6-9b63-63f3610a2088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e1edfc-d973-4cb1-b778-e84e44dc2ad6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81446cda-7bd9-4bd6-9b63-63f3610a2088",
                    "LayerId": "dcd731e6-d29b-4b8a-bfcd-bb44c8a25d42"
                }
            ]
        },
        {
            "id": "43336774-9096-4f45-9cf3-9ff2aba16f12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad59f455-ddc8-4ae5-866d-0755a9662fd1",
            "compositeImage": {
                "id": "d36aed2e-3b3c-4375-b3e3-b22cb7f620b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43336774-9096-4f45-9cf3-9ff2aba16f12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b5ce0da-bdad-4452-91e8-c15b2695e4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43336774-9096-4f45-9cf3-9ff2aba16f12",
                    "LayerId": "dcd731e6-d29b-4b8a-bfcd-bb44c8a25d42"
                }
            ]
        },
        {
            "id": "e3355607-e9ca-4b0c-a4d7-bcdfe40552a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad59f455-ddc8-4ae5-866d-0755a9662fd1",
            "compositeImage": {
                "id": "e3fbbb69-ec31-4f14-b689-2bede87f7300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3355607-e9ca-4b0c-a4d7-bcdfe40552a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cdf0714-6240-408a-a9e2-5516166dd58c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3355607-e9ca-4b0c-a4d7-bcdfe40552a5",
                    "LayerId": "dcd731e6-d29b-4b8a-bfcd-bb44c8a25d42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "dcd731e6-d29b-4b8a-bfcd-bb44c8a25d42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad59f455-ddc8-4ae5-866d-0755a9662fd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}