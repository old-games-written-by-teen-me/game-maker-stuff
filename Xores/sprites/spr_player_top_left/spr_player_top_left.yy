{
    "id": "56079a01-2e4e-467b-8449-56bb3a4c7134",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4589a9be-60a5-4a66-a5db-5ad43d250319",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "575f8e2c-b34b-49e4-940f-3d78dd0d38cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4589a9be-60a5-4a66-a5db-5ad43d250319",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dde1220-05be-464e-822d-7125a5dc4876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4589a9be-60a5-4a66-a5db-5ad43d250319",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "0fef9016-0279-4309-9799-15258f6d97b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "99bc1091-f579-47f2-9039-321decb05c06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fef9016-0279-4309-9799-15258f6d97b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94abb3b6-29b5-4659-9347-bf9257b907f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fef9016-0279-4309-9799-15258f6d97b1",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "e119463c-38e2-4fb5-9311-5eb24c38320b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "780753a5-6d08-4d3e-a102-dd92ec367984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e119463c-38e2-4fb5-9311-5eb24c38320b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5976c8a4-044a-4650-8613-3d90d2c2056f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e119463c-38e2-4fb5-9311-5eb24c38320b",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "52d7f6fe-803d-4fe7-af69-471509181d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "8dc98c0a-be2a-4499-ba81-8c2264923d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52d7f6fe-803d-4fe7-af69-471509181d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08ddcf30-f2be-453a-a127-0786f1b5fe95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52d7f6fe-803d-4fe7-af69-471509181d4d",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "54258ae7-0280-40b5-bd8f-0e39563f3404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "fc692d02-6f99-4054-8385-4f32565d9c76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54258ae7-0280-40b5-bd8f-0e39563f3404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0bc7f7-d033-45fd-986e-cd8497fe192a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54258ae7-0280-40b5-bd8f-0e39563f3404",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "ced03a1f-8cf5-4b69-9fbb-d33e12a10402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "e841f255-fae8-444b-af32-fe810ffd5f62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced03a1f-8cf5-4b69-9fbb-d33e12a10402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58556a0d-f1f8-42bb-976f-c87f448c473a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced03a1f-8cf5-4b69-9fbb-d33e12a10402",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "13a1ef40-4b36-4a82-a849-f28d3b2fe215",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "7f14d7b1-9221-4fe5-98b8-47aedc9dbb96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13a1ef40-4b36-4a82-a849-f28d3b2fe215",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82afbe70-ad4f-48c0-877d-c4b551bc70ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13a1ef40-4b36-4a82-a849-f28d3b2fe215",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "704e2c1d-eb90-4f56-92d5-064415f2ef49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "8ef635b3-5160-475b-bfb0-ff6a2c2bd291",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "704e2c1d-eb90-4f56-92d5-064415f2ef49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "425d9324-0081-4ac8-89ed-9b6460a8503b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "704e2c1d-eb90-4f56-92d5-064415f2ef49",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "d54cf54c-0f4b-4866-9c43-d75b857ab03d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "d42d5323-1e1b-4d96-9120-86677a94365a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d54cf54c-0f4b-4866-9c43-d75b857ab03d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "689137dd-6716-4ab0-96e2-367c93fc74e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d54cf54c-0f4b-4866-9c43-d75b857ab03d",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "727cd092-2654-4709-95cb-0bf7bbd52663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "cc4c8141-b70f-449d-9908-9547868db7c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727cd092-2654-4709-95cb-0bf7bbd52663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e604f2-5497-4b56-b888-6fd149c74d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727cd092-2654-4709-95cb-0bf7bbd52663",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "aeab58ff-8ef6-4d07-90fe-de78f17bffa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "9d998cc6-bbbf-4ee3-ad8e-8cf131be78b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeab58ff-8ef6-4d07-90fe-de78f17bffa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ee027d6-7a6e-47e0-a709-594270396c03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeab58ff-8ef6-4d07-90fe-de78f17bffa8",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        },
        {
            "id": "c8cc8d72-127a-413e-a8c2-74912361b4d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "compositeImage": {
                "id": "e4c26eb3-6429-42f9-8ea4-58078f3538d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8cc8d72-127a-413e-a8c2-74912361b4d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3c7317e-e24a-4bd7-97ef-e868ef1cc0ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8cc8d72-127a-413e-a8c2-74912361b4d3",
                    "LayerId": "9b7984e1-ad78-4e77-a769-d17b97e1b404"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "9b7984e1-ad78-4e77-a769-d17b97e1b404",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56079a01-2e4e-467b-8449-56bb3a4c7134",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}