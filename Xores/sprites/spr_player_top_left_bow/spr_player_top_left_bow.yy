{
    "id": "cb63f2c0-ece0-4c39-9199-ab86a0b4c652",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_left_bow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d283e202-62fc-4443-b244-c5009d7d1160",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb63f2c0-ece0-4c39-9199-ab86a0b4c652",
            "compositeImage": {
                "id": "88faf68f-7f12-4dcf-ad19-1bb9c2087175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d283e202-62fc-4443-b244-c5009d7d1160",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e6bd484-381d-4be3-86ee-d8c1d0da39ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d283e202-62fc-4443-b244-c5009d7d1160",
                    "LayerId": "0a2c402a-f2ba-4de5-a5b0-b86fe766e685"
                }
            ]
        },
        {
            "id": "6c77cd7a-2aa0-4ebf-a8f3-7f153b043543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb63f2c0-ece0-4c39-9199-ab86a0b4c652",
            "compositeImage": {
                "id": "1ddad9b0-1b7f-4376-a1b3-3eecfe95b3ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c77cd7a-2aa0-4ebf-a8f3-7f153b043543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00b7ab67-9853-4fd8-a075-194bb0b3deb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c77cd7a-2aa0-4ebf-a8f3-7f153b043543",
                    "LayerId": "0a2c402a-f2ba-4de5-a5b0-b86fe766e685"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "0a2c402a-f2ba-4de5-a5b0-b86fe766e685",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb63f2c0-ece0-4c39-9199-ab86a0b4c652",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}