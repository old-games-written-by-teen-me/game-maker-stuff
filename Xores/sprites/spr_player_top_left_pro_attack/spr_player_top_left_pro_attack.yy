{
    "id": "9a3e66bb-cd1c-43af-ad48-222fe235e395",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_left_pro_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "710f88f1-e725-4f43-b07d-daf7a17813b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3e66bb-cd1c-43af-ad48-222fe235e395",
            "compositeImage": {
                "id": "64066d47-cf5a-43e8-9971-4b540cd3a1c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710f88f1-e725-4f43-b07d-daf7a17813b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca18dce-21cb-4ae6-b133-81e59809df47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710f88f1-e725-4f43-b07d-daf7a17813b5",
                    "LayerId": "32d00442-2f7d-4e3c-b9fe-eae0becbaafc"
                }
            ]
        },
        {
            "id": "8601914f-2a56-4543-9185-2723a0931f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3e66bb-cd1c-43af-ad48-222fe235e395",
            "compositeImage": {
                "id": "30195cdf-e7df-47c2-8d55-ab3d44653164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8601914f-2a56-4543-9185-2723a0931f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3158fa4b-5cd6-46ce-b3ee-14fee5097010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8601914f-2a56-4543-9185-2723a0931f53",
                    "LayerId": "32d00442-2f7d-4e3c-b9fe-eae0becbaafc"
                }
            ]
        },
        {
            "id": "0bd38783-1727-49e8-907f-455e1262c75b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a3e66bb-cd1c-43af-ad48-222fe235e395",
            "compositeImage": {
                "id": "86b4b69a-a915-42cf-9317-f6ad8b5aff0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd38783-1727-49e8-907f-455e1262c75b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1b7406-2caa-412c-8b1a-51bc012bef2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd38783-1727-49e8-907f-455e1262c75b",
                    "LayerId": "32d00442-2f7d-4e3c-b9fe-eae0becbaafc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "32d00442-2f7d-4e3c-b9fe-eae0becbaafc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a3e66bb-cd1c-43af-ad48-222fe235e395",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}