{
    "id": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17a44879-df81-4d98-ab55-d675319deb17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "eb349638-544f-41f0-a371-59e72ba8d0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17a44879-df81-4d98-ab55-d675319deb17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb55d3f-2f8d-48a7-bcae-d3cbda690210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17a44879-df81-4d98-ab55-d675319deb17",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "1e484e69-19d8-46e7-85ef-e85bf2a6a6fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "15f306d8-03cd-4a1b-a18e-3fdbcdf0f72c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e484e69-19d8-46e7-85ef-e85bf2a6a6fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08892877-7bea-4dc9-9443-ff2cbaa6070f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e484e69-19d8-46e7-85ef-e85bf2a6a6fb",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "e161ee29-da62-42ef-99f7-1bdd2b5a3d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "84d12244-236a-4aff-824c-261cee0d9cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e161ee29-da62-42ef-99f7-1bdd2b5a3d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf2b903e-4da9-4349-9d3e-b7e834c12c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e161ee29-da62-42ef-99f7-1bdd2b5a3d30",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "0555bbbc-f189-43f4-a5aa-f44dfe638a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "df23bc64-87f6-453e-a54e-a2541e88de9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0555bbbc-f189-43f4-a5aa-f44dfe638a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d930353f-3136-4e03-9472-2ccaf00da94a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0555bbbc-f189-43f4-a5aa-f44dfe638a39",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "56cb0553-a4b9-451c-8db7-0ed731b6c956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "fe8a06af-568f-46c7-9305-376e0698b871",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56cb0553-a4b9-451c-8db7-0ed731b6c956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1df41a42-a9a7-4015-8083-6dd92e04f0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56cb0553-a4b9-451c-8db7-0ed731b6c956",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "5aea7f62-d441-4c11-81f5-62d8b203a20d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "9212aead-f9b9-4c41-be48-24b2f98388d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aea7f62-d441-4c11-81f5-62d8b203a20d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e73eb7-f647-4c32-82e6-65bec647b99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aea7f62-d441-4c11-81f5-62d8b203a20d",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "a745166a-0fa9-44f9-beb0-2ddba4b316c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "a0b53f74-04a3-44ac-bc6f-87d7ae1d4cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a745166a-0fa9-44f9-beb0-2ddba4b316c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69016543-400c-492d-beb3-6d59fca34616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a745166a-0fa9-44f9-beb0-2ddba4b316c8",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "b69b4118-8bab-4121-aede-39c956cb486f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "f28091a2-a8dd-464e-8c77-be734fcb1bcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b69b4118-8bab-4121-aede-39c956cb486f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3505e71f-0f83-4cf3-96f2-0f7eb7e66fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b69b4118-8bab-4121-aede-39c956cb486f",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "624b7d31-79c0-44af-bf14-37b433b310f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "f6a4ed1f-f0c8-4050-aa3e-7085844463a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "624b7d31-79c0-44af-bf14-37b433b310f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7abe118-1271-4e69-8d1a-b7167849b1c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "624b7d31-79c0-44af-bf14-37b433b310f7",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "b7107404-8587-4776-be85-5e9d71aaa17a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "983b2973-dc55-4f50-b4d3-ca2ec4fe3d80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7107404-8587-4776-be85-5e9d71aaa17a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f326ba9c-b1c1-4adb-b8cf-a05c4e60c9ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7107404-8587-4776-be85-5e9d71aaa17a",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "916d8d98-320c-44fd-89b5-64cbd1dc4217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "e53b876d-d9d8-4735-b1ad-fb829813ec19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "916d8d98-320c-44fd-89b5-64cbd1dc4217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "773c68d2-1a0b-401f-a63e-6cc2f115d550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "916d8d98-320c-44fd-89b5-64cbd1dc4217",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        },
        {
            "id": "746b7be0-4ed9-49eb-b3c8-fda1d5eae9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "compositeImage": {
                "id": "05ad6732-424a-4516-b5b0-71d8feeb2d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "746b7be0-4ed9-49eb-b3c8-fda1d5eae9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8f76fb-2e96-4f8a-a85b-4ea43a809a31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746b7be0-4ed9-49eb-b3c8-fda1d5eae9b8",
                    "LayerId": "3332f859-fe5a-44ab-b34b-3d11f7702469"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "3332f859-fe5a-44ab-b34b-3d11f7702469",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bd61aab-3ae7-4690-bb4e-597ffac2368c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}