{
    "id": "abd70952-6e26-41ff-b47b-84a2ec1e94fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_right_bow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 2,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "089aec6f-895a-4562-8e22-56a931348f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abd70952-6e26-41ff-b47b-84a2ec1e94fa",
            "compositeImage": {
                "id": "9755efcb-bcec-482e-a74a-525bb20fc77c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "089aec6f-895a-4562-8e22-56a931348f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a118fa-a816-4643-9c39-4a1edadb4bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "089aec6f-895a-4562-8e22-56a931348f3f",
                    "LayerId": "04fae2dc-568a-46b8-85f1-334a3ef6dc8b"
                }
            ]
        },
        {
            "id": "087dd50d-ca5a-4869-981d-d9294b587431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abd70952-6e26-41ff-b47b-84a2ec1e94fa",
            "compositeImage": {
                "id": "c3207d74-a7be-4689-b1a3-a05f0b5e763e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "087dd50d-ca5a-4869-981d-d9294b587431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe375d76-e2df-4d45-b612-82259df4719b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "087dd50d-ca5a-4869-981d-d9294b587431",
                    "LayerId": "04fae2dc-568a-46b8-85f1-334a3ef6dc8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "04fae2dc-568a-46b8-85f1-334a3ef6dc8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abd70952-6e26-41ff-b47b-84a2ec1e94fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}