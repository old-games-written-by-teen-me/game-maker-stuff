{
    "id": "0a8de452-3c30-4392-95c4-439a29629182",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_right_pro_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6d47666-62f4-4409-8f9d-0310ba5fe65e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a8de452-3c30-4392-95c4-439a29629182",
            "compositeImage": {
                "id": "34fd7b48-17d7-4ab2-93da-1589f86ea37b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d47666-62f4-4409-8f9d-0310ba5fe65e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a8e5fa-402f-4ad4-b564-e317caaf27c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d47666-62f4-4409-8f9d-0310ba5fe65e",
                    "LayerId": "d166419c-9760-4e19-a9a3-dc111476ba2b"
                }
            ]
        },
        {
            "id": "4ff45080-72ee-4ac9-8587-9dc51cb68057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a8de452-3c30-4392-95c4-439a29629182",
            "compositeImage": {
                "id": "1af4abcf-f082-4fa1-aebc-cafb2d462b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ff45080-72ee-4ac9-8587-9dc51cb68057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f94cf14d-57fa-485e-a996-b9192284262b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ff45080-72ee-4ac9-8587-9dc51cb68057",
                    "LayerId": "d166419c-9760-4e19-a9a3-dc111476ba2b"
                }
            ]
        },
        {
            "id": "3f7abb01-4cd8-4a55-826a-dd44b0049d80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a8de452-3c30-4392-95c4-439a29629182",
            "compositeImage": {
                "id": "823251ce-3913-4282-b7c4-763e81120feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f7abb01-4cd8-4a55-826a-dd44b0049d80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b734d815-89db-482f-8f08-1e2057d3b42c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f7abb01-4cd8-4a55-826a-dd44b0049d80",
                    "LayerId": "d166419c-9760-4e19-a9a3-dc111476ba2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "d166419c-9760-4e19-a9a3-dc111476ba2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a8de452-3c30-4392-95c4-439a29629182",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}