{
    "id": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95a24569-582e-4256-b58f-6de3ab444d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "43ce2c46-5fad-460d-a7b4-8a1af44b8eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a24569-582e-4256-b58f-6de3ab444d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4122e09-d5e1-4ee2-a823-32e667e39d1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a24569-582e-4256-b58f-6de3ab444d60",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "26874468-bee0-4afe-a25e-1e0afa205f3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "1a6d8927-edc3-43fc-bc0e-c806426f2183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26874468-bee0-4afe-a25e-1e0afa205f3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f32f79-2f9d-433f-83b1-ea2c4d103d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26874468-bee0-4afe-a25e-1e0afa205f3d",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "809490bd-0149-4a00-9c6c-0cae18fd1e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "4b270a08-7e6d-4f2e-a35d-f679e743aee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809490bd-0149-4a00-9c6c-0cae18fd1e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a51d3ab-b99c-4a13-8a1e-39ad9139f2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809490bd-0149-4a00-9c6c-0cae18fd1e16",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "9e422a3f-06e2-44f4-864e-036a9b4f91f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "eac54bf4-ec6d-426e-ad6b-5adc17c5555c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e422a3f-06e2-44f4-864e-036a9b4f91f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e61c3aa0-b805-413d-9f6c-d7b29edcbea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e422a3f-06e2-44f4-864e-036a9b4f91f4",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "7f8bf8af-6d3c-4906-b770-0be206641aa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "9cf37a1f-66ae-4bc7-9a9b-38842a306021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8bf8af-6d3c-4906-b770-0be206641aa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41c156a2-3036-4ba5-9563-9d8856e46343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8bf8af-6d3c-4906-b770-0be206641aa7",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "0bd4de47-7760-4c34-beab-5f44b79f77f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "ca9fabd3-7709-4691-9486-834a014d37a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd4de47-7760-4c34-beab-5f44b79f77f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fbd2df8-d3f0-4ae2-bda6-cf6feef0b40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd4de47-7760-4c34-beab-5f44b79f77f3",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "124fcbab-8a68-4bd4-ba80-606025588216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "cb54c1c2-a304-475d-bfb8-ca7751d47b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124fcbab-8a68-4bd4-ba80-606025588216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d86e0868-a943-4615-9133-9bb3a06d87cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124fcbab-8a68-4bd4-ba80-606025588216",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "e9bb06fa-7ec8-4054-aec9-ef5090ae5cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "0bfab7ea-d0c4-4524-ad5a-24558b26d230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9bb06fa-7ec8-4054-aec9-ef5090ae5cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ef30dec-dc20-46cd-9070-ecbb6f52ab86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9bb06fa-7ec8-4054-aec9-ef5090ae5cfa",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "483caa0c-c3f1-4b52-a321-80f058fbd2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "372f088b-7e24-48ee-97ac-cd3065286b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "483caa0c-c3f1-4b52-a321-80f058fbd2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "397a314d-b5fb-4b08-947d-fc9d2c76b147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "483caa0c-c3f1-4b52-a321-80f058fbd2f1",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "5e51c643-d2ee-41dc-a945-f5af037aa9c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "31b61bee-b2b4-4960-8f6a-9e55f314d04f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e51c643-d2ee-41dc-a945-f5af037aa9c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9bb01a6-b0ec-484f-b7e3-21ed010c0757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e51c643-d2ee-41dc-a945-f5af037aa9c1",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "e9c645cd-52df-45c5-9b50-d4e7b38f78ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "e1ebf85b-0e1c-4f5a-8c24-60b7ec0655da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c645cd-52df-45c5-9b50-d4e7b38f78ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ea011c1-df53-4fb6-af4e-174de32bbde8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c645cd-52df-45c5-9b50-d4e7b38f78ea",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        },
        {
            "id": "cc7583f4-c19f-416f-936b-0e63076d669c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "compositeImage": {
                "id": "c4467dcb-ad65-47dd-a59a-a9688bfb9d28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7583f4-c19f-416f-936b-0e63076d669c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d98bae-b3f8-438d-a38b-398124d9d01c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7583f4-c19f-416f-936b-0e63076d669c",
                    "LayerId": "a604ab27-366f-44bd-87ef-13d45e2160d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "a604ab27-366f-44bd-87ef-13d45e2160d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd24ab41-8661-494e-abc7-1fbda2495f4a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}