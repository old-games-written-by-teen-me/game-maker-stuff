{
    "id": "9db3844c-f8fe-458a-a705-ad2964f317b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_up_bow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26f8dc08-231d-4daa-bc86-318f02612040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db3844c-f8fe-458a-a705-ad2964f317b9",
            "compositeImage": {
                "id": "88c964b8-4e8c-4c00-a753-e32b0ab5ea8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26f8dc08-231d-4daa-bc86-318f02612040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b0561ff-18e0-4acf-80bd-d561d91d12d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26f8dc08-231d-4daa-bc86-318f02612040",
                    "LayerId": "167ad8d4-1a64-4bbf-b878-d6d6f68e2dc2"
                }
            ]
        },
        {
            "id": "2b28bd19-ffbf-413b-9df4-aed32176eb67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db3844c-f8fe-458a-a705-ad2964f317b9",
            "compositeImage": {
                "id": "34af07be-4ea9-4f25-8514-40de1693e414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b28bd19-ffbf-413b-9df4-aed32176eb67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07f52e39-6366-4689-9a23-571070c029b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b28bd19-ffbf-413b-9df4-aed32176eb67",
                    "LayerId": "167ad8d4-1a64-4bbf-b878-d6d6f68e2dc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "167ad8d4-1a64-4bbf-b878-d6d6f68e2dc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9db3844c-f8fe-458a-a705-ad2964f317b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}