{
    "id": "3350217f-6da5-4442-8ee1-8c5a2e065183",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_top_up_pro_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 2,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6ed33f5-38bc-42a8-9f4a-77fe4421d98e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3350217f-6da5-4442-8ee1-8c5a2e065183",
            "compositeImage": {
                "id": "a97468c5-c49e-4a9f-83ad-6aed803e44d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ed33f5-38bc-42a8-9f4a-77fe4421d98e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a99180c-7300-4dfd-9b69-ae46f3593a52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ed33f5-38bc-42a8-9f4a-77fe4421d98e",
                    "LayerId": "32bf57ee-0e89-4af5-a062-5d9752ac9fc4"
                }
            ]
        },
        {
            "id": "419a95bd-36de-4433-b80c-598d7d110e00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3350217f-6da5-4442-8ee1-8c5a2e065183",
            "compositeImage": {
                "id": "13f3cd5e-1be6-416c-b3fc-d7e1d2b06499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419a95bd-36de-4433-b80c-598d7d110e00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4966b3ad-1874-44ee-9fa5-77b96c90681d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419a95bd-36de-4433-b80c-598d7d110e00",
                    "LayerId": "32bf57ee-0e89-4af5-a062-5d9752ac9fc4"
                }
            ]
        },
        {
            "id": "e5910397-09e6-433b-97f3-eda6b46a3e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3350217f-6da5-4442-8ee1-8c5a2e065183",
            "compositeImage": {
                "id": "8d4aeaae-959f-4566-8f48-57271bae018a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5910397-09e6-433b-97f3-eda6b46a3e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5bc17e3-33c8-40ab-812f-dbf3288129b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5910397-09e6-433b-97f3-eda6b46a3e97",
                    "LayerId": "32bf57ee-0e89-4af5-a062-5d9752ac9fc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "32bf57ee-0e89-4af5-a062-5d9752ac9fc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3350217f-6da5-4442-8ee1-8c5a2e065183",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}