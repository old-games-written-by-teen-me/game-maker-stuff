{
    "id": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 1,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50ebaed8-9ed2-4b87-af87-452360404521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "a2d0e250-64e0-44b3-abcd-a8c8d08928d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ebaed8-9ed2-4b87-af87-452360404521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8598c6b3-ebf9-4b0b-86e1-86dde690f8fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ebaed8-9ed2-4b87-af87-452360404521",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "0cb2cfd5-d376-4f2d-986e-81f30b0de3ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "ae9f5dca-51d8-4ec1-899f-e2cd79d17c06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cb2cfd5-d376-4f2d-986e-81f30b0de3ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489c057b-938e-4d55-ba25-e58fd9ee4b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cb2cfd5-d376-4f2d-986e-81f30b0de3ea",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "b84e9685-f4a7-4a94-bcdb-246655e07a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "6392666e-afde-4e36-970e-efefcc74a34d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84e9685-f4a7-4a94-bcdb-246655e07a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4dc3668-e7ae-420d-90e8-f4841e76da28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84e9685-f4a7-4a94-bcdb-246655e07a6a",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "02a969ab-058a-48e5-a642-2fd17f8afb4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "52ef212e-c1c0-4699-80c4-458f24e01a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02a969ab-058a-48e5-a642-2fd17f8afb4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6fdc159-b601-41e1-913c-b315f7b49749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02a969ab-058a-48e5-a642-2fd17f8afb4e",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "1e529e75-9c69-4aca-be6c-2a5626eb3c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "05729a18-be6c-453d-bb20-adf35760ddbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e529e75-9c69-4aca-be6c-2a5626eb3c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43bcaeb8-e6ff-4235-9212-b8521a7bf7f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e529e75-9c69-4aca-be6c-2a5626eb3c2c",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "0b7eab88-30e5-4d66-9a18-bc0e6f6c14af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "ddebb7da-79d4-4599-8ee0-1291c2b520fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7eab88-30e5-4d66-9a18-bc0e6f6c14af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "178bb769-cfbd-4f31-88d7-5331dc35b6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7eab88-30e5-4d66-9a18-bc0e6f6c14af",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "f45d17b9-0670-4ea2-a2ca-53139d7c1c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "f2b5fe4c-e539-4f05-b94d-5e282bdfdd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45d17b9-0670-4ea2-a2ca-53139d7c1c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6afbef4-3af0-413f-9789-07e0598aeba9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45d17b9-0670-4ea2-a2ca-53139d7c1c27",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "8531abf8-0dd4-4bc9-b803-ba58aa36cf96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "a458e345-ffe9-464e-adda-6d8e3debd2df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8531abf8-0dd4-4bc9-b803-ba58aa36cf96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23238927-1e05-4165-989f-148a579c831e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8531abf8-0dd4-4bc9-b803-ba58aa36cf96",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "8c8ce3b1-fbbd-418e-80e5-810e8f1d0bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "2469704f-f69f-4c06-84d9-d4e9b95a8923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8ce3b1-fbbd-418e-80e5-810e8f1d0bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893ea472-250a-477e-be8b-c7d9a7e29f13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8ce3b1-fbbd-418e-80e5-810e8f1d0bf1",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "a25e49b5-930a-478b-9c38-911e73a740df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "dd5b9ca6-989c-4f3e-9b42-0b01f937c32d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25e49b5-930a-478b-9c38-911e73a740df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "758659d3-1df9-4338-b65a-3c5a49e17e59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25e49b5-930a-478b-9c38-911e73a740df",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "9c0a6e22-1805-40f6-baf5-2c614d41aec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "8075e86f-bd82-4b00-b843-9605c8070f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c0a6e22-1805-40f6-baf5-2c614d41aec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e0ae54-45aa-4674-8503-97b288b9b237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c0a6e22-1805-40f6-baf5-2c614d41aec7",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        },
        {
            "id": "c3f314d1-5574-4f3d-904c-47b8e3351724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "compositeImage": {
                "id": "a58e2c7b-aa2c-4aaa-80bc-a5fe5508d462",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3f314d1-5574-4f3d-904c-47b8e3351724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93de8833-d00a-4626-a2c9-c0cddac9d906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3f314d1-5574-4f3d-904c-47b8e3351724",
                    "LayerId": "4f332a83-1fe4-4e58-924a-4a8546edfafa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "4f332a83-1fe4-4e58-924a-4a8546edfafa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1319a274-dea4-4cdd-bf3b-c05cb9e7c91c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 5,
    "yorig": 9
}