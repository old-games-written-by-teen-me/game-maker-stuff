{
    "id": "ac05559a-424d-421f-b6e3-b89225e5680f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_roomobject_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 84,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36102351-a67c-4766-8754-3b34f0efa019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac05559a-424d-421f-b6e3-b89225e5680f",
            "compositeImage": {
                "id": "3ffbcfc8-1ef1-46f7-b9b2-a13ccec1d040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36102351-a67c-4766-8754-3b34f0efa019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa57424-170f-4735-a277-05ec3aecf61c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36102351-a67c-4766-8754-3b34f0efa019",
                    "LayerId": "33aa910b-606f-4a55-9c4d-914551f6d2b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "33aa910b-606f-4a55-9c4d-914551f6d2b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac05559a-424d-421f-b6e3-b89225e5680f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}