{
    "id": "0028c9e5-6603-4170-b0bd-a8a957e88542",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffc903bd-2c59-4e3e-80bc-29492a7c65cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0028c9e5-6603-4170-b0bd-a8a957e88542",
            "compositeImage": {
                "id": "935e3019-708c-4b1a-a891-73a1bfd71778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffc903bd-2c59-4e3e-80bc-29492a7c65cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b56fe088-f964-4dff-8ffb-5ff0a06a1456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffc903bd-2c59-4e3e-80bc-29492a7c65cc",
                    "LayerId": "890e02cd-de41-4e98-ad6f-14d397a528fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "890e02cd-de41-4e98-ad6f-14d397a528fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0028c9e5-6603-4170-b0bd-a8a957e88542",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 12
}