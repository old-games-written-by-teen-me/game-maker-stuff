{
    "id": "d397051a-b977-406a-b487-e15184634a23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke_part",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "109dad8c-a600-4350-b4bf-30d2f8671bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d397051a-b977-406a-b487-e15184634a23",
            "compositeImage": {
                "id": "32c329bd-1666-419d-a914-39ec1c5d539e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "109dad8c-a600-4350-b4bf-30d2f8671bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "591cefa4-8cbf-46f1-a415-6526fdf635c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "109dad8c-a600-4350-b4bf-30d2f8671bd5",
                    "LayerId": "f023a5df-3b71-40ea-899f-c5e5e3594655"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f023a5df-3b71-40ea-899f-c5e5e3594655",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d397051a-b977-406a-b487-e15184634a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}