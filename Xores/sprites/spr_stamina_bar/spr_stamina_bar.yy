{
    "id": "6ddf8bd2-46f5-4ae2-9b0f-383834a91d0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stamina_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 11,
    "bbox_right": 61,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28c1fef2-0f8d-45ca-88af-6caa1565e8af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ddf8bd2-46f5-4ae2-9b0f-383834a91d0c",
            "compositeImage": {
                "id": "001dd5ce-07ff-441c-84b0-04af166d1cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28c1fef2-0f8d-45ca-88af-6caa1565e8af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f2bc511-0a14-4a68-b028-772340efd4fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c1fef2-0f8d-45ca-88af-6caa1565e8af",
                    "LayerId": "df152d21-4ac5-4487-8513-89a194caf5c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df152d21-4ac5-4487-8513-89a194caf5c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ddf8bd2-46f5-4ae2-9b0f-383834a91d0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 11,
    "yorig": 2
}