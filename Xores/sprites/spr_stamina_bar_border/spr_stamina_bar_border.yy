{
    "id": "963f15d3-19aa-4bb3-bf6b-0ef994219dab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stamina_bar_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 6,
    "bbox_right": 62,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27f6672a-571a-42a2-b98d-7d04ae18f514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963f15d3-19aa-4bb3-bf6b-0ef994219dab",
            "compositeImage": {
                "id": "30e95c18-23e6-4465-a214-687e1714057a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27f6672a-571a-42a2-b98d-7d04ae18f514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4002bceb-689d-411a-8a0e-550733519634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27f6672a-571a-42a2-b98d-7d04ae18f514",
                    "LayerId": "c41b8da7-97b3-4768-8693-fc27594a62dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c41b8da7-97b3-4768-8693-fc27594a62dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "963f15d3-19aa-4bb3-bf6b-0ef994219dab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 11,
    "yorig": 2
}