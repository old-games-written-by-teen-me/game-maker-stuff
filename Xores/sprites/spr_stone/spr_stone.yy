{
    "id": "1265f5ac-e880-4130-947d-a9135166d484",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee3afbed-0d6b-410f-aa83-3e445e58ab4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1265f5ac-e880-4130-947d-a9135166d484",
            "compositeImage": {
                "id": "01836cd7-1852-4fd0-8684-a90f40c44157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee3afbed-0d6b-410f-aa83-3e445e58ab4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb93b9a-f591-4efa-8f52-8e42e12cf8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee3afbed-0d6b-410f-aa83-3e445e58ab4c",
                    "LayerId": "6f0b29a7-a70f-41cd-90ca-e28c679e24bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6f0b29a7-a70f-41cd-90ca-e28c679e24bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1265f5ac-e880-4130-947d-a9135166d484",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}