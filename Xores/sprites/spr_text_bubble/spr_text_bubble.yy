{
    "id": "4970af00-dd45-4268-8085-0e7a16d4f4bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_text_bubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "faffbbf8-726c-47e9-b693-79015203b269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4970af00-dd45-4268-8085-0e7a16d4f4bf",
            "compositeImage": {
                "id": "7264af7d-24ee-49a9-9c23-04a9b63f9a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faffbbf8-726c-47e9-b693-79015203b269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8511e7a0-45c7-4964-af27-c0d941d42011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faffbbf8-726c-47e9-b693-79015203b269",
                    "LayerId": "3c90ffd2-1f13-466d-af2b-eecdf2903f9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "3c90ffd2-1f13-466d-af2b-eecdf2903f9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4970af00-dd45-4268-8085-0e7a16d4f4bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 0
}