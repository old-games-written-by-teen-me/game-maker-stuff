{
    "id": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 11,
    "bbox_right": 36,
    "bbox_top": 52,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b190b8e-2d2a-43e5-b475-6650f146817a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
            "compositeImage": {
                "id": "fa2ef34c-4f61-44ec-9dae-94b3a220db0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b190b8e-2d2a-43e5-b475-6650f146817a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4290938e-3151-4d88-a78e-eee15c076a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b190b8e-2d2a-43e5-b475-6650f146817a",
                    "LayerId": "1cf0c45e-0cbc-4453-aa26-93b5474e5f27"
                }
            ]
        },
        {
            "id": "3f901b42-b40d-401d-992b-a4a8a797f086",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
            "compositeImage": {
                "id": "ac3552c5-7088-41a0-af61-4e95b5e1257a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f901b42-b40d-401d-992b-a4a8a797f086",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "488d836f-c6bc-4be9-8b87-ea31e23673f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f901b42-b40d-401d-992b-a4a8a797f086",
                    "LayerId": "1cf0c45e-0cbc-4453-aa26-93b5474e5f27"
                }
            ]
        },
        {
            "id": "96e090db-c47a-4cb0-bcce-5d32ea208fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
            "compositeImage": {
                "id": "24a26745-60b1-4190-a1a1-4ac2c082536d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e090db-c47a-4cb0-bcce-5d32ea208fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e9a5da-98e3-4a09-8703-81b7c9ef5c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e090db-c47a-4cb0-bcce-5d32ea208fb9",
                    "LayerId": "1cf0c45e-0cbc-4453-aa26-93b5474e5f27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1cf0c45e-0cbc-4453-aa26-93b5474e5f27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9ffd7ac-965b-4eee-a2a7-905197f03a65",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 50
}