{
    "id": "a262212c-eb70-401a-8f87-2913f89d8951",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ca1d3e6-6315-4c39-87fd-ba08bb837970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a262212c-eb70-401a-8f87-2913f89d8951",
            "compositeImage": {
                "id": "c3dd2ae8-ef7e-4ff4-a129-e0654dff11fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ca1d3e6-6315-4c39-87fd-ba08bb837970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d939e399-76e8-417d-92e8-47e974003e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ca1d3e6-6315-4c39-87fd-ba08bb837970",
                    "LayerId": "6e914385-d71b-4dd3-9f13-2040ca346b86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6e914385-d71b-4dd3-9f13-2040ca346b86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a262212c-eb70-401a-8f87-2913f89d8951",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}