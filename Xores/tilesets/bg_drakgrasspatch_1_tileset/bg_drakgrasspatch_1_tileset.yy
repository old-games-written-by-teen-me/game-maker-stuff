{
    "id": "13d608c4-bc3a-4996-9bd1-6feb21691bbb",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "bg_drakgrasspatch_1_tileset",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 4,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "beba6710-d104-4f98-bbe5-33a4ed11e57f",
    "sprite_no_export": false,
    "textureGroupId": "00000000-0000-0000-0000-000000000000",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 16,
    "tileheight": 30,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 30,
    "tilexoff": 0,
    "tileyoff": 0
}