{
    "id": "d710dfbd-59fa-4dbf-bdf1-0c2d2c9d4580",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "fg_bed_tileset",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "dc0879a9-0094-4bad-903d-49ef6edecce7",
    "sprite_no_export": false,
    "textureGroupId": "00000000-0000-0000-0000-000000000000",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 1,
    "tileheight": 16,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 16,
    "tilexoff": 0,
    "tileyoff": 0
}