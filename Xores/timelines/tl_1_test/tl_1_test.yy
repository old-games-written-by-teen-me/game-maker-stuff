{
    "id": "4c487250-2bb6-42c1-8991-2f9c306cdd8d",
    "modelName": "GMTimeline",
    "mvc": "1.0",
    "name": "tl_1_test",
    "momentList": [
        {
            "id": "fbd09205-13a3-4e7d-8484-604df65ac81a",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "195496ff-7b46-4505-a0dc-13574edb6a89",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 0,
                "eventtype": 0,
                "m_owner": "4c487250-2bb6-42c1-8991-2f9c306cdd8d"
            },
            "moment": 0
        },
        {
            "id": "3761c1e4-ce43-47ae-9410-b0b86f308ce2",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "d6852b58-e5ff-4a99-887f-11a03b2d6e1a",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 30,
                "eventtype": 0,
                "m_owner": "4c487250-2bb6-42c1-8991-2f9c306cdd8d"
            },
            "moment": 30
        },
        {
            "id": "5382c728-8776-4b56-89b6-2c7583184a6d",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "a9aa250c-18dd-4db9-b1a2-f35df30b76a1",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 120,
                "eventtype": 0,
                "m_owner": "4c487250-2bb6-42c1-8991-2f9c306cdd8d"
            },
            "moment": 120
        },
        {
            "id": "55b3ae56-6c8a-4886-921f-090abe87f087",
            "modelName": "GMMoment",
            "mvc": "1.0",
            "name": "",
            "evnt": {
                "id": "9270cdb3-a26b-44b1-9791-48b8ddc9fcc3",
                "modelName": "GMEvent",
                "mvc": "1.0",
                "IsDnD": false,
                "collisionObjectId": "00000000-0000-0000-0000-000000000000",
                "enumb": 160,
                "eventtype": 0,
                "m_owner": "4c487250-2bb6-42c1-8991-2f9c306cdd8d"
            },
            "moment": 160
        }
    ]
}